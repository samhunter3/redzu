<?php

namespace Fuel\Migrations;

class Create_user_meta
{
	public function up()
	{
		\DBUtil::create_table('user_meta', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'meta_key' => array('constraint' => 255, 'type' => 'varchar'),
			'meta_value' => array('type' => 'text'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('user_meta');
	}
}