<?php

namespace Fuel\Migrations;

class Create_notifications
{
	public function up()
	{
		\DBUtil::create_table('notifications', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'type' => array('constraint' => 64, 'type' => 'varchar'),
			'message' => array('constraint' => 255, 'type' => 'varchar'),
			'user_read' => array('constraint' => 1, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('notifications');
	}
}