<?php

namespace Fuel\Migrations;
class Create_metafields
{
	public function up()
	{
		\DBUtil::create_table('metafields', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'label' => array('constraint' => 255, 'type' => 'varchar'),
			'meta' => array('constraint' => 255, 'type' => 'varchar'),
			'default' => array('constraint' => 255, 'type' => 'varchar'),
			'group' => array('constraint' => 24, 'type' => 'varchar'),
			'weight' => array('constraint' => 11, 'type' => 'int'),
			'options' => array('constraint' => 255, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),
		), array('id'));

		self::create_default();
	}

	static public function create_default()
	{
		
		\Model_Metafield::insert_unique(
			array(
				'meta' => 'about_me',
			),
			array(
				'meta' => 'occupation',
				'group' => 'userspecs',
			),
			array(
				'meta' => 'height',
				'group' => 'userspecs',
			),
			array(
				'meta' => 'weight',
				'group' => 'userspecs',
			),
			array(
				'meta' => 'education',
				'group' => 'userspecs',
			),
			array(
				'meta' => 'body_type',
				'group' => 'userspecs',
				'options' => array("default" => '—', "Rather not say", "Thin", "Overweight", "Skinny", "Average",
					"Fit", "Athletic", "Jacked", "A little extra", "Curvy", "Full figured", "Used up"),
				
			),
			array(
				'meta' => 'smokes',
				'group' => 'userspecs',
				'options' => array("default" => '—', "Yes", "Sometimes", "When drinking", "Trying to quit", "No"),
				
			),
			array(
				'meta' => 'drinks',
				'group' => 'userspecs',
				'options' => array("default" => '—', "Very often", "Often", "Socially", "Rarely", "Desperately", "Not at all"),
				
			),
			array(
				'meta' => 'drugs',
				'group' => 'userspecs',
				'options' => array("default" => '—', "Never", "Sometimes", "Often"),
			),

			array(
				'meta' => 'sign',
				'group' => 'userspecs',
				'options' => array("Aquarius", "Pisces", "Aries", "Taurus", "Gemini", "Cancer", "Leo", "Virgo", "Libra", "Scorpio", "Sagittarius", "Capricorn"),
			),
			array(
				'meta' => 'religion',
				'group' => 'userspecs',
				'options' => array("Agnosticism", "Atheism", "Christianity", "Judaism", "Catholicism", "Islam", "Hinduism", "Buddhism", "Other"),
			),
			array(
				'meta' => 'ethnicity',
				'group' => 'userspecs',
				'options' => array("Asian", "Middle Eastern", "Black", "Native American", "Indian", "Pacific Islander", "Hispanic / Latin", "White", "Other"),
			)
		);
	}

	public function down()
	{
		\DBUtil::drop_table('metafields');
	}
}