<?php

namespace Fuel\Migrations;

class Create_users
{
	public function up()
	{
		\DBUtil::create_table('users', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'username' => array('constraint' => 255, 'type' => 'varchar'),
			'verify_email' => array('constraint' => 255, 'type' => 'varchar'),
			'searchable' => array('constraint' => 1, 'type' => 'int', 'default' => 0),
			'password' => array('constraint' => 255, 'type' => 'varchar'),
			'email' => array('constraint' => 255, 'type' => 'varchar'),
			'profile_fields' => array('type' => 'text'),
			'group' => array('constraint' => 11, 'type' => 'int'),
			'last_login' => array('constraint' => 20, 'type' => 'int'),
			'last_action' => array('constraint' => 20, 'type' => 'int', 'default' => 0),
			'login_hash' => array('constraint' => 255, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),
		), array('id'));

		$admin_username = "Redzu";
		$admin_password = "livenow123";
		$admin_pass_hash= \Auth::instance()->hash_password($admin_password);
		$admin_email    = "drsonya@redzu.com";

		$user = \Model_User::factory(array(
		    'username' => $admin_username,
		    'password' => $admin_pass_hash,
		    'email' => $admin_email,
		    'profile_fields' => '',
		    'group' => '100',
		    'last_login' => '',
		    'last_action' => '',
		    'login_hash' => '',
		    'searchable' => 0,
		));

		$sam = \Model_User::factory(array(
		    'username' => 'Sam',
		    'password' => \Auth::instance()
		    	->hash_password('Stifrtvcd6'),
		    'email' => 'x6media@gmail.com',
		    'profile_fields' => '',
		    'group' => '999',
		    'last_action' => '',
		    'last_login' => '',
		    'login_hash' => '',
		    'searchable' => 0,
		));

		if ($user and $user->save()) {
		    \Cli::write("Added admin 'Redzu' account");
		} else {
		    \Cli::write("Failed to add admin 'Redzu' account");
		}

		if ($sam and $sam->save()) {
		    \Cli::write("Added hacker 'Sam' account");
		} else {
		    \Cli::write("Failed to add hacker 'Sam' account");
		}
	}

	public function down()
	{
		\DBUtil::drop_table('users');
	}
}