<?php

namespace Fuel\Migrations;

class Add_events_fields
{
	public function up()
	{
		\DBUtil::add_fields('events', array(
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'ticket_website' => array('constraint' => 500, 'type' => 'varchar'),
		));		
	}

	public function down()
	{
		\DBUtil::drop_fields('events', array('ticket_website', 'user_id'));
	}
}