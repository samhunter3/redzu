<?php

namespace Fuel\Migrations;

class Create_user_search_filters
{
	public function up()
	{
		\DBUtil::create_table('user_search_filters', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'query_params' => array('constraint' => 255, 'type' => 'varchar'),
			'label' => array('constraint' => 255, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('user_search_filters');
	}
}