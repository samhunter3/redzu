<?php

namespace Fuel\Migrations;

class Create_events
{
	public function up()
	{
		\DBUtil::create_table('events', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'event_name' => array('constraint' => 75, 'type' => 'varchar'),
			'start_time' => array('constraint' => 11, 'type' => 'int'),
			'end_time' => array('constraint' => 11, 'type' => 'int'),
			'ticket_price' => array('constraint' => '8', 'type' => 'float'),
			'age_min' => array('constraint' => 3, 'type' => 'int'),
			'age_max' => array('constraint' => 3, 'type' => 'int'),
			'venue_name' => array('constraint' => 52, 'type' => 'varchar'),
			'venue_address' => array('constraint' => 125, 'type' => 'varchar'),
			'venue_city' => array('constraint' => 52, 'type' => 'varchar'),
			'state' => array('constraint' => 40, 'type' => 'varchar'),
			'zipcode' => array('constraint' => 5, 'type' => 'varchar'),
			'country' => array('constraint' => 2, 'type' => 'varchar'),
			'max_attendees' => array('constraint' => 4, 'type' => 'int'),
			'latitude' => array('constraint' => 30, 'type' => 'int'),
			'longitude' => array('constraint' => 30, 'type' => 'int'),
			'venue_phone' => array('constraint' => 12, 'type' => 'varchar'),
			'venue_website' => array('constraint' => 500, 'type' => 'varchar'),
			'additional_info' => array('constraint' => 2500, 'type' => 'varchar'),
			'tags' => array('constraint' => 255, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('events');
	}
}