<?php
namespace Social;

class Model_Like extends Model_SocialElement
{
	protected static $_belongs_to = array('user' => array('model_to' => '\\Model_User'));
	protected static $_properties = array(
		'id',
		'object_type',
		'object_id',
		'user_id',
		'created_at',
		'updated_at'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function get_likes( $class, $oid = null )
	{
		if( $class instanceof \Orm\Model )
		{
			$oid = $class->id;
			$class = strtolower(substr(get_class( $class ), 6));
		}
		elseif( is_string($class) )
		{
			$class = strtolower($class);
		}

		return Model_Like::find()->where(array(
			'object_type' => $class,
			'object_id' => $oid
		));
	}
}
