<?php
namespace Social;

use \Fuel\Core\Model_User as Model_User;

class Model_SocialElement extends Model
{
	protected static $_query_wrapper;

	public static function query_wrapper()
	{
		$args = func_get_args();
		static::$_query_wrapper or static::$_query_wrapper = '\\Social\\QueryWrapper';
		return call_user_func_array(array(static::$_query_wrapper, 'forge'), $args);
	}

	public static function & social_row_data($object_type, $parent)
	{
		$data = array(
			'object_type' => $object_type,
			'object_id' => $parent->id,
		);

		$belongs_to = isset(static::$_belongs_to[$object_type]) ? static::$_belongs_to[$object_type] : false;

		if ($belongs_to)
		{
			$model_key = isset($belongs_to['key_to']) ? $belongs_to['key_to'] : ($object_type . '_id');
			$data[$model_key] = $parent->id;
		}

		return $data;
	}

	public static function & get_social_query($parent, $key)
	{
		$se_model = static::get_social_model($key);
		$query = static::query_wrapper($se_model, $parent);	
		
		$query->where($se_model::social_row_data($parent::get_object_type(), $parent));
		\Inflector::pluralize($key) === $key or $query->limit(1);

		return $query;
	}
}
