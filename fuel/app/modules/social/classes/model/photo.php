<?php

namespace Social;
class Model_Photo extends Model_SocialElement
{
	protected static $_default_photos = array();

	protected static $_belongs_to = array('user' => array('model_to' => '\\Model_User'));
	
	protected static $_table_name = 'photos';
	protected static $_properties = array(
		'id',
		'user_id',
		'object_type',
		'object_id',
		'user_id',
		'url',
		'title',
		'filename',
		'album',
		'description',
		'created_at',
		'updated_at'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function get_default_photo( $oid = null, $otype = 'user' )
	{
		if( isset(static::$_default_photos[$otype.'-'.$oid]) )
		{
			return static::$_default_photos[$otype.'-'.$oid];
		}

		if( $oid !== null )
		{
			$image = static::find()->where(array('object_type' => $otype, 'object_id' => $oid, 'album' => 'redzu-default'))->get_one();
			if( $image )
			{
				return static::$_default_photos[$otype.'-'.$oid] = $image->to_array();
			}
			elseif( $has_image = static::find()->where(array('object_type' => $otype, 'object_id' => $oid))->get_one() )
			{
				$has_image->album = 'redzu-default';
				$has_image->save();

				return static::$_default_photos[$otype.'-'.$oid] = $has_image;
			}
		}

		$photo = null;
		if( $otype == 'user' && ($user=\Model_User::find($oid)) && $animal = $user->get_meta('animal', false) )
		{
			$animal = \Redzu_Animals::get( $animal );
			$photo = $animal['image'];
		}

		return static::$_default_photos[$otype.'-'.$oid] = array(
			'url' => $photo ? $photo : 'assets/img/no-image.gif',
			'user_id' => null,
			'title' => 'redzu-default',
			'filename' => 'redzu-default',
			'album' => 'redzu-default',
			'description' => 'The default Redzu photo for your profile!',
			'has_photos' => false,
		);

	}
}
