<?php
namespace Social;

class Model_Comment extends Model_SocialElement
{
	protected static $_belongs_to = array('user' => array('model_to' => '\\Model_User'));
	
	protected static $_properties = array(
		'id',
		'object_type',
		'object_id',
		'user_id',
		'text',
		'created_at',
		'updated_at'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public function get_comments( $class, $oid = null )
	{
		if( $class instanceof Model )
		{
			$oid = $class->id;
			$class = strtolower(substr(get_class( $model ), 7));
		}
		elseif( is_string($class) )
		{
			$class = strtolower($class);
		}

		return static::find()->where(array(
			'object_type' => $class,
			'object_id' => $oid
		));
	}
}
