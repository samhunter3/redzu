<?php

namespace Social;

/**
 * Query Wrapper for Social Elements Model
 *
 * @package Redzu
 * @author  Samuel hunter
 */

class QueryWrapper implements \ArrayAccess, \Iterator
{
	
	public $parent;
	public $child;
	public $query;

	protected $_child_key;
	protected $_keys;
	protected $_result;
	protected $_multiple;

	public function __construct($child, $parent)
	{
		$this->child  = $child;
		$this->_child_key = $child::get_object_type();
		$this->parent = $parent;
		$this->query  = $child::query();
		$this->_multiple = \Inflector::pluralize($this->_child_key) === $this->_child_key;
	}

	static public function forge($child, $parent)
	{
		return new QueryWrapper($child, $parent);
	}

	public function __call($method, $args)
	{
		if( method_exists($this->query, $method) )
		{
			return call_user_func_array(array($this->query, $method), $args);
		}
		elseif( method_exists($this->child, $method) )
		{
			return call_user_func_array(array($this->child, $method), $args);
		}
	}

	public function get_query()
	{
		return $this->query->get();
	}

	/**
	 * Access
	 *
	 */
	public function & _get($key)
	{
		if( $parent = $child::get_object_type() )
		{
			$final_models = array();
			$models = $this->query->get();

			foreach($models as $m_k => &$model)
			{
				$final_models[$m_k] = $model->get($key);
			}

			if( \Inflector::pluralize($key) !== $key )
			{
				return reset($final_models);
			}

			return $final_models;
		}

		return get_key($key);
	}

	public function & _set($key, $value)
	{
		$child =& $this->child;
		if ($parent = $child::get_object_type())
		{
			$models = $this->query->get();
		}

		return $this->set_key($key, $value);
	}

	public function _has_key($key)
	{
		$child =& $this->child;
		return isset($this->query->$key) || (bool)($child::get_object_type());
	}

	public function _remove($key)
	{
		unset($this->_keys[$key]);
	}

	public function get_key($key)
	{
		return '';
	}

	public function set_key($key, $value)
	{
	}

	/**
	 * Iterator
	 *
	 */
	public function rewind()
	{
		return rewind($this->_keys);
	}

	public function current()
	{
		return current($this->_keys);
	}

	public function key()
	{
		return key($this->_keys);
	}

	public function next()
	{
		return next($this->_keys);
	}

	public function valid()
	{
		return $this->has_key(key($this->_keys));
	}


	/**
	 * Access, Array
	 *
	 */
	public function offsetSet($key, $value)
	{
		return $this->_set($key, $value);
	}

	public function offsetGet($key)
	{
		return $this->_get($key);
	}

	public function offsetExists($key)
	{
		return $this->_has_key($key);
	}

	public function offsetUnset($key)
	{
		return $this->_remove($key);	
	}



	/**
	 * Access, Object
	 *
	 */
	public function & __get($key)
	{
		return $this->_get($key);		
	}

	public function & __set($key, $value)
	{
		return $this->_set($key, $value);		
	}

	public function __isset($key)
	{
		return $this->_has_key($key);		
	}

	public function __unset($key)
	{
		return $this->_remove($key);		
	}
}