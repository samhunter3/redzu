<?php

namespace Social;

class Model extends \Orm\Model
{
	protected static $_object_type;

	public function before()
	{
		parent::before();

		// Load ninjauth authentication
		\Config::load('ninjauth', 'ninjauth');
	}
	public static function get_object_type()
	{
		$called = get_called_class();
		return $called::$_object_type = \Inflector::singularize( \Inflector::tableize($class_name) );
	}

	public static function get_social_model($key)
	{
		$model = '\\Social\\Model_' . \Inflector::classify($key);
		return (class_exists($model) && array_key_exists('object_type', $model::properties())) ? $model : null;
	}

/*
	public function & get( $key )
	{
		if ( ! ($se_model = static::get_social_model($key)) )
		{
			return parent::get($key);
		}

		$multiple = \Inflector::pluralize($key) === $key;
		return $se_model::get_social_query($this, $key);
	}

	public function offsetSet( $key, $value )
	{		
		$this->push($key, $value);
	}

	public function set( $key, $models )
	{
		if( ! ($se_model = static::get_social_model($key)) )
		{
			return parent::set( $key, $models );
		}

		$multiple = \Inflector::pluralize($key) === $key;

		if( ! $multiple && is_array($models) )
		{
			$models = reset($models);
		}

		$query = $this->get($key)->delete();
		$this->push($key, $models);
	}
*/

	public function __call($key, $args)
	{
		if( $model = static::get_social_model($key) )
		{
			$query = $model::find();

			$class_name = get_class($this);
			$class_singular = \Inflector::singularize( \Inflector::tableize($class_name) );
			$class_fk = \Inflector::foreign_key($class_name);

			list($primary_key) = (array) $this->primary_key();
			if(array_key_exists($class_singular, (array) $model::relations()))
			{
				$query->where($class_fk, $this->$primary_key);
			}

			$query->where(array(
				'object_type' => $class_singular,
				'object_id' => $this->$primary_key
			));
			($multiple = \Inflector::pluralize($key) === $key) or $query->limit(1);

			$get_method = $multiple ? 'get' : 'get_one';
			$method = isset($args[0]) ? array_shift($args) : $get_method;

			if( $method )
			{
				$query = call_user_func_array(array($query, $method), $args);
			}

			return $query;
		}

		parent::__call($key, $args);
	}

/*
	public function __call($key, $args)
	{
		$multiple = \Inflector::pluralize($key) === $key;
		$objid = isset($args[0]) && is_numeric($args[0]) ? (integer)$args[0] : null;

		try {
			$find = $this->get($key);
		}
		catch (\OutOfBoundsException $e)
		{
			return parent::__call($key, $args);
		}

		if( $objid )
		{
			return $find->where('id', $find->id)->get_one();
		}

		if( $multiple )
		{
			return $find->get();
		}

		return $find->get_one();
	}
*/

}