<?php

namespace Admin;

class Controller extends \Controller_Template_Default
{
	public function after($response)
	{
		if (isset($this->template->content))
		{
			$this->template->content = \View::forge('admin/_tabs.twig', array(
				'content' => $this->template->content));
		}

		parent::after($response);
		return $response;
	}
}