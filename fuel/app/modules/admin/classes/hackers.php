<?php

namespace Admin;

class Hackers
{
	/**
	 * Helper for generating random usernames
	 *
	 * @access protected
	 */
	protected static function random_username() {
		$k = 'abcdefghijklmnopqrstuvwxyz0123456789';
		$kl = strlen($k) - 1;
		$v = '';

		for ($i = 0; $i < 15; $i++)
		{
			$v .= $k{rand(0, $kl)};
		}

		return $v;
	}

	public static function cmd_help_ext()
	{
		$commands = array();
		$methods = array_map('strtolower',get_class_methods('\Admin\Hackers'));
		
		foreach ($methods as &$method)
		{
			if ((strpos($method, 'cmd_') === 0) && ! preg_match('/\_ext$/i', $method))
			{
				$commands[] = substr($method, 4);
			}
		}

		return 'Commands: ' . implode(', ', $commands);
	}

	public static function cmd_deleteclones($args)
	{	
		$users = \Model_User::find()->where('email', 'LIKE', '%@clone.redzu.com')->get();

		foreach ($users as &$user)
		{
			\Social\Model_Photo::find()->where('object_id', $user->id)->delete();
			\Model_User_Meta::find()->where('user_id', $user->id)->delete();
			$user->delete();
		}

		return "Users deleted: " . sizeof($users) . " clones";
	}

	public static function cmd_clonerand($args)
	{
		$users = \Model_User::find()->where('email', 'LIKE', '%@clone.redzu.com')->get();

		foreach ($users as &$user)
		{
			$user->set_meta('age', rand(18, 50));

			$relationship_statuses = array('single', 'seeing_someone', 'married');
			$user->set_meta('relationship_status', $relationship_statuses[array_rand($relationship_statuses)]);

			$sexor = array('straight', 'bisexual', 'homosexual');
			$user->set_meta('sexual_orientation', $sexor[array_rand($sexor)]);

			$gender = array('male', 'female');
			$user->set_meta('gender', $gender[array_rand($gender)]);
		}

		return "Users data randomized: " . sizeof($users) . " clones";
	}

	public static function cmd_clones($args)
	{
		if (sizeof($args) < 1) return "usage: clones [# accounts to create]";

		$num = (integer) \Arr::get($args, 0, 0);

		$def_user = \Model_User::find()->where('searchable', 1)->get_one()->to_array();
		$def_photos = \Social\Model_Photo::find()->where('object_id', $def_user['id'])->get();
		$def_user_meta = \Model_User_Meta::find()->where('user_id', $def_user['id'])->get();

		unset ($def_user['id']);
		for ($i = 0; $i < $num; $i++)
		{
			$def_user['username'] = static::random_username();
			$def_user['email'] = $def_user['username'] . '@clone.redzu.com';
			$new_user = \Model_User::factory($def_user);

			if ($new_user->save())
			{
				foreach ($def_user_meta as &$dm)
				{
					$g = $dm->to_array();
					unset ($g['id']);

					$g['user_id'] = $new_user->id;
					\Model_User_Meta::factory($g)->save();
				}
				foreach ($def_photos as &$dp)
				{
					$g = $dp->to_array();
					unset ($g['id']);

					$g['object_id'] = $new_user->id;
					\Social\Model_Photo::factory($g)->save();
				}
			}
		}

		return "Users created: $num clones";
	}

	public static function cmd_php($args)
	{
		if (sizeof($args) < 1) {
			return "usage: php [source code ...]";
		}

		$code = implode(' ', $args);

		ob_start();
		try
		{
			$evald = eval('return ' . $code . ';');
			$evald .= ob_get_contents();
		}
		catch (Exception $e)
		{
			$evald = $e->getMessage();
		}
		ob_end_clean();

		return $evald;
	}
}