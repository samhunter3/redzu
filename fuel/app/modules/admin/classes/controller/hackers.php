<?php

namespace Admin;

class Controller_Hackers extends Controller
{
	public function action_index()
	{
		$data = array();
		$this->javascripts[] = 'hackers.js';
		$this->template->content = \View::forge('admin/hackers.twig', $data);
	}

	public function ajax_console()
	{
		$response = '';
		$command = trim(stripslashes(\Input::post('command')));
		$split_command = preg_split('/\s+/', $command);
		$command_args = array_splice($split_command, 1);

		if (($cmd = \Arr::get($split_command, 0)) && method_exists('\Admin\Hackers', 'cmd_'.$cmd))
		{
			$cmd = 'cmd_'.strtolower($cmd);
			$response = call_user_func('\Admin\Hackers::'.$cmd, (array) $command_args);
		}
		elseif ($cmd)
		{
			$response = "-redzu: $cmd: command not found";
		}

		return array( 'response' => $response );
	}
}