<?php
namespace Admin;

class Controller_Admin extends Controller
{
	public function action_index()
	{
		$data = array();
		$this->template->content = \View::forge('admin/index.twig', $data);
	}
}