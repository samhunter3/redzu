<?php

namespace Deals;

class Controller_Deals extends \Controller_Template_Default
{
	public function action_index()
	{
		$data = array();
		$deals = json_decode(file_get_contents('https://api.groupon.com/v2/deals.json?division_id=san-francisco&area=east-bay
&client_id=b1725936447d51a8cd83e1b653e135915c35900a'), true);

		$this->template->content = \View::forge('deals/index.twig', $data);
		$this->template->content->set('deals', $deals['deals'], false);
	}
}