<?php

namespace Users;
class Controller_Messages extends \Controller_Template_Default
{
	public function before()
	{
		parent::before();
	}

	public function action_index()
	{
		return call_user_func(array($this, 'action_all'));
	}
	public function action_all($user = null)
	{
		$args = func_get_args();

		if ($user)
		{
			return call_user_func_array(array($this, 'action_read'), $args);
		}
		
		return call_user_func_array(array($this, 'action_list'), $args);
	}

	/**
	 * @route /users/messages/all/:username
	 * @route /users/messages/read/:username
	 */
	public function action_read($user = null)
	{
		$query = \Model_User::find()->where('username', $user)->get_one();
		if ($user && !($from=\Model_User::find()->where('username', $user)->get_one()))
		{
			$this->response->status = 404;
			$this->template->content = \View::forge('errors/custom.twig', array(
				'title' => 'User does not exist',
				'message' => 'Maybe you clicked a bad link.'
			));

			return;
		}

		$data = array();
		$data['to_user'] = $from;
		$this->javascripts[] = 'messages.js';
		$this->template->content = \View::forge('users/messages/chat.twig', $data);
	}

	/**
	 * @route /users/messages/all
	 * @route /users/messages/list
	 */
	public function action_list()
	{
		$data = array();

		$query = \Model_DB::forge('messages');
		$data['unread_messages'] = \Model_Message::find()->where('user_id', $this->user->id)
			->where('read', '!=', 1)->count();
		$data['messages'] = $query
			->where('id', 'IN', new \Database_Expression('
				(SELECT MAX(`id`) FROM `messages` WHERE
					(`from_user_id` = '.(integer)$this->user->id.' OR
					`user_id` = '.(integer)$this->user->id.'))'))
			->order_by('created_at', 'DESC')
			->get();

		#\Debug::dump($data['messages']);

		$this->template->content = \View::forge('users/messages/index.twig', $data);
	}
}