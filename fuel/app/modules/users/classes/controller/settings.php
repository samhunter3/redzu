<?php

namespace Users;

class Controller_Settings extends \Controller_Template_Default
{
	public function before()
	{
		parent::before();
	}

	protected function update_account(array &$fields, array &$errors)
	{
		$fields = \Helpers::filter_fields($fields, array(
			'confirm_password',
			'password',
			'email'
		), true, false);

		// Validator instance
		$val = \Validation::forge();

		// Reference our validation library
		$val->add_callable('validationlib');

		// Set Redzu_Metafields for validation library
		\ValidationLib::config( $val );

		if (isset($fields['email']))
		{
			$val->add('email', 'Email')
				->add_rule('required')
				->add_rule('trim')
				->add_rule('valid_email');

			if( \Model_User::find()->where(array(
				array('email', trim($fields['email'])),
				array('id', '!=', $this->user->id)
			))->count() )
			{
				$errors['email'] = 'User with that email already exists';
			}
		}

		if ($set_password=strlen($fields['password']))
		{
			if($set_password < 6)
			{
				$errors['password'] = 'Too short, minimum is 6 characters';
			}
			elseif($set_password > 16)
			{
				$errors['password'] = 'Too long, maximum is 16 characters';
			}
			elseif( $fields['password'] !== $fields['confirm_password'] )
			{
				$errors['confirm_password'] = empty($fields['confirm_password']) ?
					'Can\'t be empty!' : 'Does not match new password';
			}
		}

		if(! $val->run($fields))
		{
			$errors = array_merge($errors, (array)$val->errors());
		}

		if (! empty($errors))
		{
			\Helpers::load_js('raw', 'handleFlagErrors("#mysettings");');
			return;
		}

		// Validations below
		
		if( isset($fields['email']) )
		{
			$this->user->set('email', $fields['email'])->save();
		}

		if($set_password)
		{
			$this->user->set('password', \Auth::hash_password($fields['password']))->save();
		}

		\Session::set_flash('success', 'Settings updated!');
		return \Response::redirect(\Uri::create('users/settings'));
	}

	public function action_profile()
	{
		$data = array();
		$this->template->content = \View::forge('users/settings/profile.twig', $data);
	}

	public function action_account( $method = null, $provider = '' )
	{
		$data = array();
		$data['errors'] = array();
		$data['fields'] = \Input::post();
		$data['loginmethods'] = array('facebook' => 'Facebook', 'twitter' => 'Twitter');
		$sconnections = array();
		foreach($data['loginmethods'] as $k => &$m)
		{
			$m = array('name' => $m);
			$sconnections[$k] = \NinjAuth\Model_Authentication::find()->where(array('provider' => $k, 'user_id' => \Helpers::my_uid()))->get_one();
			$m['connected'] = $sconnections[$k];
		}

		switch( $method )
		{
			case 'link':
				\Session::set('redirect', 'users/settings/account');
				return \Response::redirect('users/session/'.$provider);
			case 'unlink':
				if (isset($sconnections[$provider]))
				{
					$sconnections[$provider]->delete();
					\Session::set_flash('success', $data['loginmethods'][$provider]['name'] . ' account removed');
				}
				return \Response::redirect(\Uri::create('users/settings/account'));
		}

		if( \Input::method() === 'POST' )
		{
			$this->update_account($data['fields'], $data['errors']);
		}

		$data['fields'] = array_merge($this->user->to_array(), $data['fields']);
		$this->template->content = \View::forge('users/settings/account.twig', $data);
	}

	public function action_index()
	{
		return $this->action_account();
	}

	public function after($response)
	{
		if (isset($this->template->content))
		{
			$this->template->content = \View::forge('users/settings/_tabs.twig', array(
				'content' => $this->template->content));
		}

		parent::after($response);
		return $response;
	}
}