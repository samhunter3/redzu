<?php

namespace Users;
class Controller_Photos extends \Controller_Photos
{
	public function before()
	{
		$username = $this->param('username');
		$user = $username ? array('username' => $username) : array('id' => \Helpers::my_uid());
		$this->_photos_url = $username ? 'profile/'.strtolower($username).'/photos' : 'me/photos';
		if($user = \Model_User::find()->where($user)->get_one())
		{
			\Helpers::set_pfuser($user);
			$this->object_type = 'user';
			$this->object_id = $user->id;

			return parent::before();
		}

		\Request::show_404();
	}
}