<?php

namespace Users;

class Controller_Users extends \Controller_Template_Default
{
	protected $_list_title = 'Find People';

	/**
	 * Events
	 */
	public function before()
	{
		$templatedata = array();
		if($this->request->action === 'login' or $this->request->action === 'request')
		{
			$this->template = 'templates/default-2.twig';
			$templatedata['body_class'] = 'logintmpl';
		}


		if ($this->request->action === 'register' and ! \Config::get('allow_registration') and ! \Session::get('invite_token') )
		{
			$this->template = 'templates/default-2.twig';
			$templatedata['body_class'] = 'logintmpl';
		}

		parent::before();
		\Helpers::populateView($this->template, $templatedata);

		// Load the configuration for this provider
		\Config::load('ninjauth', 'ninjauth');
		\Package::load('oauth2');
		\Package::load('oauth');
	}

	/**
	 * Helpers
	 */
	protected function has_valid_invite_token()
	{
		if (\Config::get('allow_invitation') && \Session::get('invite_token'))
		{
			$invitation = \Model_Invitation::from_token(\Session::get('invite_token'));
			if (! $invitation && ! \Auth::check())
			{
				\Session::delete('invite_token');
				$this->template->content = \View::forge('errors/custom.twig', array(
					'title' => 'Invitation is invalid.',
					'message' => 'We\'re redirecting you to the homepage.',
					'redirect' => '/'
				));
				return;
			}

			return $invitation;
		}

		return true;
	}

	/**
	 * Controller Actions
	 */

	public function action_index()
	{
		$data = array();

		$this->template->title = 'Redzu';
		$this->template->content = \View::forge('users/index.twig', $data);
	}

	public function action_logout()
	{
		\Auth::instance()->logout();
		if( isset($this->user_id) )
		{
			\Model_User::find($this->user_id)->set('last_action', 0)->save();
		}
		return \Response::redirect('/users/login');
	}

	public function action_search($query_type = 'search', $page = 1)
	{
		return call_user_func_array(array($this, 'action_list'), array($query_type, $page));
	}

	public function action_list($query_type = 'all', $page = 1)
	{
		$myID = \Helpers::my_uid();
		$data = array();

		$data['title'] = 'Find people';

		// List of filtered users
		$data['filter'] = array();

		// Query users matching filter
		$query = \User_Query::where(array(
			array('searchable', 1),
			array('id', '!=', $myID),
		))->of_orientation($this->user);

		// Configure pagination
		$pagination = \Pagination::forge(array(
			'pagination_url' => (\Uri::create('users/'.$query_type.'/')
				.'/%s'.'?'.$_SERVER['QUERY_STRING']),
			'total_items' => $query->count(),
			'per_page' => (integer) \Input::param('per_page', 15),
			'current_page' => (integer) $page,
			'template' => array(
				'wrapper_start'  => '<div class="main-pagination">'
			),
		));

		$query->limit($pagination->per_page)
			->offset($pagination->offset);

		$results = $query->get_with_meta();
		foreach ($results as $uid => &$user)
		{
			$data['filter'][$uid] = array_merge($user, array
			(
				'photo' => \Social\Model_Photo::get_default_photo($uid),
			));
		}

		$this->template->content = \View::forge('users/list.twig', $data);
		$this->template->content->set('pagination', $pagination->create_links(), false);
	}

	public function action_cancel()
	{
		$this->template->content = \View::forge('users/cancel.twig');
	}

	public function action_session($provider = null)
	{
		if(! $this->has_valid_invite_token())
		{
			return;
		}

		return in_array($provider, array('facebook', 'twitter'))
			and \NinjAuth\Strategy::forge($provider)->authenticate();
	}

	public function action_callback($provider)
	{
		$strategy = \NinjAuth\Strategy::forge($provider);	
		\NinjAuth\Strategy::login_or_register($strategy);
	}

	public function action_request()
	{
		$data = array();
		$fields = \Input::post();
		if( \Input::method() === 'POST' )
		{
			$errors = array();

			// Validator instance
			$val = \Validation::forge();

			// Reference our validation library
			$val->add_callable('validationlib');

			// Set Redzu_Metafields for validation library
			\ValidationLib::config( $val );

			$val->add('email', 'Email')
				->add_rule('required')
				->add_rule('trim')
				->add_rule('unique', 'users.email')
				->add_rule('valid_email');

			$val->add('zipcodeb', 'Zip Code')
				->add_rule('min_length', 5);

			$data['submit'] = true;
			if ($val->run($fields))
			{
				if($zip = \Input::post('zipcodeb'))
				{
					$zvald   = \Redzu_Metafield_Zipcode::forge();
					$zipdata = $zvald->validate('Zip Code', array('zipcode' => $zip));
					$errors  = $zvald->errors;
					$fields = $val->validated();
				}
			}
			else
			{
				$errors = $val->errors();
			}

			if ($errors)
			{
				$data['errors'] = true;
				\Session::set_flash('error', implode("<br />", $errors));
			}
			elseif(isset($zipdata))
			{
				if( ! \Model_Invitation::find()->where('email', $fields['email'])->limit(1)->count() )
				{
					\Model_Invitation::factory(array(
						'email' => $fields['email'],
						'data' => json_encode($zipdata),
					))->save();
				}
			}
		}

		$data['fields'] = \Helpers::filter_fields($fields, array('zipcodeb', 'email'), true);
		$this->template->content = \View::forge('users/request_invite.twig', $data);
	}

	public function action_login()
	{
		if ($this->logged_in)
		{	
			return \Response::redirect('/me');
		}

		$username = \Input::post('username');
		$password = \Input::post('password');

		if( \Input::get('next') )
		{
			// Set the path to navigate to after login
			\Session::set( 'next', \Input::get('next') );
		}

		$user = \Model_User::find_by_username($username);
		if(\Input::method() === 'POST')
		{
			if( $username and $password and \Auth::instance()->login( $username, $password ) )
			{
				// \Auth::instance()->logout();
				
				$this->logged_in = \Auth::instance()->get_id();
			}
			else
			{
				if( ! $username || ! $password )
				{
					$username or \Session::set_flash('usernameerror', 'Username is empty');
					$password or \Session::set_flash('passworderror', 'Password is empty');
				}
				elseif( $username and ! \Model_User::find()->where('username', $username)->count() )
				{
					\Session::set_flash('usernameerror', 'User doesn\'t exist');
				}
				elseif( $password )
				{
					\Session::set_flash('passworderror', 'Password is incorrect');
				}
			}
		}

		if( $this->logged_in )
		{
			return \Response::redirect('/me');
		}

		$this->template->title = 'Login to Redzu';
		$this->template->content = \View::forge('users/login.twig');

		// View properties
		$this->template->content->username = $username;
		$this->template->content->flash_error = \Session::get_flash('loginerror');
		$this->template->content->username_error = \Session::get_flash('usernameerror');
		$this->template->content->password_error = \Session::get_flash('passworderror');
	}

	public function action_recover($code = null)
	{
		$data = array();
		$data['errors'] = array();

		if ($code)
		{
			$data['code'] = $code;
			$valid_code = false;

			if ($valid_code)
			{
				#$user
				return \Response::forge('users/settings/account');
			}

			\Session::set_flash('error', 'Invalid account recovery code');
		}

		if ($data['post'] = \Input::method() === 'POST')
		{
			$user = \Model_User::query();
			$data['identifier'] = \Input::post('identifier');

			if (\Helpers::validate_email($data['identifier']))
			{
				$user->where('email', $data['identifier']);
			}
			else
			{
				$user->where('username', $data['identifier']);
			}

			if ($user->count())
			{
				$user->get_one()->send_password_recovery_email();
			}
			else
			{
				$data['errors']['identifier'] = "User does not exist";
			}
		}

		$this->template->content = \View::forge('users/recover_password.twig', $data);
	}

	public function action_invite()
	{
		\Session::set('invite_token', \Input::get('k'));
		return \Response::redirect('users/register');
	}

	public function action_register( $new_step = null, $param2 = null )
	{
		if (! ($invite_token=$this->has_valid_invite_token()))
		{
			return;
		}
		elseif (! \Config::get('allow_registration'))
		{
			if( \Config::get('allow_invitation') )
			{
				return \Response::redirect('users/request');
			}

			$this->template->content = \View::forge('errors/custom.twig', array(
				'title' => 'Registration closed',
				'message' => 'New members will not be accepted at this time.',
				'redirect' => '/',
			));
			return;
		}

		// Current step of registration
		$step = (integer) \Session::get('regstep', 1);

		if( $new_step && is_numeric($new_step) && (integer)$new_step <= $step )
		{
			$step = (integer)$new_step;
		}

		if( $this->logged_in )
		{
			\Session::delete('regstep');
			\Session::delete('regdata');
			\Session::set_flash('error', 'You already have an account!');
			return \Response::redirect( \Uri::create('/me') );
		}

		// Form submission status
		$post = \Input::method() === 'POST';

		// Validator instance
		$val = \Validation::forge();

		// Reference our validation library
		$val->add_callable('validationlib');

		// Set Redzu_Metafields for validation library
		\ValidationLib::config( $val );

		// View content
		$content = array( 'title' => 'Create an account' );

		$fields = array();
		$regdata = (array) \Session::get('regdata', array());

		// Merge validated input
		$updateRegData = function () use (&$fields, &$regdata) {
			$regdata = array_merge( $regdata, array_map('trim', $fields) );
			\Session::set('regdata', $regdata);
		};

		$user_hash = (array) \Session::get('ninjauth.user');
		$authentication = (array) \Session::get('ninjauth.authentication');

		$username = \Input::post('register.username') ?: \Arr::get($user_hash, 'nickname');
		$email = \Input::post('register.email') ?: \Arr::get($user_hash, 'email');
		$password = \Input::post('register.password');

		if (!$password and \Arr::get($user_hash, 'email'))
		{
			$password = substr( $password ?: \Crypt::encode($username,
				\Config::get('crypt_key') . rand(0,999999)), 0, 255 );
		}
		
		if( $step === 1 )
		{
			$content['animals'] = \Helpers::animals_list();

			$this->javascripts[] = 'register.js';

			if( $post )
			{
				// Filtered post parameters
				$fields = \Helpers::filter_fields(
					(array) \Input::post('register'),
					array('animal'),
					true
				);

				if( ! \Redzu_Animals::has($fields['animal']) )
				{
					$fields['animal'] = '';
				}

				$updateRegData();
				$step++;
			}
		}
		elseif( $step === 2 )
		{
			$content['title'] = 'Your almost done!';
			$animal = \Session::get('regdata.animal');
			if( $animal )
			{
				$content['animal'] = \Redzu_Animals::get( $animal );
				$animalname = $content['animal']['name'];
			}

			$allowed_fields = array(
				'username',
				'password',
				'email',
				'tos',
			);

			$fields = array();
			if ($username)
			{
				$email and $post = true;
				$fields['username'] = $username;
				$fields['tos'] = true;
				$fields['password'] = $password;
				$fields['email'] = $email;
			}

			// Filtered post parameters
			$fields = array_merge(\Helpers::filter_fields(
				(array) \Input::post('register'),
				$allowed_fields,
				true
			), $fields);


			if( $post )
			{

				$val->set_message('match_value', ':label fields must match.');

				$val->add('username', 'Username')
					->add_rule('required')
					->add_rule('trim')
					->add_rule('match_pattern', '/^[a-z][a-z0-9]+$/i')
					->add_rule('unique', 'users.username')
					->add_rule('min_length', 3)
					->add_rule('max_length', 16);

				$val->add('password', 'Password')
					->add_rule('required')
					->add_rule('trim')
					->add_rule('min_length', 3)
					->add_rule('max_length', 255);

				$val->add('tos', 'Terms of Service')
					->add_rule('required');

				$val->add('email', 'Email')
					->add_rule('required')
					->add_rule('trim')
					->add_rule('unique', 'users.email')
					->add_rule('valid_email');

				if( $val->run( $fields ) )
				{

					\Debug::dump($val->errors());

					$fields = $val->validated();
					$updateRegData();

					try
					{
						// Everything is done, create account
						$user = \Auth::create_user
						(
							$regdata['username'],
							$regdata['password'],
							$regdata['email'],
							\Config::get('ninjauth.default_group')
						);

						if ($user and isset($authentication['access_token']))
						{
							\NinjAuth\Model_Authentication::forge(array(
								'user_id' => $user,
								'provider' => $authentication['provider'],
								'uid' => $authentication['uid'],
								'access_token' => $authentication['access_token'],
								'secret' => $authentication['secret'],
								'refresh_token' => $authentication['refresh_token'],
								'expires' => $authentication['expires'],
								'created_at' => time(),
							))->save();
						}

						\Model_User::find( $user )->set_meta('animal', $regdata['animal']);

						// Login the user we just created
						\Auth::force_login( $user );


						\Session::set_flash('success', 'Thanks for registering!');
						\Session::set_flash('info', 'This is your profile. When your ready you can come back and jot some things down about yourself so our users can check you out.');

							
						\Session::delete('regstep');
						\Session::delete('regdata');
						\Session::delete('regerror');
						\Session::delete('invite_token');

						if (is_object($invite_token))
						{
							$invite_token->delete();
						}

						// Yay, registration is done
						return \Response::redirect( \Uri::create('/me/profile') );
					}
					catch( \FuelException $e )
					{
						// Something failed, go back.
						\Session::set_flash('regerror', $e->getMessage());
						return \Response::redirect( \Uri::create('/users/register/' . ($step-1) ) );
					}
				}
				else
				{
					\Session::set_flash('regerror', implode( $val->errors(), '<br />' ));
				}
			}
		}

		if( $step !== \Session::get('regstep', 1) )
		{
			// Set current step
			\Session::set('regstep', $step );
			return \Response::redirect( \Uri::create('/users/register/' . $step ) );
		}

		$this->template->title = 'Create an account - Redzu';
		$this->template->content = \View::forge('users/register.twig');
		$this->template->content->step = $step;


		\Helpers::populateView( $this->template->content, (array) array_merge( $fields, $content ) );
		$this->template->content->set( 'flash_error', \Session::get_flash('regerror'), false );
	}

	public function ajax_setprofiledata($uid = null)
	{
		if( ! ($uid = \Helpers::can_use_uid($uid)) || ! ($user = \Helpers::get_user($uid)) )
		{
			return array('error' => 'You do not have permission to change this user');
		}
		$vm = \Helpers::get_view_model('Users_Ajax_SetProfileData', array('user' => $user));
		return array(
			'form' => ((string)$vm),
			'errors' => $vm->errors(),
			'success' => ! $vm->errors(),
		);
	}

	public function ajax_togglelike( $object_type = '', $object_id = 0 )
	{
		$uid = \Helpers::can_use_uid();

		if( ! is_numeric($object_id) || (strlen($object_type) < 3) )
		{
			return array('error' => 'An error occured');
		}

		$find = array(
			'object_type' => $object_type,
			'object_id' => $object_id,
		);

		$find_me = array_merge($find, array(
			'user_id' => $uid
		));

		if( $like = \Social\Model_Like::find()->where($find_me)->get_one() )
		{
			$like->delete();
		}
		else
		{
			$like = \Social\Model_Like::factory($find_me);
			$like->is_new(true);
			$like->save();
		}

		$count = \Social\Model_Like::find()->where($find)->count();
		$text = $count == 0 ? 'Like' : $count . ' ' . ($count !== 1 ? 'Likes' : 'Like');

		return array( 'text' => $text );
	}

	public function ajax_profilefields( $uid = null, $fields = null )
	{
		if( $fields === null or ! ($uid = \Helpers::can_use_uid($uid)) )
		{
			return array('error' => 'You do not have permission to change this user');
		}

		$fields = explode(',', $fields);

		$return = array();
		foreach( $fields as $k => $field )
		{
			$r =& $return[$field];
			
			$profilefield = \Model_Metafield::find()->where('meta', $field)->get_one();
			if( $profilefield )
			{
				if( \Input::method() === 'POST' )
				{
					$r['value'] = call_user_func_array('\Redzu_Profileform::update_'.$field, array(
						$uid, \Input::post()
					));

					if( $r['value'] === false )
					{
						if( \Redzu_Profileform::$_errors )
						{
							$r['error'] = implode( PHP_EOL, \Redzu_Profileform::$_errors );
						}
						else
						{
							$r['error'] = 'An unexpected error occured.';
						}
					}
				}
				else
				{
					$r['form'] = call_user_func_array('\Redzu_Profileform::form_'.$field, array(
						$uid,
					));

					if( \Redzu_Profileform::$_errors )
					{
						$r['error'] = implode( PHP_EOL, \Redzu_Profileform::$_errors );
					}
				}
			}
			else
			{
				$r['error'] = 'Profile field does not exist. Hacking attempt logged';
				continue;
			}
		}

		return $return;
	}

	public function ajax_photos( $uid = null, $pid = null, $action = null )
	{
		if( $fields === null or ! ($uid = \Helpers::can_use_uid($uid)) )
		{
			return array('error' => 'An unexpected error occured');
		}

		$user = \Helpers::get_user($uid);
		$actions = array('update', 'delete', 'default');
		$photo = $user->photos(0)->where(array( 'id' => $pid ))->get_one();

		if( ! $photo )
		{
			return array('error' => 'Photo does not exist');
		}

		$data = array();

		if( $action === 'update' )
		{
			if( ($description = \Input::param('description')) === null )
			{
				return array('error' => 'Parameter "description" required');
			}

			$photo->description = $description;
			$photo->save();
		}

		return $data;
	}

	public function action_upgrade()
	{
		$this->comingsoon();
	}

	public function action_profile( $username = null )
	{
		if($edit = ($username === 'edit' or $username === strtolower($this->logged_in)))
		{
			$username = $this->logged_in;
		}
		
		$user = \Model_User::find()->where( 'username', $username )->get_one();

		if( ! $user ) return \Request::show_404();
		$edit or $edit = (bool)\Helpers::can_use_uid($user->id);

		\Helpers::can_use_uid($user->id) or $user->increment_meta('profile_hits');
		\Helpers::set_pfuser($user);

		$this->template->title = $user->username . '\'s profile';
		$this->template->content = \View::forge('users/profile.twig');

		if( $this->template->content->edit = $edit )
		{
			$this->javascripts[] = 'edit_profile.js';
		}
	}
}
