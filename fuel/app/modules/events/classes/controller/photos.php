<?php
namespace Events;
class Controller_Photos extends \Controller_Photos
{
	protected $object_type = 'event';
	protected $_view = 'events/photos.twig';

	public function before()
	{
		$this->object_id = (integer) $this->param('id');
		$this->_photos_url = '/events/'.$this->object_id.'/photos';
		
		if( ! $this->object_id ) {
			return \Request::show_404();
		}

		return parent::before();
	} 
}