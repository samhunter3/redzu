<?php
namespace Events;

class Controller_Events extends \Controller_Template_Default
{
	protected $viewtype = 'calendar';

	public function action_show( $id = 0 )
	{
		$data = array();
		$id or $id = $this->param('id', 0);

		if( ! $id || ! ($data['event'] = \Model_Event::find($id)))
		{
			return \Request::show_404();
		}

		
		$data['edit'] = \Helpers::can_use_uid($data['event']->user_id);
		$data['buy_ticket'] = (\Helpers::validate_url($data['event']->ticket_website) ?
			$data['event']->ticket_website : false);

		$this->template->content = \View::forge('events/show.twig', $data);
	}

	public function update_event( $model = null )
	{
		$model or $model = \Model_Event::forge();
		$data = array( 'event' => $model );

		$errors = array();
		$postData = \Input::post();

		foreach ($postData as &$pd)
		{
			if (is_string($pd))
			{
				$pd = stripslashes($pd);
			}
		}

		$input = 
			\Helpers::filter_fields(array_merge($model->to_array(), $postData),
			array(
			'date',
			'event_name',
			'start_time',
			'end_time',
			'ticket_price',
			'age_min',
			'age_max',
			'venue_name',
			'venue_address',
			'venue_city',
			'state',
			'zipcode',
			'country',
			'max_attendees',
			'ticket_website',
			'latitude',
			'longitude',
			'venue_phone',
			'venue_website',
			'additional_info',
			'user_id',
			'tags',
		), true, false);

		if (! isset($input['user_id']) || ! $input['user_id']) {
			$input['user_id'] = \Helpers::my_uid();
		}

		if (! $input['date'] && is_numeric($model->start_time))
		{
			$input['date'] = date('m/d/Y', \Helpers::user_time($input['start_time']));
			$input['start_time'] = explode('/', date('g/i/A', \Helpers::user_time($input['start_time'])));
			$input['end_time'] = explode('/', date('g/i/A', \Helpers::user_time($input['end_time'])));
		}

		if (\Input::method() === 'POST')
		{
			// Validate Zip Code
			$zvalidator = new \Redzu_Metafield_Zipcode();
			$data = $zvalidator->validate('Zip Code', $input);
			if ($data)
			{
					$input['state'] = $data['state_abbreviated'];
					$input['venue_city'] = $data['city'];
					$input['zipcode'] = $data['zipcode'];
					$input['country'] = $data['country'];
					$input['latitude'] = $data['latitude'];
					$input['longitude'] = $data['longitude'];
			}
			else
			{
				$errors = array_merge($errors, $zvalidator->errors);
			}

			(($len=strlen($input['event_name'])) < 4) && $errors['event_name'] = 'Too short, minimum 4 characters';
			($len > 75) and $errors['event_name'] = 'Too long, maximum 75 characters';

			if( ! preg_match('/^([0-9]{1,2}\/){0,2}[0-9]{4}$/', $input['date']) )
			{
				$errors['date'] = 'Invalid date';
			}
			elseif( strtotime($input['date']) < (time() - \Helpers::DAYS) )
			{
				$errors['date'] = 'That time has already past';
			}
			else
			{
				if( sizeof($input['start_time']) !== 3 )
				{
					$errors['start_time'] = 'Invalid time';
				}
				elseif( sizeof($input['end_time']) !== 3 )
				{
					$errors['end_time'] = 'Invalid time';
				}
				else
				{
					$start_time = $input['start_time'][0] . ':' . $input['start_time'][1] . $input['start_time'][2];
					$end_time = $input['end_time'][0] . ':' . $input['end_time'][1] . $input['end_time'][2];
					$startd = \Helpers::user_time(strtotime($input['date'] . ' ' . $start_time));
					$endd = \Helpers::user_time(strtotime($input['date'].' '.$end_time));
					if( $startd < time() )
					{
						$errors['start_time'] = 'Must be future time';
					}
					elseif( $endd < $startd )
					{
						$errors['end_time'] = 'Can\'t be before the event starts';
					}
				}
			}


			if( ! is_numeric($input['ticket_price']) )
			{
				$errors['ticket_price'] = 'Invalid ticket price';
			}
			elseif( $input['ticket_price'] > 400 )
			{
				$errors['ticket_price'] = 'Tickets can\'t cost more than $400 USD';
			}


			if( strlen($input['venue_name']) < 5 )
			{
				$errors['venue_name'] = 'Too short, minimum 4 characters';
			}
			elseif( strlen($input['venue_name']) > 54 )
			{
				$errors['venue_name'] = 'Too long, maximum 54 characters';
			}


			if( strlen($input['venue_address']) < 5 )
			{
				$errors['venue_address'] = 'Too short, minimum 4 characters';
			}
			elseif( strlen($input['venue_address']) > 54 )
			{
				$errors['venue_address'] = 'Too long, maximum 54 characters';
			}

			if( ! is_numeric($input['age_min']) )
			{
				$errors['age_min'] = 'Invalid';
			}
			if( ! is_numeric($input['age_max']) )
			{
				$errors['age_max'] = 'Invalid';
			}

			if( strlen($input['additional_info']) < 25 )
			{
				$errors['additional_info'] = 'Too short, at least 25 characters';
			}
			elseif( strlen($input['venue_address']) > 2500 )
			{
				$errors['additional_info'] = 'Too long, maximum 54 characters';
			}
			
			$input['venue_phone'] = preg_replace('/[^0-9]+/', '', $input['venue_phone']);
			if( $input['venue_phone'] && strlen($input['venue_phone']) < 10 )
			{
				$errors['venue_phone'] = 'Invalid phone number';
			}

			if( (integer)$input['ticket_price'] !== 0 && ! \Helpers::validate_url($input['ticket_website']) )
			{
					$errors['ticket_website'] = 'Invalid URL';
			}

			($input['venue_website'] === 'http://') && $input['venue_website'] = '';
			if( strlen($input['venue_website']) && ! \Helpers::validate_url($input['venue_website']) )
			{
					$errors['venue_website'] = 'Invalid URL';
			}

			if (empty($errors))
			{
				// Do insert
				$input['start_time'] = \Helpers::gmt_from_user_time(strtotime($input['date'] . $start_time));
				$input['end_time'] = \Helpers::gmt_from_user_time(strtotime($input['date'] . $end_time));

				unset($input['date']);
				$model->values($input)->save();

				return \Response::redirect(\Uri::create('/events/show/'.$model->id));
			}
			else
			{
				$data['errors'] = $errors;
			}
		}

		$data['fields'] = $input;
		$data['errors'] = $errors;
		\Helpers::load_js('raw', 'redzu.pages.eventCalendar()');
		$this->template->content = \View::forge('events/update.twig', $data);	
	}

	public function action_create()
	{
		return $this->update_event();
	}

	public function action_update( $id = null )
	{
		if ($id && $model = \Model_Event::find($id))
		{
			return $this->update_event($model);
		}

		return \Request::show_404();
	}

	public function action_calendar( $month = null, $day = null )
	{
		$current_time = \Helpers::user_time(time());
		$current_date = array
		(
			'month'      => date('n', $current_time),
			'month_full' => date('F', $current_time),
			'day'        => date('j', $current_time),
			'year'       => date('Y', $current_time),
		);

		if( ! $month || $month < 1 || $month > 12 )
		{
			$month = $current_date['month'];
		}

		$day or $month == $current_date['month'] and $day = date('j');

		$selected_time = \Helpers::user_time(strtotime("$month/1/".$current_date['year']));

		$selected_date = array
		(
			'first_day'  => date('D', $selected_time),
			'month'      => date('n', $selected_time),
			'month_full' => date('F', $selected_time),
			'day'        => (integer)$day,
			'year'       => $current_date['year'],
		);

		$data = array();
		$days = array_flip(range(1, \Date::days_in_month($selected_date['month'], $selected_date['year'])));
		foreach ($days as $k => &$v)
		{
			$start = $selected_time + (($k-1) * \Helpers::DAYS);
			$end = $selected_time + (($k) * \Helpers::DAYS) - 1;
			$v = array('num' => $k, 'range' => array($start, $end), 'name' => date( 'l', $selected_time + $start ));

			$v['events'] = \Model_Event::find()->where(array(
				array('start_time', '>=', $start),
				array('end_time', '<=', $end),
			))->limit(4)->get();
		}

		switch($selected_date['first_day'])
		{
			case 'Sun':	$blank = 0; break;
			case 'Mon': $blank = 1; break;
			case 'Tue': $blank = 2; break;
			case 'Wed': $blank = 3; break;
			case 'Thu': $blank = 4; break;
			case 'Fri': $blank = 5; break;
			case 'Sat': $blank = 6; break;
		}

		$weeks = array();

		for($i = 0; $i < $blank; $i++) {
			array_unshift($days, null);
		}

		$blank_right = 35 - sizeof($days);
		for($i = 0; $i < $blank_right; $i++) {
			array_push($days, null);
		}

		$weeks = array_chunk($days, 7);

		$data['view'] = 'calendar';
		$data['current_time'] = $current_time;
		$data['month'] = array();
		$data['month']['num'] = $selected_date['month'];
		$data['month']['name'] = $selected_date['month_full'];
		$data['month']['weeks'] = $weeks;
		$data['month']['selected_day'] = $selected_date['day'];

		$this->javascripts[] = 'eventcalendar.js';
		$this->template->content = \View::forge('events/index.twig', $data);
	}

	public function action_list()
	{
		$args = func_get_args();
		return call_user_func_array(array($this, 'action_index'), $args);
	}

	public function action_index( $month = null, $day = null )
	{
		return $this->action_calendar($month, $day);
	}	
}