<?php
/**
 * Customized Controller class
 *
 * Mixins + Events
 *
 * @author Samuel Hunter
 */
class Controller extends Fuel\Core\Controller
{
	/**
	 * Stores instances of mixin classes
	 * @access  private
	 * @var     array
	 */
	private $mixins = array();

	/**
	 * 'before' event
	 *
	 * @param void
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$this->register_mixins();
		$this->__call( 'before', func_get_args() );
	}

	/**
	 * 'after' event
	 *
	 * @param  Response $response
	 * @return void
	 */
	public function after( $response )
	{
		parent::after( $response );
		$this->__call( 'after', func_get_args() );
	}

	/**
	 * Register mixins from class properties
	 *
	 * @param  void
	 * @return void
	 */
	public function register_mixins()
	{
		$ref = new ReflectionClass( $this );
		$allProps = $ref->getProperties();		

		foreach( $allProps as $k => $v )
		{
			$var_name = $v->name;

			if(
				strpos(strtolower($var_name), 'mixin_') === 0
				&& $this->$var_name !== false
			)
			{
				$class = ($this->$var_name) ?: substr($var_name, 6);
				$this->mixins[] = new $class( $this->request, $this->response );
			}
		}
	}

	/**
	 * Magically call methods on mixin classes that
	 * are undefined in the called class
	 *
	 * @param string
	 * @param array
	 * @return void
	 * @throws BadMethodCallException
	 */
	public function __call( $method, $args )
	{
		foreach( $this->mixins as $class ) {
			if( method_exists( $class, $method ) ) {
				return call_user_func_array(array($class, $method), $args);
			}
		}

		if( ! (method_exists( $this, $method ) || $called) ) {
			throw new \BadMethodCallException('Invalid method: '.get_called_class().'::'.$method);
		}
	}
}