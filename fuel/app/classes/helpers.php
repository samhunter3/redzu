<?php

/**
 * Helpers!!!!11
 *
 * Some general helpers to help with Redzu development
 *
 * @package Helpers
 * @author  Samuel Hunter
 * @link    http://www.redzu.com
 */

class Helpers
{
	const SECONDS = 1;
	const MINUTES = 60;
	const HOURS = 3600;
	const DAYS = 86400;
	const YEARS = 31536000;

	/**
	 * @var  array  List of months, Jan-Dec
	 */
	static protected $_all_months;

	/**
	 * @var  array  Some globals for the templates
	 */
	static protected $_globals = array();

	static protected $_enqued_scripts = array();

	/**
	 * Promotes and preserves \Fuel\Core\Asset::img
	 *
	 * Additional features:
	 *   - Seamless image resizing
	 *   - Option to return URL rather than HTML
	 *
	 * @access public
	 * @category Fuel, Twig
	 * @param string $img Path to image
	 * @param array $attr Use ['width'] and ['height'] for resizing
	 * @param mixed $group
	 * @return string Image tag HTML
	 */
	static public function img($image = '', $attr = array(), $group = NULL)
	{

		$resize = null;
		if( ! isset($attr['resize']) )
		{
			$resize = array(
				'height' => \Arr::get($attr, 'height'),
				'width'  => \Arr::get($attr, 'width'),
			);

			if (! is_numeric($resize['height'])) unset($resize['height']);
			if (! is_numeric($resize['width'])) unset($resize['width']);
		}

		try
		{
			return \Asset::img( static::img_path( $image, $resize ), $attr, $group );
		}
		catch( FuelException $e )
		{
			return '';
		}
	}

	static public function get_like_button($type = '$GLOBAL', $id = '')
	{
		$uid = static::my_uid();

		$likes = \Social\Model_Like::find()->where(array('object_type' => $type, 'object_id' => $id));

		$count = $likes->count();
		$action = $likes->where(array('user_id' => $uid))->count() ? 'unlove' : 'love';
		
		$text = $count == 0 ? 'Like' : $count . ' ' . ($count !== 1 ? 'Likes' : 'Like');

		return '<span data-object-id="'.$id.'" data-object-type="'.$type.'"  class="im-icon im-action-like im-icon-' . $action . ' show-text">'.$text.'</span>';
	}

	public static function user_time($time, $offset = null)
	{
		$offset or $offset = static::get_global('gmtoffset');
		return (integer) ($time - ($offset * static::HOURS));
	}

	public static function gmt_from_user_time($time, $offset = null)
	{
		$offset or $offset = static::get_global('gmtoffset');
		return (integer) ($time + ($offset * static::HOURS));
	}

	public static function load_js( $type = null, $javascript = null )
	{
		if( $type === null )
		{
			$scripts = '';
			foreach( static::$_enqued_scripts as &$script )
			{
				list($type, $javascript) = $script;
				if($type === 'raw') {
					$scripts .= '<script type="text/javascript" data-rel="enqued">'.$javascript.'</script>' . PHP_EOL;
				}
			}

			return $scripts;
		}

		static::$_enqued_scripts[] = array($type, $javascript);
	}

	public static function get_flashes()
	{
		$flashes = array_flip(array('error','notice','success', 'info'));
		foreach( $flashes as $k => &$v ) {
			$v = \Session::get_flash($k);
		}
		
		return $flashes;
	}

	public function get_multiple_meta()
	{
		$keys = func_get_args();
		$user = array_shift($keys);

		sizeof($keys) && is_array(reset($keys)) && $keys = reset($keys);

		if (! is_object($user) && is_numeric($user))
		{
			$user = \Model_User::get($user->id);
		}

		return \Model_User_Meta::find()->where(array(
			array('user_id', $user)
		) + $keys)->get();
	}

	public static function set_pfuser($user)
	{
		$pfuser = $user->to_array();
		$pfuser['photos'] = array( $user->get_default_photo() );
		$pfuser['is_online'] = $user->online_status();
		$pfuser['age'] = $user->get_meta('age');
		$pfuser['gender'] = ucfirst($user->get_meta('gender'));
		$pfuser['location_city'] = $user->get_meta('location_city');
		$pfuser['sexual_orientation'] = ucfirst($user->get_meta('sexual_orientation'));
		$pfuser['relationship_status'] = ucfirst(strtr($user->get_meta('relationship_status'), '_', ' '));
		$pfuser['location_state'] = $user->get_meta('location_state');
		$pfuser['photos'] = array_merge( $pfuser['photos'], $user->photos(0)
			->where(array( array('user_id', '=', $user->id), array('album', '!=', 'redzu-default') ))
			->limit(1)->get() );
		$pfuser['basic_info'] = false;

		$my_profile = static::my_uid($user->id);
		\View::set_global('my_profile', $my_profile);
		\View::set_global('can_edit_profile', static::can_use_uid($user->id));


		$pfuser['completed_profile'] = 100;
		$completed_profile = $user->searchable;
		if( $my_profile and ! $completed_profile )
		{
			$evaluations = array
			(
				'have_an_account' =>
					isset($user) ?: 'Something bad happend with registration',

				'age_sex_location' =>
					function () use (&$user, &$pfuser) {
						$pass = $user->get_meta('gender', null)
							and $user->get_meta('sexual_orientation', null)
							and $user->get_meta('location_zipcode', null)
							and $user->get_meta('relationship_status', null)
							and is_array($user->get_meta('birthdate', null))
							and is_float($user->get_meta('location_latitude', null))
							and is_float($user->get_meta('location_longitude', null));

						($pfuser['basic_info'] = $pass)
						or \Helpers::load_js('raw', 'redzu.update_profile_info();');

						return (bool)$pass ?: array('javascript:redzu.update_profile_info();', '2. We need your age, sex &amp; location');
					},

				'photo_exists' => 
					($user->photos('count') > 0 ?: array(\Uri::create('me/photos'), '3. Upload a photo')),

			);

			$evaluations_failed = array();
			foreach( $evaluations as $e_key => &$evaluated )
			{
				is_callable($evaluated) and $evaluated = $evaluated();

				if( $evaluated !== true )
				{
					$evaluations_failed[$e_key] = $evaluated;
					!isset($pfuser['next_step']) and $pfuser['next_step'] = $evaluated;
				}
			}

			$pfuser['completed_profile'] = (integer)( floor( (sizeof($evaluations) - sizeof($evaluations_failed)) / sizeof($evaluations) * 100 ) );

			if( $pfuser['completed_profile'] === 100 )
			{
				$pfuser['searchable'] = $user->searchable = 1;
				$user->save();

				\Session::set_flash('dialog', array('Thanks for finishing your profile! You can now be found in the search!'));
			}
		}

		$completed_profile and $pfuser['basic_info'] = true;
		
		\View::set_global('pfuser', $pfuser, true);
		\View::set_global('profile_link', $my_profile ? 'me' : "profile/".strtolower($pfuser['username']));


		return $pfuser;
	}

	static public function get_view_model($view, $vars = array(), $method = 'view')
	{
		$vmodel = \ViewModel::forge('\\View_'.$view, $method);
		foreach( $vars as $k => &$v )
		{
			$vmodel->set($k, $v);
		}

		return $vmodel;
	}
	
	static public function get_view_module($view, $vars = array())
	{
		$method = 'view';
		if($pos = strpos($view, '#')) {
			$method = substr($view, $pos+1);
			$view = substr($view, 0, $pos);
		}

		$view = 'Module_'. strtr($view, ' -', '__');
		$method = strtr($method, ' -', '__');
		return static::get_view_model($view, $vars, $method);
	}

	static public function get_likes_count()
	{
		$args = func_get_args();
		$model = array_shift($args);
		$id = sizeof($args) ? array_shift($args) : null;

		return \Social\Model_Like::get_likes($model, $id)->count();
	}

	static public function singular($var)
	{
		$var = strtolower($var{0});
		return in_array($var{0}, array('a','e','i','o','u')) ? 'an' : 'a';
	}

	static public function has_access( $def = null )
	{
		return \Auth::has_access( static::get_request_acl($def) );
	}

	static public function get_request_acl( $request = null )
	{
		$def = $request;

		if (null === $request)
		{
			$request = \Request::active();
		}
		
		if (is_object($request) && $request instanceof \Request)
		{
			$def = array();
			$def[0] = strtolower($request->route->segments[0]);
			$def[1] = strtolower($request->action);

			if (isset($request->route->segments[1]) && ($seg2=strtolower($request->route->segments[1])) !== $def[1])
			{
				$def[1] = $seg2 . '/' . $def[1];
			}
		}
		
		return $def;
	}

	/**
	 * If active
	 *
	 * @return mixed "active" or null
	 */
	static public function route_active($route)
	{
		$routes = func_get_args();
		$request_route = implode('/', Request::active()->route->segments);
		
		if( substr_count($request_route, '/index') === 0 )
		{
			$request_route .= '/index';
		}

		foreach( $routes as $r_k => $r_v )
		{
			if( strpos($request_route, $r_v) === 0 )
			{
				return 'active';
			}
		}

		return null;
	}

	/**
	 * All profile fields
	 *
	 * @return array
	 */
	static public function get_metafields($group = null)
	{
		return \Model_Metafield::find()->where(array('group' => $group))->order_by('weight', 'desc')->get();
	}

	/**
	 * Get global value from keystore
	 *
	 * @param  string $key
	 * @return mixed Value of Helpers::$_globals[ $key ]
	 */
	static public function get_global( $key, $default = null, $default_empty = 0 )
	{
		$key = explode( '.', $key );
		$val = static::$_globals;

		try
		{
			foreach( $key as $k => $v )
			{
				if( ! isset($val[$v]) )
				{
					return $default;
				}

				$val = $val[$v];
			}

			return ($default_empty && empty($val)) ? $default : $val;
		}
		catch( Exception $e )
		{
			return $default;
		}
	}

	/**
	 * Access user meta in templates
	 *
	 * @param  mixed $user
	 * @param  string|int $key
	 * @param  string $key
	 * @return mixed $value for conveniance, who needs Booleans?
	 */
	static public function user_data( $user, $key, $default = '' )
	{
		$user = static::get_user($user);
		if( $user && Model_Metafield::find()->where('meta', $key)->limit(1)->count() )
		{
			return Helpers::format_profilefield( $key, $user->get_meta($key, $default) );
		}

		return $default;
	}

	static public function format_profilefield($key, $val)
	{
		$classn = '\Redzu_Metafield_'.\Inflector::words_to_upper($key);
		if( class_exists($classn) && method_exists($classn, 'format') )
		{
			return ($cl = call_user_func($classn.'::format', $val)) !== null ? $cl : $val;
		}

		return $val;
	}

	/**
	 * Set global value in keystore
	 *
	 * @param  string $key
	 * @param  string [$value]
	 * @return mixed  Returns the value that was set
	 */
	static public function set_global( $key, $value = null )
	{
		$key = explode( '.', $key );
		$val =& static::$_globals;

		try
		{
			$size = sizeof($key) - 1;
			foreach( $key as $k => $v )
			{
				if( ! isset($val[$v]) && $k < $size )
				{
					$val[$v] = array();
				}

				$val =& $val[$v];
			}

			return ($val = $value) ?: true;

		}
		catch( Exception $e )
		{
			return null;
		}
	}

	/**
	 * Validate URL
	 *
	 * @param  string $value The supposed URL to validate
	 * @return array|false Returns parsed URL if valid
	 */
	static public function validate_url( $value )
	{
		return filter_var( $value, FILTER_VALIDATE_URL ) ? parse_url( $value ) : false;
	}

	/**
	 * Validate Email Address
	 *
	 * @param string $email Email address
	 * @return bool Is this a valid email?
	 */
	static public function validate_email( $email )
	{
		return filter_var( $email, FILTER_VALIDATE_EMAIL );
	}

	/**
	 * Get path of image relative to assets image directory
	 *
	 * Can optionally resize the image
	 *
	 * @access public
	 * @param string $image Image path to do stuff with
	 * @param array|bool [$resize] Array with 'width' and/or 'height' values if you want to resize
	 * @return string
	 */
	static public function img_path( $image, $resize = array() )
	{
		$asset_paths = Config::get('asset.paths');
		$asset_image_dir = Config::get('asset.img_dir');
		$is_url = static::validate_url( $image );

		$height = \Arr::get($resize, 'height');
		$width = \Arr::get($resize, 'width');

		if (! $is_url)
		{
			if ($height || $width)
			{
				$image = static::resize_image( $image, $width, $height );
			}

			foreach ($asset_paths as $k => $v)
			{
				$l = trim($v . $asset_image_dir, '/');
				$strp = strpos( $image, $l );
				if( $strp === 0 )
				{
					$image = substr( $image, strlen($l) );
					break;
				}
			}
		}

		return $image;
	}

	/**
	 * Get URL of asset with cache-busting parameters, awwwwwwh yeah.
	 *
	 * Can optionally resize the image
	 *
	 * @access public
	 * @param string $image Path to image, can be relative to asset paths
	 * @param array|bool [$resize] Array with 'width' and/or 'height' values if you want to resize
	 * @return string URL of image
	 */
	static public function img_url( $image, $resize = array() )
	{
		if( static::validate_url( $image ) )
		{
			return $image;
		}

		$image = \Asset::find_file(static::img_path($image, $resize), 'img') ?: $image;
		$attach_url = file_exists(DOCROOT.$image) ? '?'.filemtime(DOCROOT.$image) : '';
		$image = \Uri::create( $image ) . $attach_url;

		return $image;
	}

	/**
	 * Get User Model from Object
	 *
	 * Accepts username, ID, and user models and returns a user model
	 * if no parameters passed it will return a model of the current user
	 *
	 * @access public
	 * @param mixed [$oUser] An object representing a user
	 * @return mixed User Object or false, empty array, null if failed
	 */
	static public function get_user( $oUser = null )
	{
		if( $oUser === null && ($uid = \Helpers::my_uid()) )
		{
			$oUser = (integer) $uid;
			return \Model_User::find($oUser);
		}

		if( is_numeric($oUser) )
		{
			return \Model_User::find( (integer) $oUser );
		}
		else if( is_string($oUser) )
		{
			return \Model_User::find()->where('username', $oUser)->get_one();
		}

		return is_object($oUser) ? $oUser : false;
	}

	static public function my_uid( $uid = null )
	{
		list($driver, $my_uid) = \Auth::get_user_id();
		if( $my_uid and ($uid === null or $uid === 'me' or (string)$uid === (string)$my_uid) )
		{
			return $my_uid;
		}

		return false;
	}

	static public function can_use_uid( $uid = 'me' )
	{
		if( $this_user = static::my_uid($uid) )
		{
			return $this_user;
		}
		elseif( \Auth::instance()->has_access( array('admin', 'index') ) )
		{
			return (integer)	$uid;
		}

		return false;
	}

	static public function get_thumb_prefix( $original_image_path )
	{
		$original_image_path = strtolower(strtr($original_image_path, DS, '/'));
		$docroot = strtolower(strtr(DOCROOT, DS, '/'));

		return md5(substr($original_image_path, ($b=strpos($original_image_path, $docroot) === 0) ? $b+1 : 0));
	}

	static public function get_thumbnails_of( $original_image_path, $export = null )
	{
		if( $export === null )
		{
			$asset_paths = Config::get('asset.paths');
			$export = reset($asset_paths) . Config::get('asset.img_dir') . "gd";
		}


		return glob($export.'/' . static::get_thumb_prefix( $original_image_path ) .  '_*');
	}

	static public function get_user_sexuality()
	{
		
	}

	/**
	 * Doesn't resize images or anything..
	 * 
	 * Okay, just kidding it does.
	 *
	 * @access public
	 * @param string $image_path
	 * @param int [$width]
	 * @param int [$height]
	 * @param string [$export] GD Output directory
	 * @return void
	 */
	static public function resize_image( $image_path, $width = null, $height = null, $export = null )
	{
		try
		{
			$image_path = \Asset::find_file( $image_path, 'img' ) ?: $image_path;
			$image_split = explode( '.', $image_path );
			$image_ext = end($image_split);

			list($image_width, $image_height) = @getimagesize($image_path);

			if (is_array($width))
			{
				$width = $width[1] > $image_width ? $image_width : $width[1];
			}
			if (is_array($height))
			{
				$height = $height[1] > $image_height ? $image_height : $height[1];
			}

			if( $export === null )
			{
				$asset_paths = Config::get('asset.paths');
				$export = reset($asset_paths) . Config::get('asset.img_dir') . "gd";
			}

			$fname = static::get_thumb_prefix($image_path)."_".substr(md5(serialize(array($width, $height))), 0, 9);

			$thumbnail_image = trim($export, '/').'/'.$fname.'.'.$image_ext;

			if( ! file_exists( DOCROOT.$image_path ) )
			{
				throw new FuelException("Image does not exist \"{$image_path}\" and can not be resized");
			}
			elseif( ! file_exists( DOCROOT.$thumbnail_image ) )
			{
				$thumbnail_directory = dirname($thumbnail_image);

				if (! is_dir($thumbnail_directory))
				{
					mkdir ($thumbnail_directory, true);
					chmod ($thumbnail_directory, 0755);
				}

				$img = \Image::forge()
					->load( DOCROOT . $image_path );

				$width and $img->crop_resize($width, $height)
					or $img->resize($width, $height, false, false);

				$img->save( DOCROOT . $thumbnail_image );
			}

			return $thumbnail_image;
		}
		catch( FuelException $e )
		{
			return $image_path;
		}
	}

	/**
	 * Lazy POST data filtering
	 *
	 * @access public
	 * @param array|mixed $fields Fields to be filtered
	 * @param array|mixed $allowed Whitelist of keys to allow presence in fields 
	 * @param bool|int $fill Fills fields with keys that are empty and present in whitelist
	 * @param bool|int $strings_only Only allow strings in fields
	 * @return int
	 */
	static public function filter_fields( $fields, $allowed, $fill = false, $strings_only = true )
	{
		/**
		 * Attempt to convert our whitelist to array to allow
		 * instances of stdClass for example..
		 *
		 * @todo Recursive typecasting, stop being lazy Sam.
		 */
		$allowed = (array) $allowed;

		/**
		 * Remove values from $fields which keys are not
		 * whitelisted (Array $allowed)
		 */
		$fields = array_intersect_key( $fields, array_flip($allowed) );

		/**
		 * Filter for values that are not strings
		 *
		 * Useful if $fields is POST data that can contain
		 * multidimensional array values, super uncool bro..
		 */
		if( $strings_only )
		{
			foreach( $fields as $k => &$v )
			{
				if( !is_string($v) )
				{
					unset( $fields[$k] );
				}
			}
		}

		/**
		 * Force empty string values on allowed keys
		 * that are not set in $fields array
		 */
		if( $fill )
		{
			foreach( $allowed as $k2 => $v2 )
			{
				if( ! isset( $fields[$v2] ) )
					$fields[$v2] = '';
			}
		}
		
		return $fields;
	}

	/**
	 * Converts underscored text to words
	 *
	 * @param string $underscored
	 * @return string
	 */
	static public function underscore_to_words( $underscored )
	{
		return ucwords( strtr( $underscored, '_', ' ' ) );
	}

	/**
	 * Get list of animals
	 *
	 * @param void
	 * @return array
	 */
	static public function animals_list()
	{
		$animals = Redzu_Animals::get_all();
		foreach( $animals as $k => &$animal )
		{
			$animal['name'] = static::underscore_to_words( $k );
		}

		return $animals;
	}

	/**
	 * Upload files and return s3 URL
	 *
	 * @param string $files Absolute path to file
	 * @todo Document
	 */
	static public function handle_upload_s3( $files, $bucket = 'redzu_users', $uri )
	{
		$urls = array();
		$files = (array) $files;
		foreach( $files as $file )
		{
			if( ! file_exists($file) )
			{
				return false;
			}

			$input = S3::inputFile( $file );
			if (S3::putObject($input, $bucket, $uri, S3::ACL_PUBLIC_READ)) {
				if( $urls )
				{
					$urls[ $file ] = $url;
				}
			}
	    }
	}

	/**
	 * Get age in years from date
	 *
	 * @access public
	 * @todo Achieve unix timestamp of DOB with \Fuel\Core\Date
	 * @param int|string $month_or_date Numeric value of month or timestamp
	 * @param int|string $day 
	 * @param int|string $years
	 * @return int
	 */
	static public function years_old( $month_or_date, $day = null, $year = null )
	{
		if( is_array($month_or_date) && 3 === sizeof($month_or_date) )
		{
			return call_user_func_array('\Helpers::years_old', $month_or_date);
		}

		$nowtime = \Date::forge()->get_timestamp();
		$day and $year and $month_or_date = "$month_or_date/$day/$year";
		is_string($month_or_date) and $month_or_date = strtotime($month_or_date);
		
		return floor( ($nowtime - $month_or_date) / static::YEARS );
	}

	/**
	 * Populate Views and ViewModels with data simply
	 *
	 * @access public
	 * @param View|ViewModel $view
	 * @param array $data The data to push to the View object
	 * @param bool $filter Filter the data using uri_filter
	 * @return int
	 */
	static public function populateView( View &$view, Array $data, $filter = true )
	{
		foreach( $data as $k => &  $v )
		{
			$view->set( $k, $v, $filter );
		}
	}

	static public function truncate($str, $maxlength = 26)
	{
		if(strlen($str) > $maxlength) {
			return substr($str, 0, abs($maxlength)-3) . '...';
		}

		return $str;
	}

	/**
	 * Get a list of all the months
	 *
	 * @access public
	 * @param void
	 * @return array
	 */
	static public function get_all_months()
	{
		if( static::$_all_months )
		{
			return static::$_all_months;
		}

		return static::$_all_months = array
		(
			1 => 'January',
			2 => 'February',
			3 => 'March',
			4 => 'April',
			5 => 'May',
			6 => 'June',
			7 => 'July',
			8 => 'August',
			9 => 'September',
			10 => 'October',
			11 => 'November',
			12 => 'December',
		);
	}
}