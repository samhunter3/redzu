<?php

class Controller_Pages extends Controller_Template_Default
{
	public function action_tos()
	{
		$this->title = 'Terms of Service | Redzu';
		$this->template->content = \View::forge('pages/tos.twig');
	}
}