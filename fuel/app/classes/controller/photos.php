<?php

use \Social\Model_Photo;

class Controller_Photos extends \Controller_Template_Default
{
	protected $_photos_url = 'me/photos';
	protected $_view = 'users/photos.twig';

	public function before()
	{
		parent::before();
	}

	private function get_user_photo($uid = null, $pid = null)
	{
		return Model_Photo::find()->where(array('id' => $pid, 'user_id' => $uid))->get_one();
	}

	public function action_index()
	{
		return $this->action_show();
	}

	public function ajax_setdefault( $uid = null, $id = null )
	{
		if( ! ($uid = \Helpers::can_use_uid($uid)) )
		{
			return array('error' => 'Permission denied');
		}

		if( $photo = $this->get_user_photo($uid, $id) )
		{
			$photo->album = 'redzu-default';
			
			$old_default = \Social\Model_Photo::find()->where(array(
				'object_id' => $photo->object_id,
				'object_type' => $photo->object_type,
				'user_id' => $uid,
				'album' => 'redzu-default',
			))->get_one();

			$old_default->album = '';
			$old_default->save();

			if( $photo->save() )
			{
				return array('message' => 'Success');
			}
		}

		return array('error' => 'Photo does not exist');
	}

	public function ajax_edit( $uid = null, $id = null )
	{
		if( ! ($uid = \Helpers::can_use_uid($uid)) )
		{
			return array('error' => 'Permission denied');
		}

		if( $photo = $this->get_user_photo($uid, $id) )
		{
			$description = (string) \Input::post('description');
			$photo->description = substr($description, 0, 255);

			if( $photo->save() )
			{
				return array('description' => $description);
			}
		}

		return array('error' => 'Photo does not exist');
	}

	public function ajax_delete( $uid = null, $id = null )
	{
		if( ! ($uid = \Helpers::can_use_uid($uid)) )
		{
			return array('error' => 'Permission denied');
		}

		if( ($photo = $this->get_user_photo($uid, $id)) && $photo->delete() )
		{
			if( $this->object_type === 'user' && ! Model_Photo::find()->where(array('object_type' => 'user', 'object_id' => $uid))->limit(1)->count() )
			{
				\Model_User::find($uid)->set('searchable', 0)->save();
			}
			return array('message' => 'Photo deleted');
		}
		
		return array('error' => 'Photo does not exist');		
	}

	public function action_preview( $pid = null )
	{
		if( ! ($photo = Social\Model_Photo::find($pid)) )
		{
			return \Request::show_404();
		}

		$data = array();
		$data['edit'] = ($uid = \Helpers::can_use_uid($photo->user_id));
	}

	public function action_upload()
	{
		$files = \Input::file();
		$uid = $this->user_id;
		$whitelist = array('gif', 'jpg', 'jpeg', 'png', '');

		$url_path = 'assets/img/uimg/';
		$upload_dir = DOCROOT.strtr($url_path, '/', DS);

		foreach( $files as $k => $file )
		{
			$ext = explode(".", $file['name']);
			$ext = strtolower(@$ext[ sizeof($ext) - 1 ]);
			$success = false;

			if( in_array($ext, $whitelist) )
			{
				$new_fname = $uid.'_'.md5(rand(0,9999).base64_encode($this->logged_in.$file['name']).$file['tmp_name']).'.'.$ext;

				if( ! is_dir($upload_dir) )
				{
					mkdir( $upload_dir, 0777, true );
				}

				move_uploaded_file( $file['tmp_name'], $upload_dir.$new_fname );

				$success = Model_Photo::factory(array(
					'user_id' => $uid,
					'object_type' => $this->object_type,
					'object_id' => $this->object_id,
					'title' => $file['name'],
					'url'   => $url_path . $new_fname,
					'filename' => $file['name'],
					'album' => '',
					'description' => '',
				))->save();
			}

			if($success)
			{
				\Session::set_flash('upload_success', 'Photo has been added to your profile!');
			}
			else
			{
				\Session::set_flash('upload_error', 'Are you sure that was a photo?');
			}
			@unlink( $file['tmp_name'] );
		}

		\Response::redirect( \Uri::create($this->_photos_url) );
		return;
	}
	
	public function action_show()
	{
		$data = array();
		$data['photos'] = Social\Model_Photo::find()->where(array(
			array('object_id', $this->object_id),
			array('object_type', $this->object_type)
		))->get();

		$object_model = '\\Model_'.\Inflector::classify($this->object_type);
		$object = $object_model::find($this->object_id);

		if( ! $object )
		{
			return \Request::show_404();
		}
		
		if($this->object_type === 'user') {
			$userid = $object->id;
		} 
		else {
			$userid = $object->user_id;
		}

		$data['flash_error'] = \Session::get_flash('upload_error');
		$data['flash_success'] = \Session::get_flash('upload_success');
		$data['default_photo'] = Social\Model_Photo::get_default_photo($this->object_id, $this->object_type);
		$data[$this->object_type] = $object;

		$data['photos_link'] = $this->_photos_url;

		if( $data['edit'] = \Helpers::can_use_uid($userid) )
		{
			$this->javascripts[] = 'edit_photos.js';
		}

		$this->on_render($data);
	}

	public function on_render($data)
	{
		$this->template->content = \View::forge($this->_view, $data);
	}

}