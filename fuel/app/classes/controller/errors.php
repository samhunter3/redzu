<?php

/**
 * Status Code Error Controller
 * 
 * @package  app
 * @extends  Controller
 */

class Controller_Errors extends Controller_Template_Default
{
	/**
	 * Redzu Homepage Handler
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		$this->template->content = \View::forge('errors/unknown.twig');
	}

	/**
	 * The 404 action for the application.
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		$this->response->status = 404;
		$this->template->content = \View::forge('errors/404.twig');
	}

	/**
	 * The 404 action for the application.
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_unknown()
	{
		$this->response->status = 500;
		$this->template->content = \View::forge('errors/unknown.twig');
	}
}