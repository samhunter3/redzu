<?php

class Controller_Emails extends Controller
{
	public function action_index($tmpl = null)
	{
		return $this->action_show();
	}

	public function action_show($tmpl = null)
	{
		$this->response->body = Redzu_Email::forge(array($tmpl), array());
	}
}