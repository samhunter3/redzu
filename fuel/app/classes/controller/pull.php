<?php

class Controller_Pull extends Controller_Rest
{
	protected $_result;
	protected $_uid;

	public function before()
	{
		if( $uid = \Helpers::my_uid() )
		{
			$this->_uid = $uid;
			return parent::before();
		}

		$this->_result = array('success' => 'false');
		return \Request::show_404();
	}

	public function delete_messages()
	{
		$data = array();
		$data['messages'] = array();
		$messages = (array) \Input::param('messages');
		foreach ($messages as $m_k => $m_v)
		{
			$message = \Model_Message::find($m_v);
			if ($message && $message->from_user_id == $this->_uid)
			{
				$data['messages'][$message->id] = true;
				$message->delete();
			}
			elseif ($message)
			{
				$data['messages'][$message->id] = false;
			}
		}
		$this->respond(1, $data);
	}

	public function post_messages()
	{
		$data = array();
		$user = (integer) \Input::param('user');
		$message = \Input::param('message');
		$oMessage = \Model_Message::factory(array(
			'from_user_id' => $this->_uid,
			'user_id' => $user,
			'read' => 0,
			'message' => $message,
		));

		$oMessage->save();
		$data['messages'] = $this->_sanitize_message($oMessage);
		$this->respond(1, $data);
	}

	public function _sanitize_message($messages)
	{
		if(is_object($messages) and $messages instanceof \Orm\Model)
		{
			$messages = array( $messages );
		}

		foreach($messages as &$m)
		{
			$m = $m->to_array();
			$m['created_at'] = \Date::time_ago($m['created_at']);
		}

		return $messages;
	}

	public function get_messages()
	{
		$last = (integer) \Input::param('last');
		$user = (integer) \Input::param('user');
		$data = array();
		$messages = \Model_Message::find()->where_open()
			->where(array('user_id' => $this->_uid, 'from_user_id' => $user))
			->where_close();

		if (! $last)
		{
			$messages->or_where_open()
				->where(array('user_id' => $user, 'from_user_id' => $this->_uid))
				->where_close();
		}

		$messages = $messages->where('id', '>', $last)
			->order_by('created_at', 'DESC')
			->limit(50)->get();

		foreach($messages as $message)
		{
			if ($message->user_id == $this->_uid)
			{
				$message->set('read', 1)->save();
			}
		}

		$data['last'] = $messages ? reset($messages)->id : ($last ?: 0);
		$data['messages'] = $this->_sanitize_message($messages);
		$this->respond((bool)count($messages), $data);
	}

	public function respond($success, $data = array())
	{
		if( ! is_array($data) )
		{
			$data = array('message'=>$data);
		}

		$data['success'] = (boolean) $success;
		$data['now'] = time();

		$this->_result = $data;

		if( $data['success'] ) {
			$this->response($this->_result);
		}
	}

	public function post_gmt()
	{
		$offset = \Input::post('offset', 0);
		if( is_numeric($offset) )
		{
			if( \Auth::check() )
			{
				list($driver, $uid) = \Auth::get_user_id();
				\Model_User::find($uid)->set_meta('gmtoffset', (integer)$offset);
			}
			else
			{
				\Session::set('gmtoffset', (integer)$offset);
			}

		}
		return $this->respond(1, array('gmt' => (integer)$offset));
	}

	public function get_gmt()
	{
		return $this->respond(1, array('gmt' => (integer)\Session::get('gmtoffset')));
	}

	public function get_notifications()
	{
		return $this->respond(0, null);
	}

	public function get_testusers( $amount = null )
	{
		if( $amount == 0 ) {
			return $this->respond(array('error' => 'Request /pull/testusers/{amount_of_users} to create test users'));
		}

		$b = range('a', 'z');
		$fbusers = json_decode(file_get_contents('https://graph.facebook.com/search?q='.$b[rand(0,22)].'+'.$b[rand(0,22)].'&type=user&offset=0&limit='.floor($amount*1.5).'&access_token=AAACEdEose0cBAPQdZCDgcRmdvmQrhrs5JDeHkuUhSrnCU22WF1ARdl7ZB5TpJjuL7N4v41EWDQ6y8vR1hj0Jan6FudoVfHgKBOHXOgNgZDZD'), true);

		foreach($fbusers as $k => &$v)
		{
			if($fbusers) {

			}
		}

		return $this->respond(1, $fbusers);	
	}

	public function get_index()
	{
		$this->respond(1, array('error' => 'No object requested'));
	}

	public function after($response)
	{
		list($action) = explode('.', $this->request->action);
		$controller_method = strtolower(\Input::method()) . '_' . $action;
		if( \Input::param('poll') === 'true' && method_exists($this, $controller_method) )
		{
			$polled = 0;
			while( !$this->_result['success'] && $polled++ < 6 )
			{
				sleep(1);

				list($actions, $arguments) = $this->request->method_params;
				call_user_func_array(array($this, $controller_method), $arguments);
			}

			$this->_result['poll'] = array($action, $polled);
		}

		$this->response($this->_result);
		parent::after($response);
		return $response;
	}
}