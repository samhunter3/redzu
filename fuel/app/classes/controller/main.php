<?php

/**
 * Redzu Home
 * 
 * @package  app
 * @extends  Controller
 */

class Controller_Main extends Controller_Template_Default
{

	/**
	 * Redzu Homepage Handler
	 * 
	 * @access  public
	 * @return  void
	 */
	public function action_index()
	{
		if( $this->logged_in )
		{
			$response = \Request::forge('users/index')->execute()->response();
			\Event::shutdown();
			$response->send(true);
			exit;
		}

		$this->template->title = 'Redzu';
		$this->template->content = \View::forge('main/index.twig');
	}
}
