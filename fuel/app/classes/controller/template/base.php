<?php

class Controller_Template_Base extends \Controller_Template
{
	public $template = 'templates/default.twig';
	public $javascripts = array();
	public $css = array();

	public function before()
	{
		parent::before();

		$this->register_globals();		
		if( ! (isset($this->twig) && $this->twig) )
		{
			static::register_helpers();
		}

		// Default template
		$this->template->title = 'Redzu';
	}

	public function after($response)
	{
		parent::after($response);

		// Controller javascript assets
		$this->template->set('javascript_tags', implode(
			'', array_map('\Asset::js', $this->javascripts)
		), false);

		// Controller CSS assets
		$this->template->set('css_tags', implode(
			'', array_map('\Asset::css', $this->css)
		), false);

		return $response;
	}

	public function register_globals()
	{
	}

	static public function register_helpers()
	{
		$twig = \Parser\View_Twig::parser();

		$twig->addFunction('input_get', new Twig_Function_Function('Input::get'));
		$twig->addFunction('input_post', new Twig_Function_Function('Input::post'));
		$twig->addFunction('has_access', new Twig_Function_Function('\Helpers::has_access'));

		// Asset helpers
		$twig->addFunction('img', new Twig_Function_Function('Helpers::img'));
		$twig->addFunction('img_url', new Twig_Function_Function('Helpers::img_url'));
		$twig->addFunction('time_ago', new Twig_Function_Function('\Date::time_ago'));
		$twig->addFunction('get_global', new Twig_Function_Function('Helpers::get_global'));
		$twig->addFunction('singular', new Twig_Function_Function('Helpers::singular'));
		$twig->addFilter('chunk', new Twig_Filter_Function('array_chunk', array()));
		$twig->addFilter('substr', new Twig_Filter_Function('substr', array()));
		$twig->addFilter('truncate', new Twig_Filter_Function('\Helpers::truncate', array()));
		$twig->addFilter('years_old', new Twig_Filter_Function('\Helpers::years_old', array()));
		$twig->addFilter('time_ago', new Twig_Filter_Function('\Date::time_ago', array()));
		$twig->addFunction('route_active', new Twig_Function_Function('Helpers::route_active'));
		$twig->addFunction('get_metafields', new Twig_Function_Function('Helpers::get_metafields'));
		$twig->addFunction('user_data', new Twig_Function_Function('Helpers::user_data'));
		$twig->addFunction('set_global', new Twig_Function_Function('Helpers::set_global'));
		$twig->addFunction('js', new Twig_Function_Function('\Asset::js'));
		$twig->addFunction('css', new Twig_Function_Function('\Asset::css'));
		$twig->addFunction('set_flash', new Twig_Function_Function('\Session::set_flash'));
		$twig->addFunction('get_flash', new Twig_Function_Function('\Session::get_flash'));
		$twig->addFunction('load_js', new Twig_Function_Function('\Helpers::load_js'));
		$twig->addFunction('get_url', new Twig_Function_Function('\Uri::current'));
		$twig->addFilter('nl2br', new Twig_Filter_Function('strtoupper'));

		$twig->addFunction('get_comments', new Twig_Function_Function('Helpers::get_comments'));
		$twig->addFunction('get_flashes', new Twig_Function_Function('Helpers::get_flashes'));
		$twig->addFunction('get_like_button', new Twig_Function_Function('Helpers::get_like_button'));
		$twig->addFunction('include_viewmodel', new Twig_Function_Function('\Helpers::get_view_model'));
		$twig->addFunction('include_module', new Twig_Function_Function('\Helpers::get_view_module'));
	}
}