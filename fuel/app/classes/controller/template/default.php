<?php

class Controller_Template_Default extends Controller_Template_Base
{
	public $template = 'templates/default.twig';
	public $javascripts = array('pull.js', 'slider.js', 'tweet.js', 'jquery-ui-1.8.18.custom.js', 'scripts.js');
	public $css = array('style.css', 'jquery-ui/redzu/jquery-ui-1.8.18.custom.css');
	public $logged_in = false;

	static protected function do_auth( Controller $instance )
	{

		
		\Model_Metafield::insert_unique(
			array(
				'meta' => 'about_me',
			),
			array(
				'meta' => 'occupation',
				'group' => 'userspecs',
			),
			array(
				'meta' => 'height',
				'group' => 'userspecs',
			),
			array(
				'meta' => 'weight',
				'group' => 'userspecs',
			),
			array(
				'meta' => 'education',
				'group' => 'userspecs',
			),
			array(
				'meta' => 'body_type',
				'group' => 'userspecs',
				'options' => array("default" => '', "Rather not say", "Thin", "Overweight", "Skinny", "Average",
					"Fit", "Athletic", "Jacked", "A little extra", "Curvy", "Full figured", "Used up"),
				'default' => 'default',
			),
			array(
				'meta' => 'smokes',
				'group' => 'userspecs',
				'options' => array("default" => '', "Yes", "Sometimes", "When drinking", "Trying to quit", "No"),
				'default' => 'default',
			),
			array(
				'meta' => 'drinks',
				'group' => 'userspecs',
				'options' => array("default" => '', "Very often", "Often", "Socially", "Rarely", "Desperately", "Not at all"),
				'default' => 'default',
				
			),
			array(
				'meta' => 'drugs',
				'group' => 'userspecs',
				'options' => array("default" => '', "Never", "Sometimes", "Often"),
				'default' => 'default',
			),

			array(
				'meta' => 'sign',
				'group' => 'userspecs',
				'options' => array("Aquarius", "Pisces", "Aries", "Taurus", "Gemini", "Cancer", "Leo", "Virgo", "Libra", "Scorpio", "Sagittarius", "Capricorn"),
				'default' => 'default',
			),
			array(
				'meta' => 'religion',
				'group' => 'userspecs',
				'options' => array("default" => '', "Agnosticism", "Atheism", "Christianity", "Judaism", "Catholicism", "Islam", "Hinduism", "Buddhism", "Other"),
				'default' => 'default',
			),
			array(
				'meta' => 'ethnicity',
				'group' => 'userspecs',
				'options' => array("default" => '', "Asian", "Middle Eastern", "Black", "Native American", "Indian", "Pacific Islander", "Hispanic / Latin", "White", "Other"),
				'default' => 'default',
			)
		);
		$instance->gmtoffset = \Session::get('gmtoffset', null);

		if( \Auth::instance()->check() )
		{
			$instance->logged_in = \Auth::instance()->get_screen_name();

			$uid = \Auth::instance()->get_user_id();
			$instance->user_id = @end( $uid );
			
			$tmpl =& $instance->template;

			$tmpl->logged_in = !!$instance->logged_in;
			$tmpl->username = $instance->logged_in ?: 'guest';
			$tmpl->user_id = $instance->user_id;

			$instance->user = Model_User::find($instance->user_id)->set('last_action', time());
			\View::set_global('user', $instance->user);

			$tmpl->message_count = \Model_Message::find()->where(array('user_id' => $instance->user->id, 'read' => '0'))->count();

			$instance->user->save();

			$instance->gmtoffset = $instance->user->get_meta('gmtoffset', $instance->gmtoffset);
			\Helpers::set_global('gmtoffset', $instance->gmtoffset);

			if( $instance->gmtoffset === null )
			{
				\Helpers::load_js('raw', 'redzu.pull("/gmt", { offset: -((new Date).getTimezoneOffset()/60) });');
			}
		}

		$req_acl = Helpers::get_request_acl($instance->request);
		if( ! \Auth::instance()->has_access( $req_acl ) )
		{

			if( ! $instance->logged_in )
			{
				return \Response::redirect( '/users/login?next=' . urlencode(\Uri::current()) );
			}
			else
			{
				return $instance->unknown_error();
			}
		}

	}

	public function register_globals()
	{
		if( \Auth::instance()->check() )
		{
			$uid = \Auth::instance()->get_user_id();
			if( $user = \Model_User::find(@end($uid)) )
			{
				$user = $user->to_array();
				$user['default_photo'] = \Social\Model_Photo::get_default_photo($user['id']);

				\Helpers::set_global('user', $user);
			}
		}
			
		\Helpers::set_global('site', array
		(
			'environment' => \Fuel::$env
		));
	}

	public function action_ajax( $action = null )
	{
		if( \Fuel::$env !== \Fuel::DEVELOPMENT and ! \Input::is_ajax() )
		{
			return false;
		}
		
		$args = func_get_args();
		$method = 'ajax_' . array_shift($args);

		if (! method_exists($this, $method))
		{
			return \Request::show_404();
		}

		$get_format = explode('.', end($args));
		$encoding = isset($get_format[1]) ? strtolower($get_format[1]) : 'json';
		$response = call_user_func_array(array($this, $method), $args);
		$format = \Format::forge($response);

		switch ($encoding)
		{
			case 'xml':
				header('Content-type: application/xml, text/xml');
				echo $format->to_xml();
				exit;

			case 'csv':
				header('Content-type: text/plain');
				echo $format->to_csv();
				exit;

			default:
				header('Content-type: application/json');
				echo $format->to_json();
				exit;
		}
	}

	public function before()
	{
		static $befored = false;
		parent::before();

		if( $befored )
		{
			return;
		}

		$befored = true;
		static::do_auth($this);	
	
	}
	public function after( $response )
	{
		parent::after( $response );
		\View::set_global('flash', \Helpers::get_flashes(), false);

		// Fuel needs response returned
		return $response;
	}

	public function unknown_error()
	{
		if (Fuel::$env === 'development') throw new Exception('Unknown error');
		$response = \Request::forge('/errors/unknown')->execute()->response();
		\Event::shutdown();
		$response->send(true);
		exit;
	}
	
	public function comingsoon()
	{
		$this->template->title = 'Feature coming soon - Redzu';
		$this->template->content = \View::forge('errors/comingsoon.twig');
	}
}