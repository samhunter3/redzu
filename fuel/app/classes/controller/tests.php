<?php
/**
 * Controller for testing with PHPUnit implicitly executed
 *
 * Reason being, I'm developing on Windows and the command line is just slow
 * I really don't want to install cygwin is being a nuisance.
 *
 * @author Samuel Hunter
 * @package Redzu
 */
final class Controller_Tests extends Controller
{
	protected static $app_test_path;

	static public function _init()
	{
		static::$app_test_path = APPPATH . 'tests' . DS;
	}

	final public function __clone()
	{
		// Cloning an awesome controller doesn't give you double the awesome
		throw new BadMethodException("FUUUUUUUUUUUUUUUUUUUUU");
	}

	public function rtest_index()
	{
		echo '<code>Use /tests/raw?test_id={test_id}</code>';
		exit;
	}

	public function action_raw()
	{
		$_this =& $this;
		$test = \Input::get('test_id', 'index');
		$test and method_exists($this, $test = 'rtest_'.$test) or $test = 'rtest_index';
		return \Response::forge(\Debug::dump(\Debug::benchmark(function () use ($test, $_this) {
			\Debug::dump( $_this->$test() );
		})));
	}

	public function action_index()
	{
		$group = $this->param('group');
		$suite = $this->param('suite');

		$res = '';
		$fuelroot = realpath(DOCROOT . '/../');

		$group = preg_replace('/[^0-9a-z\_]+/i', '', $group);
		$oil = $fuelroot.DS.'oil';

		@include_once('PHPUnit/Autoload.php');

		if ( ! class_exists('PHPUnit_Framework_TestCase'))
		{
			return 'PHPUnit does not appear to be installed.'.PHP_EOL.PHP_EOL."\tPlease visit http://phpunit.de and install.";
		}

		$command = 'cd '.$fuelroot.'; phpunit -c "'.COREPATH.'phpunit.xml"';
		$group and $group !== 'all' and $command .= ' --group='.$group;
		$suite and $command .= ' ' . static::$app_test_path . $suite;

		foreach(explode(';', $command) as $c)
		{
			$res .= passthru($c);
		}


		return \Response::forge("
			<!DOCTYPE html>
			<html>
			<style>
				* { margin:0; padding:0 }
				body {
					padding:30px;
					background:#f0f0f0;
					color:#444;
					white-space:pre;
					font:13px/16px monospace;
				}
			</style>
			<body>{$res}</body>
			</html>
		");
	}
}