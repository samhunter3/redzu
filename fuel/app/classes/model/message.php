<?php

class Model_Message extends \Orm\Model
{
	protected static $_belongs_to = array(
		'user',
		'from_user' => array(
			'key_from' => 'from_user_id',
			'model_to' => 'Model_User',
			'key_to' => 'id',
			'cascade_save' => true,
			'cascade_delete' => false,
		));
	protected static $_properties = array(
		'id',
		'user_id',
		'from_user_id',
		'message',
		'read',
		'created_at',
		'updated_at'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);
}
