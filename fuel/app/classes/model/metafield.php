<?php

class Model_Metafield extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'label',
		'meta',
		'default',
		'group',
		'weight',
		'options',
		'created_at',
		'updated_at'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function insert_unique( Array $data )
	{
		$args = func_get_args();
		$retdata = array();

		foreach( $args as $k => &$input )
		{
			if( isset($input['options']) && is_array($input['options']) )
			{
				$input['options'] = json_encode( $input['options'] );
			}

			if( ! isset($input['label']) && isset($input['meta']) )
			{
				$input['label'] = ucwords( strtolower( strtr($input['meta'], '_', ' ') ) );
			}

			if( ! isset($input['weight']) )
			{
				if ( ! isset($last) )
				{
					$last = static::find()->order_by('id', 'desc')->limit(1)->get_one();
					$weight = isset($last->id) && $last->id ? ($last->id + 1) : 0;
				}

				$input['weight'] = $weight++;
			}

			if( ! isset($input['meta']) && isset($input['label']) )
			{
				$input['meta'] = preg_replace( '/[^a-z0-9\-\_]+/i', '',
					strtolower( strtr( $input['label'], ' ', '_' ) ) );
			}

			isset($input['default']) or $input['default'] = '';
			isset($input['group']) or $input['group'] = '';
			isset($input['options']) or $input['options'] = '';

			$retdata[ $input['meta'] ] = false;
			if( ! static::find()->where('meta', $input['meta'])->count() )
			{
				$retdata[ $input['meta'] ] = static::factory($input)->save();
			}
		}

		return $retdata;
	}
}
