<?php

class Model_Invitation extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'email',
		'data',
		'created_at',
		'updated_at'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	static public function from_token($token = null)
	{
		if (($token = @\Crypt::decode($token)) && $model = static::find($token))
		{
			return $model;
		}

		return false;
	}

	public function get_token()
	{
		return \Crypt::encode($this->id);
	}
}
