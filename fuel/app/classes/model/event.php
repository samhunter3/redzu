<?php

class Model_Event extends \Social\Model_SocialElement
{
	protected static $_properties = array(
		'id',
		'event_name',
		'start_time',
		'end_time',
		'ticket_price',
		'ticket_website',
		'user_id',
		'age_min',
		'age_max',
		'venue_name',
		'venue_address',
		'venue_city',
		'state',
		'zipcode',
		'country',
		'max_attendees',
		'latitude',
		'longitude',
		'venue_phone',
		'venue_website',
		'additional_info',
		'tags',
		'created_at',
		'updated_at'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);
	
	public function get_default_photo()
	{
		return \Social\Model_Photo::get_default_photo($this->id, 'event');
	}
}
