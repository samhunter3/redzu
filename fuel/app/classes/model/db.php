<?php

/**
 * Circumvents some terrible un-DRY design practices in FuelPHP
 *
 */
class Model_DB
{
	protected $_model;
	protected $_query;

	public function __construct($model)
	{
		$this->_model = (is_object($model) ? ($model = get_class($model)) : \Inflector::classify('model_'.$model));
		$this->_query = call_user_func('DB::select')->from(call_user_func($this->_model.'::table'));
	}

	static public function forge($model)
	{
		return new static($model);
	}

	public function get()
	{
		$results = $this->_query->execute()->as_array();

		foreach($results as &$result)
		{
			$result = call_user_func($this->_model.'::factory', $result, false);
		}

		return $results;
	}

	public function __call($method, $args)
	{
		$result = call_user_func_array(array($this->_query, $method), $args);
		if ($result instanceof Database_Query_Builder)
		{
			return $this;	
		}

		return $result;
	}
}