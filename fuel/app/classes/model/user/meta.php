<?php
/**
 * TODO:
 *
 * Fuel's Inflector generated the plural form of
 * 'meta' as 'metum'; no huge deal but hope to fix
 * in the future to prevent confusion
 *
 * @package Redzu
 * @author  Samuel Hunter
 * @link    http://www.redzu.com
 */

class Model_User_Meta extends Orm\Model
{
	protected static $_belongs_to = array('user');
	protected static $_table_name = 'user_meta';
	protected static $_properties = array(
		'id',
		'user_id',
		'meta_key',
		'meta_value',
		'created_at',
		'updated_at'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function encode($data)
	{
		return json_encode($data);
	}
}
