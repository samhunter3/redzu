<?php

class Model_User_Search_Filter extends \Orm\Model
{
	protected static $_table_name = 'user_search_filters';
	protected static $_properties = array(
		'id',
		'user_id',
		'query_params',
		'label',
		'created_at',
		'updated_at'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);
}
