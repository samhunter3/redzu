<?php

class Model_User_Activated extends \Model_User
{
	protected static function pre_find( $query )
	{
		$query->where('searchable', 1);
		return $query;
	}
}