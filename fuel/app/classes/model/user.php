<?php

class Model_User extends Social\Model
{
	protected static $_meta = array();
	protected static $_properties = array(
		'id',
		'username',
		'password',
		'email',
		'searchable',
		'profile_fields',
		'group',
		'last_login',
		'last_action',
		'login_hash',
		'created_at',
		'updated_at',
	);

	/**
	 * @var array User meta (eg. date of birth, name, about me)
	 */
	protected static $_has_many = array(
		'meta' => array(
			'model_to' => 'Model_User_Meta'
		),
		'search_filters' => array(
			'model_to' => 'Model_User_Search_Filter'
		)
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('username', 'Username', 'required|max_length[255]');
		$val->add_field('password', 'Password', 'required|max_length[255]');
		$val->add_field('email', 'Email', 'required|valid_email|max_length[255]');
		$val->add_field('profile_fields', 'Profile Fields', 'required');
		$val->add_field('group', 'Group', 'required');
		$val->add_field('last_login', 'Last Login', 'required');
		$val->add_field('login_hash', 'Login Hash', 'required|max_length[255]');

		return $val;
	}

	public function send_password_recovery_email()
	{
		$reset_key = md5(\Crypt::encode($this->email).$this->last_login);
		$this->set_meta('password_reset_key', $reset_key);
		return $this->send_email('recover-password', 'Recover password', null, array(
			'reset_code' => $reset_key,
		));
	}

	public function send_email($emailtmpl, $subject, $message = '', $data = array())
	{
		$data['user'] = $this;
		$email = \Redzu_Email::forge($emailtmpl, $data);
		$email->send($this->email, $subject, $message);

		return $this;
	}

	public function get_meta( $key, $default = null, $use_profilefield = true )
	{
		$results = array();

		if (isset(static::$_meta[$this->id][$key]))
		{
			return static::$_meta[$this->id][$key];
		}
		elseif (! isset(static::$_meta[$this->id]))
		{
			static::$_meta[$this->id] = array();
		}

		$val = null;
		$user_metas = Model_User_Meta::find()
			->where(array(
				'user_id' => $this->id,
				'meta_key' => $key
			))->get();

		foreach ($user_metas as &$user_meta)
		{
			if( isset($user_meta->meta_value) )
			{
				$val = @json_decode(html_entity_decode($user_meta->meta_value), true);
			}

			if( $use_profilefield && $profilefield = Model_Metafield::find()->where('meta', $key)->get_one() )
			{
				if( ! isset($user_meta->meta_value) && ! $val )
				{
					$val = $profilefield->default;
				}

				if( $val !== '' && $profilefield->options )
				{
					$pfopt = @json_decode($profilefield->options, true);
					$val = isset($pfopt[$val]) ? $pfopt[$val] : null;
				}
			}

			$results[] = static::$_meta[$this->id][$key] = ($val !== '' && $val !== null ? $val : $default);
		}

		return is_array($key) ? $results : reset($results);
	}

	public function online_status()
	{
		return ($this->last_action + 300) > time();
	}

	public function get_default_photo()
	{
		return \Social\Model_Photo::get_default_photo($this->id);
	}

	public function increment_meta( $key, $by = 1 )
	{
		$value = $this->get_meta( $key, 0 );

		if( is_integer($value) )
		{
			return $this->set_meta( $key, ($value + $by) );
		}

		return $this;
	}

	public function decrement_meta( $key, $by = 1 )
	{
		return $this->increment_meta( $key, ($by * -1) );
	}

	public function set_meta( $key, $value, $use_profilefield = true )
	{
		$profilefield = Model_Metafield::find()->where('meta', $key)->get_one();

		if( $use_profilefield && $profilefield && $profilefield->options )
		{
			$options = (array)@json_decode($profilefield->options, true);
			
			if( $value === '' || ! in_array( $value, ($okeys=array_keys($options)) ) )
			{
				$value = isset($options[$profilefield->default]) ? $profilefield->default : '';
			}
		}

		is_numeric($value) and $value{0} !== '0' and $value = (float)$value;
		$value = \Model_User_Meta::encode($value);
		$fields = array(
			'user_id' => $this->id,
			'meta_key' => $key,
		);

		$user_meta = Model_User_Meta::find()
			->where($fields)
			->get_one();

		if( ! $user_meta )
		{
			$user_meta = Model_User_Meta::factory($fields);
		}

		if( ! $user_meta->set('meta_value', $value)->save() )
		{
			throw new FuelException("Can't save user meta for '{$key}'");
		}

		if (isset(static::$_meta[$this->id][$key]))
		{
			unset(static::$_meta[$this->id][$key]);
		}

		return $this;
	}

}
