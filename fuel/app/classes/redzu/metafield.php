<?php
class Redzu_Metafield
{
	public $errors = array();
	public $label = 'Field';

	static public function format( $value ) {}

	public function __construct() {}

	public static function __callStatic($method, $args)
	{
		return call_user_func_array("\\Model_Metafield::$method", $args);
	}

	public static function forge()
	{
		return new static();
	}
}