<?php

class Redzu_Animals
{
	/**
	 * @var  array  List of animals
	 */
	static protected $_all_animals;

	/**
	 * Get a list of animals
	 *
	 * @param void
	 * @return array
	 */
	static public function get_all()
	{
		if( ! isset(static::$_all_animals) )
		{
			\Config::load('animals', 'animals');
			static::$_all_animals = \Config::get('animals');

			foreach( static::$_all_animals as $k => &$v )
			{
				$v['image'] = \Asset::find_file("/animals/{$k}.png", 'img')
					?: \Asset::find_file("/animals/default.png", 'img');
			}
		}

		return static::$_all_animals;
	}

	/**
	 * Get a specific of animal
	 *
	 * @param string $name the name of the animal
	 * @return array
	 */
	static public function get( $name )
	{
		$name = strtolower( strtr($name, ' ', '_') );
		$animals = static::get_all();

		if( isset($animals[ $name ]) )
		{
			$animals[ $name ]['name'] = ucwords( strtr($name, '_', ' ') );
			return $animals[ $name ];
		}

		return null;
	}

	static public function has( $name )
	{
		$animals = static::get_all();
		return isset( $animals[ $name ] );
	}
}