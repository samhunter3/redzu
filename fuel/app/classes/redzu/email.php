<?php

class Redzu_Email
{
	protected $template = 'default';
	protected $data;

	public function __construct($email, $data = array())
	{
		if (is_array($email))
		{
			isset($email[1]) and $this->template = $email[1];
			$email = reset($email);
		}
		elseif (strpos($email, '#'))
		{
			list($email, $this->template) = explode('#', $email);
		}

		$email = preg_replace('/[^a-z0-9\_\-]+/i', '', $email);
		if( ! \Fuel::find_file('views/emails', $email, '.twig'))
		{
			throw new FuelException("Email does not exist.");
		}
		elseif( ! \Fuel::find_file('views/emails/templates', $this->template, '.twig'))
		{
			throw new FuelException("Email template does not exist.");
		}

		if(isset($data['user']))
		{
			is_numeric($data['user']) and $data['user'] = \Model_User::find($data['user']);
		}
		else
		{
			\Auth::check() and $data['user'] = \Model_User::find(\Helpers::my_uid());
		}

		$this->email = $email;
		$this->data = $data;
	}

	static public function forge($email, $data = array())
	{
		return new static($email, $data);
	}

	public function render($data = array())
	{
		$data = array_merge($this->data, $data);		
		\Controller_Template_Base::register_helpers();
		$email = \View::forge('emails/' .$this->email. '.twig', $data);
		return \View::forge('emails/templates/'.$this->template.'.twig', array_merge($data, array('content' => $email)));
	}

	public function __toString()
	{
		return (string) $this->render();
	}


	public function send($to, $subject, $message = '', $data = array())
	{
		$data['to'] = $to;
		$data['message'] = $message;
		$data['subject'] = $subject;

		$email = \Email::forge();
		$email->to($to);
		$email->subject($subject);
		$email->html_body($body = static::render($data));

		if( Fuel::$env === Fuel::DEVELOPMENT )
		{
			file_put_contents(DOCROOT.'emailtests'.DS.'/'.$subject.'-'.time().'.html', $body);

			try {
				$email->send();
			}
			catch (EmailSendingFailedException $e)
			{
				\PHP_Console::log('awesome');
			}

			return $this;
		}

		$email->send();

		return $this;
	}
}