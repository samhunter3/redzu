<?php

class Redzu_Metafield_Weight extends Redzu_Metafield
{
	public static function format( $val )
	{
		if( (integer)$val )
		{
			return $val . ' lbs';
		}
	}

	public static function _init()
	{
	}

	public function validate( $label, $fields )
	{
		if( ! isset($fields['weight']) || (integer)$fields['weight'] < 50 || (integer)$fields['weight'] > 999 )
		{
			return '';
		}

		return (integer)$fields['weight'];
	}

	public function form( $value )
	{
		$value = (integer) $value;
		return '<input type="text" size="5" name="weight" maxlength="3" value="'.$value.'"> lbs.';
	}
}