<?php

class Redzu_Metafield_Occupation extends Redzu_Metafield
{
	public function validate( $label, $fields )
	{
		$occupation = isset($fields['occupation']) ? $fields['occupation'] : '';
		$strl = strlen( $occupation );

		if( $strl !== 0 )
		{
			$strl > 48 && $this->errors['occupation'] = 'Occupation is too long.';
			$strl < 3 && $this->errors['occupation'] = 'Occupation is too short.';

			if( ! empty($this->errors) )
			{
				return false;
			}
		}

		return $occupation;
	}

}