<?php

class Redzu_Metafield_Education extends Redzu_Metafield
{
	public static $_check_fields = array(
		'education_1' => array(
			'default' => '—',
			'Graduated from',
			'Working on',
			'Dropped out of',
		),
		'education_2' => array(
			'default' => '—',
			'high school',
			'two-year college',
			'college/university',
			'masters program',
			'law school',
			'ph.D program',
			'space camp',
		),
	);

	public static function format( $val )
	{
		if( is_array($val) && sizeof($val) > 1 && ! in_array('default', $val) )
		{
			return static::$_check_fields['education_1'][ $val[0] ]
			. ' ' . static::$_check_fields['education_2'][ $val[1] ];
		}

		return '';
	}

	public function validate( $label, $fields )
	{
		$val = false;

		if( isset($fields['education_1']) && isset($fields['education_2']) )
		{
			isset(static::$_check_fields['education_2'][ $fields['education_2'] ])
			&& isset(static::$_check_fields['education_1'][ $fields['education_1'] ])
			&& ($val = array($fields['education_1'], $fields['education_2']));
		}

		return $val;
	}

	public function form( $value )
	{
		if( is_string($value) || ! $value )
		{
			$value = array('default', 'default');
		}

		foreach( $value as $k => $v )
		{
			$value === '' and $v = 'default';
		}

		$resp = '';
		$k = 0;
		foreach( static::$_check_fields as $cf_k => &$cf_v )
		{
			$resp .= '<select name="'.$cf_k.'">';
			foreach( $cf_v as $f_k => $f_v )
			{
				$resp .= '<option value="'.(string)$f_k.'" '. ( (string)$f_k == $value[$k] ? 'selected' : '' ) .'>'.$f_v.'</option>';
			}
			$resp .= '</select>';
			$k++;
		}

		return $resp;
	}
}