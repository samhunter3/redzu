<?php

class Redzu_Metafield_Height extends Redzu_Metafield
{
	public static $_check_fields = array(
		'feet'   => array('default' => '—', 3, 4, 5, 6, 7),
		'inches' => array('default' => '—', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11),
	);

	public static function format( $val )
	{
		$inches =& static::$_check_fields['inches'];
		$feet   =& static::$_check_fields['feet'];
		if( is_array($val) && sizeof($val) > 1 && $val[0] !== 'default' )
		{
			$val[0] = $feet[$val[0]];
			$val[1] = isset($inches[$val[1]]) && $val[1] !== 'default' ? $inches[$val[1]] : 0;
			$m = round( ((((integer)$val[0] * 12) + (integer)$val[1]) * 0.0254), 2 );
			return $val[0] . '′ ' . $val[1] . '″ ('.$m.'m)';
		}

		return '';
	}

	public static function _init()
	{
	}

	public function validate( $label, $fields )
	{
		if( ! isset($fields['feet']) || ! isset($fields['inches']) )
		{
			return false;
		}

		$val = false;

		isset(static::$_check_fields['feet'][$fields['feet']])
		&& isset(static::$_check_fields['inches'][$fields['inches']])
		&& ($val = array($fields['feet'], $fields['inches']));

		return $val;
	}

	public function form( $value )
	{
		if( is_string($value) || ! $value )
		{
			$value = array('default', 'default');
		}

		foreach( $value as $k => $v )
		{
			$value === '' and $v = 'default';
		}

		$k = 0;
		$resp = '';
		foreach( static::$_check_fields as $cf_k => &$cf_v )
		{
			$resp .= '<select name="'.$cf_k.'">';
			foreach( $cf_v as $f_k => $f_v )
			{
				$resp .= '<option value="'.(string)$f_k.'" '. ( (string)$f_k === $value[$k] ? 'selected' : '' )
					.'>&nbsp;'.$f_v.'&nbsp;</option>';
			}
			$resp .= '</select>&nbsp;' . $cf_k . '&nbsp;&nbsp;';
			$k++;
		}

		return $resp;
	}
}