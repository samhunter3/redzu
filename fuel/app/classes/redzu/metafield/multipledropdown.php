<?php

class Redzu_Metafield_MultipleDropdown extends Redzu_Metafield
{
	public static $_check_fields = array();

	public static function format( $val )
	{
		return implode('',(array)$val);
	}

	public static function _init()
	{
		static::$_check_fields = array_unique(array_merge(static::get_fields(), static::$_check_fields ));

		foreach( static::$_check_fields as $k => &$v )
		{
			if( ! isset($v['default']) )
			{
				$v['default'] = '—';
			}
		}
	}

	public static get_fields()
	{
		return array();
	}

	public function validate( $label, $fields )
	{
		$val = array();

		foreach( static::$_check_fields as $key => $field )
		{
			if( ! isset($fields[ $key ]) )
		}

		return $val;
	}

	public function form( $value )
	{
		if( is_string($value) )
		{
			$value = array('default', 'default');
		}

		foreach( $value as $k => $v )
		{
			$value === '' and $v = 'default';
		}

		$k = 0;
		$resp = '';
		foreach( static::$_check_fields as $cf_k => &$cf_v )
		{
			$append = '';
			$resp .= '<select name="'.$cf_k.'">';
			foreach( $cf_v as $f_k => $f_v )
			{
				$f_new = '<option value="'.(string)$f_k.'" '. ( (string)$f_k === $value[$k] ? 'selected' : '' )
					.'>&nbsp;'.$f_v.'&nbsp;</option>';

				($f_k === 'default' and ($resp .= $f_new)) or ($append .= $f_new);
			}
			$resp .= $append.'</select>';
			$k++;
		}

		return $resp;
	}
}