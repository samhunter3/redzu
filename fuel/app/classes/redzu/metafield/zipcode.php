<?php

class Redzu_Metafield_Zipcode extends Redzu_Metafield
{
	public function validate( $label, $fields )
	{
		if(! isset($fields))
		{
			$this->errors['zipcode'] = 'Zip Code is not set.';
			return false;
		}

		if (is_numeric($fields['zipcode']) && strlen($fields['zipcode']) === 5)
		{
			$zipdata = json_decode(file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=zip+code+'.$fields['zipcode'].'&sensor=false'), true);
		
			if (sizeof($zipdata['results']) && ($zipdata=$zipdata['results']))
			{
				foreach ($zipdata as $zk => &$zf)
				{
					if (in_array('postal_code', $zf['types']))
					{
						$geometry = $zf['geometry']['location'];
						$zipdata = $zf['address_components'];
						$zipcode = array_shift($zipdata);
						$city = array_shift($zipdata);
						$country = array_pop($zipdata);
						$state = array_pop($zipdata);

						if (in_array('country', $country['types']) && $country['short_name'] === 'US')
						{
							return array(
								'city' => $city['long_name'],
								'state' => $state['long_name'],
								'state_abbreviated' => $state['short_name'],
								'country' => $country['short_name'],
								'zipcode' => $zipcode['long_name'],
								'latitude' => $geometry['lat'],
								'longitude' => $geometry['lng']
							);
						}
					}
				}
			}
		}

		$this->errors['zipcode'] = 'Invalid Zip Code';
		return false;
	}
}