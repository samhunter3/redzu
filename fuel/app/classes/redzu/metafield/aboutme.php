<?php

class Redzu_Metafield_AboutMe extends Redzu_Metafield
{
	public static function format( $val )
	{
		return $val;
	}

	public static function _init()
	{
	}

	public function validate( $label, $fields )
	{
		$about_me = isset($fields['about_me']) ? $fields['about_me'] : '';
		
		$about_me = trim( preg_replace('/(\r?\n){2,}/', "\n\n", $about_me) );

		if(strlen($about_me) > 2000)
		{
			$this->errors['about_me'] = 'About me must be less than 2000 characters.';
			return false;
		}

		return $about_me;
	}

	public function form( $value )
	{
		return '<textarea maxlength="2000" rows="10" class="generic-textarea" name="about_me">'.htmlentities($value).'</textarea>';
	}
}