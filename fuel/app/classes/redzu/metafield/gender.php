<?php

class Redzu_Metafield_Gender extends Redzu_Metafield
{
	public static function format( $val )
	{
		return ucfirst($val);
	}
}