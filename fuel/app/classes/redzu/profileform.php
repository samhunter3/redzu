<?php

class Redzu_Profileform
{
	public static $_errors = array();

	/**
	 * Try to figure out what the form is
	 *
	 * @access public
	 * @param string $method
	 * @return string
	 * @throws BadMethodCallException
	 */
	public static function __callStatic($oldmethod, $args)
	{
		$methodstr = strpos($oldmethod, '_');
		$meta_name = substr($oldmethod, $methodstr+1);
		$method = substr($oldmethod, 0, $methodstr);

		$fieldObj = null;
		$field = Model_Metafield::find()->where('meta', $meta_name )->get_one();

		if( ! $field )
		{
			static::badmethod($oldmethod);
			return false;
		}

		if( class_exists($classname = '\Redzu_Metafield_' . \Inflector::camelize($field->meta)) )
		{
			$fieldObj = new $classname;
		}

		$user = Model_User::find( $args[0] );
		if( $user && $method === 'update' && isset($args[1]) )
		{
			$val = false;
			$retval = false;

			if( $fieldObj && method_exists($fieldObj, 'validate') )
			{
				$val = call_user_func_array(array($fieldObj, 'validate'), array($field->label, $args[1]));
				static::$_errors = array_merge( static::$_errors, $fieldObj->errors );
				$retval = $val;
			}
			elseif( $field->options && ($fieldopt = (array) @json_decode( $field->options )) )
			{
				$fields = $args[1];
				if( isset($fields[$field->meta]) && isset($fieldopt[ $fields[$field->meta] ]) )
				{
					$val = $fields[$field->meta];
					$retval = $fieldopt[ $val ];
				}
				elseif( $field->default !== '' )
				{
					$val = $field->default;
					$retval = $fieldopt[ $val ];
				}
			}

			if( $val !== false )
			{
				$user->set_meta( $field->meta, $val );
			}

			return $retval !== false ? Helpers::format_profilefield( $field->meta, $retval ) : false;
		}
		elseif( $user && $method === 'form' )
		{
			$cv = $user ? $user->get_meta($field->meta, $field->default) : '';
			if( $fieldObj && method_exists($fieldObj, 'form') )
			{
				return call_user_func_array(array($fieldObj, 'form'), array($cv));
			}
			elseif( $field->options && ($fieldopt = (array) @json_decode( $field->options )) )
			{
				$resp = '<select name="'.$field->meta.'">';
				foreach( $fieldopt as $k => $opt )
				{
					$resp .= '<option value="'.htmlentities($k).'"'.
						( $opt === $cv ? ' selected' : '' )
						.'>'. ($opt ? htmlentities($opt) : '&mdash;') . '</option>';
				}
				$resp .= '</select>';
				return $resp;
			}
			else
			{
				return '<input type="text" name="'.$field->meta.'" value="'.htmlentities($cv).'">';
			}
		}
	
		static::badmethod($oldmethod);
		return;
	}

	protected static function badmethod($method)
	{
		$class = get_called_class();
		throw new BadMethodCallException("Method does not exist \"$class::$method\"");
	}
}