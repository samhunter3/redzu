<?php
/**
 * An extension to ORM Query that uses a virtually
 * schema-less meta table for properties
 *
 * Class methods prefixed with an underscore can be called
 * without the underscore as a static method or instance method.
 *
 * All methods in this wrapper also override it's child object
 * \Orm\Query's methods.
 *
 * @example
 * <code>
 *    
 *
 *
 *
 * @author  Samuel Hunter
 * @package Redzu
 */
class User_Query
{
	static protected $_users = array();

	protected $_query;
	protected $_metas;
	protected $_related;
	protected $_relatedCount;
	private $limit;

	/**
	 * Special property for calculating distance
	 */
	protected function sproperty_distance($query)
	{
		return $this;
	}

	static public function forge($config = array())
	{
		return new static($config);
	}

	public function __construct($config = array())
	{
		$this->_query = \Model_User::find();
		$this->_metas = array();
		$this->_related = array();
		$this->_relatedCount = 1;
		$this->limit = null;

		foreach ($config as $c_k => $c_v)
		{
			$this->$c_k = $v;
		}
	}

	public function _of_orientation($user)
	{
		if ($user = \Helpers::get_user($user))
		{
			$sexor = $user->get_meta('sexual_orientation');
			$gender = $user->get_meta('gender');
		}

		if ($sexor !== 'bisexual')
		{
			$gender = 'male';

			if (($gender === 'female' && $sexor === 'homosexual') || ($gender === 'male' && $sexor === 'straight'))
			{
				$gender = 'female';
			}

			$this->_where(array(
				array('gender', $gender),
			));
		}

		return $this;
	}

	public function _similar($user)
	{
		is_numeric($user) and $user = static::get_user($user);
		return $this;
	}

	public function _where()
	{
		$condition = func_get_args();
		is_array(reset($condition)) and $condition = reset($condition);
		$user_properties = array_keys(\Model_User::properties());

		// Refactor the query
		foreach( $condition as $k => &$w )
		{
			if( is_array($w) && is_integer($k) )
			{
				$insert = $w;
			}
			elseif( is_string($k) && is_array($w) )
			{
				$insert = array_unique(array_merge((array) $k, $w));
			}
			elseif( is_string($k) && ! is_array($w) )
			{
				$insert = array($k, (string)$w);
			}
			else
			{
				continue;
			}

			if( in_array($insert[0], $user_properties) )
			{
				$this->_query->where($insert);
				continue;
			}
			
			if( method_exists($this, 'sproperty_'.$insert[0]) )
			{
				$this->call_user_func_array(array($this, 'sproperty_'.$insert[0]),
					array_slice($insert, 1));
				continue;
			}

			$last = sizeof( $insert ) - 1;
			$insert[ $last ] = \Model_User_Meta::encode($insert[ $last ]);
			$this->related('meta/' . ($this->_metas[] = $insert[0]) , array(
				'where' => array(
					array('meta_key', $insert[0] ),
					array_merge( (array) 'meta_value', array_slice($insert, 1) )
			)));
		}

		return $this;
	}

	/**
	 * Get query with it's meta
	 *
	 * @param string $meta_filter "all", "queried", or "none"
	 */
	public function _get_with_meta( $meta_filter = 'all' )
	{
		$models = $this->_query->get();
		$metas = array();

		foreach ($models as &$model)
		{
			$model = $model->to_array();
			foreach ($this->_metas as &$meta)
			{
				$meta_value = reset($model[$meta]);
				$meta_value = $meta_value['meta_value'];
				$model[$meta] = json_decode($meta_value, true);
			}
		}

		if( $meta_filter !== 'all' && ! is_array($meta_filter) )
		{
			return $models;
		}

		foreach ($models as $m_id => &$model)
		{
			$find_meta = \Model_User_Meta::find()
				->where('user_id', $m_id);

			if( is_array($meta_filter) )
			{
				foreach ($meta_filter as &$v)
				{
					$find_meta->where('meta_key', $v);
				}
			}
			else
			{
				foreach ($this->_metas as &$meta)
				{
					$find_meta->where(array('meta_key', '!=', $meta));
				}
			}

			$find_meta = $find_meta->get();
			foreach ($find_meta as &$model_meta)
			{
				$model[$model_meta->meta_key] = json_decode($model_meta->meta_value, true);
			}
		}

		return $models;
	}

	public function _related( $relation )
	{
		if(! in_array($relation, $this->_related))
		{
			$args = func_get_args();
			call_user_func_array(array($this->_query, 'related'), $args);
			$this->_related[ $this->_relatedCount++ ] = $relation;
		}

		return $this;
	}

	public function _order_by($property, $direction = 'ASC', $expr = '$1')
	{
		if (is_array($property))
		{
			foreach ($property as $p => $d)
			{
				$nexpr = isset($d[2]) ? $d[2] : $expr;
				$ndirection = isset($d[1]) ? $d[1] : $direction;
				if (is_int($p))
				{
					is_array($d) ? $this->_order_by($d[0], $ndirection, $nexpr) : $this->_order_by($d, $ndirection, $nexpr);
				}
				else
				{
					$this->_order_by($p, $d);
				}
			}
		}

		$args = func_get_args();
		$user_properties = array_keys(\Model_User::properties());

		if (! in_array($property, $user_properties))
		{
			$this->related('meta/'.$property);
			$column = 't'.array_search('meta/'.$property, $this->_related);

			$property = new \Database_Expression(str_replace('$1', "`{$column}`.`meta_key`", $expr));
			$args = array($property, $direction);
		}

		call_user_func_array(array($this->_query, 'order_by'), $args);
		return $this;
	}

	public function _limit( $limit )
	{
		$this->limit = $limit;
		$this->_query->limit( $limit );

		return $this;
	}

	public function _get($limit = null)
	{
		$limit and $this->_query->limit($limit);
		$results = $this->_query->get();
		$this->_query->limit($this->limit);

		return $results;
	}

	public function _result($limit = null)
	{
		return $this->_get_with_meta($limit);
	}

	public function _get_one()
	{
		return $this->_query->get_one();
	}

	public function __call($method, $args)
	{
		if( method_exists($this, '_'.$method) )
		{
			return call_user_func_array(array($this, '_'.$method), $args);
		}

		$result = call_user_func_array(array($this->_query, $method), $args);
		if( is_object($result) && $result instanceof \Orm\Query )
		{
			return $this;
		}

		return $result;
	}

	public static function __callStatic($method, $args)
	{
		return call_user_func_array(array(static::forge(), '_'.$method), $args);
	}
}