<?php
class View_Module extends ViewModel
{
	protected $_view = null;

	public function __construct($method, $auto_filter = null)
	{
		if (! $this->_view)
		{
			$view = 'partials'.DS.'modules'.DS.strtolower(substr(get_called_class(), strlen(__CLASS__) + 1));
			$this->_view = Fuel::find_file('views', $view.DS.$method, '.twig') ?: Fuel::find_file('views', $view, '.twig');
		}

		parent::__construct($method, $auto_filter);
		$this->$method = $this->method = $method;
	}
}