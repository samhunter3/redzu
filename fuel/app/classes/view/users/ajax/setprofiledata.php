<?php
class View_Users_Ajax_SetProfileData extends ViewModel
{
	protected $_view = 'users/ajax/setprofiledata.twig';

	public function errors()
	{
		return $this->errors;
	}

	public function view()
	{
		$this->errors = array();
		$errors =& $this->errors;
		$this->post = $post = \Input::method() === 'POST';
		$this->fields = array_merge(array(
			'zipcode' => $this->user->get_meta('location_zipcode'),
			'gender' => $this->user->get_meta('gender'),
			'dob' => $this->user->get_meta('birthdate'),
			'city' => $this->user->get_meta('location_city'),
			'state' => $this->user->get_meta('location_state'),
			'sexual_orientation' => $this->user->get_meta('sexual_orientation'),
			'relationship_status' => $this->user->get_meta('relationship_status'),
			'gmtoffset' => 0
		), \Input::post());

		$this->gender_fields = array
		(
			'$' => ' — ',
			'male' => 'Male',
			'female' => 'Female',
		);
		$this->sexual_orientation_fields = array
		(
			'$' => ' — ',
			'straight' => 'Straight',
			'bisexual' => 'Bisexual',
			'homosexual' => 'Homosexual',
		);
		$this->relationship_status_fields = array
		(
			'$' => ' — ',
			'single' => 'Single',
			'seeing_someone' => 'Seeing someone',
			'married' => 'Married',
		);
		$this->dob_month_fields = array_merge( 
			array('$' => ' — '), \Helpers::get_all_months()	);
		$this->dob_day_fields = array_merge( 
			array('$' => ' — '), range(1,31) );
		$this->dob_year_fields = array_merge( 
			array('$' => ' — '), array_reverse(range(1901, (integer)date('Y')-18) ));

		foreach($this->fields as &$v)
		{
			$v === '$' and $v = '';
		}

		if(\Input::method() === 'POST')
		{
			if( is_numeric($this->fields['gmtoffset']) )
			{
				$this->user->set_meta('gmtoffset', (integer)$this->fields['gmtoffset']);
			}

			if( empty($this->fields['sexual_orientation']) || array_key_exists($this->fields['sexual_orientation'], $this->sexual_orientation_fields) ) 
			{
				empty($this->fields['sexual_orientation']) and $this->user->searchable = 0;
				$this->user->set_meta('sexual_orientation', $this->fields['sexual_orientation']);
			}
			else
			{
				$errors['sexual_orientation'] = 'WOW, look a hacker';
			}

			if( empty($this->fields['relationship_status']) || array_key_exists($this->fields['relationship_status'], $this->relationship_status_fields) ) 
			{
				empty($this->fields['relationship_status']) and $this->user->searchable = 0;
				$this->user->set_meta('relationship_status', $this->fields['relationship_status']);
			}
			else
			{
				$errors['relationship_status'] = 'WOW, look a hacker';
			}

			if( empty($this->fields['gender']) || array_key_exists($this->fields['gender'], $this->gender_fields) ) 
			{
				empty($this->fields['gender']) and $this->user->searchable = 0;
				$this->user->set_meta('gender', $this->fields['gender']);
			}
			else
			{
				$errors['gender'] = 'WOW, look a hacker';
			}

			if (! empty($this->fields['zipcode']))
			{
				$zvalidator = new Redzu_Metafield_Zipcode();
				$data = $zvalidator->validate('Zip Code', $this->fields);
				if ($data)
				{
					$this->user->set_meta('location_city', $data['city'])
						->set_meta('location_state', $data['state'])
						->set_meta('location_state_abbreviation', $data['state_abbreviated'])
						->set_meta('location_country', $data['country'])
						->set_meta('location_zipcode', $data['zipcode'])
						->set_meta('location_latitude', $data['latitude'])
						->set_meta('location_longitude', $data['longitude']);
				}
				else
				{
					$errors = array_merge($errors, $zvalidator->errors);
				}
			}
			else
			{
				$this->user->searchable = 0;
			}

			$dob_complete = false;
			if (isset($this->fields['dob']) && is_array($this->fields['dob']) && isset($this->fields['dob']['month']) && $this->fields['dob']['month'] !== '$')
			{

				$dob_complete = isset($this->dob_month_fields[ $this->fields['dob']['month'] + 1 ])
					and in_array($this->fields['dob']['day'], $this->dob_day_fields)
					and in_array($this->fields['dob']['year'], $this->dob_year_fields);

				if( $this->fields['dob']['month'] === '$' || $this->fields['dob']['day'] === '$' || $this->fields['dob']['year'] === '$' )
				{
					$dob_complete = false;
					$errors['dob'] = 'Birthdate invalid.';
					$this->user->searchable = 0;
				}
				elseif( ! ($age=\Helpers::years_old($this->fields['dob']['month'] .'/'. $this->fields['dob']['day'] .'/'. $this->fields['dob']['year'])) || $age < 18 )
				{
					$errors['dob'] = '&nbsp;&nbsp;&nbsp;&nbsp;18+&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;or&nbsp;&nbsp; &nbsp;&nbsp;GTFO&nbsp;&nbsp;';
				}
				elseif($age)
				{
					$this->user->set_meta('age', $age);
					$this->user->set_meta('birthdate', $this->fields['dob']);
				}
			}
			else
			{
				$this->user->set_meta('age', '');
				$this->user->set_meta('birthdate', array());
			}

			$this->user->save();
		}
	}

}