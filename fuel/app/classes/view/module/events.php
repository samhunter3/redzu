<?php
class View_Module_Events extends View_Module
{
	public function latest()
	{
		$this->title = 'Latest Events';
		$this->events = \Model_Event::find()->where('start_time', '>=', time())->get();
	}
}