<?php

class View_Module_Profiles extends View_Module
{
	public function featured()
	{
		$this->profiles = \User_Query::where(array(
			array('id', '!=', \Helpers::my_uid()),
			array('searchable', 1)
		))->order_by('last_action', 'DESC')
		->limit(10)
		->order_by('profile_hits', 'DESC', 'CAST($1 AS SIGNED)')->limit(6)->get();
	}

	public function popular()
	{
		$this->profiles = \User_Query::where(array(
				array('id', '!=', \Helpers::my_uid()),
				array('searchable', 1)
			))->of_orientation(\Helpers::get_user())
			->limit(10)
			->order_by('profile_hits', 'DESC', 'CAST($1 AS SIGNED)')->get();

	}

	public function similar()
	{

	}
}