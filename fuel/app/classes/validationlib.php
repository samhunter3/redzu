<?php
/**
 * Some validation stuffs the Fuel library
 * does not provide. :(
 *
 * @package ValidationLib
 * @author  Samuel Hunter
 * @link    http://www.redzu.com
 */
class ValidationLib
{
	static public function _validation_unique($val, $options)
	{
		list($table, $field) = explode('.', $options);

		$result = DB::select("LOWER (\"$field\")")
		->where( $field, '=', Str::lower($val) )
		->from( $table )->execute();

		return ! ($result->count() > 0);
	}

	static public function config( &$val )
	{
		$val->set_message('unique', ':label is already taken.');
		$val->set_message('required', ':label is required.');
		$val->set_message('valid_email', ':label is invalid.');
		$val->set_message('match_pattern', ':label is invalid.');
		$val->set_message('min_length', ':label must be at least :param:1 characters.');
		$val->set_message('max_length', ':label must not exceed :param:1 characters.');
	}
}