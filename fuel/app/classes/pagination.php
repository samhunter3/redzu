<?php

class Pagination
{

	/**
	 * @var	integer	The current page
	 */
	public $current_page = null;

	/**
	 * @var	integer	The offset that the current page starts at
	 */
	public $offset = 0;

	/**
	 * @var	integer	The number of items per page
	 */
	public $per_page = 10;

	/**
	 * @var	integer	The number of total pages
	 */
	public $total_pages = 0;

	/**
	 * @var array The HTML for the display
	 */
	public $template = array(
		'wrapper_start'  => '<div class="pagination">',
		'wrapper_end'    => '</div>',
		'page_start'     => '<span class="page-links">',
		'page_end'       => ' </span>',
		'previous_start' => '<span class="previous">',
		'previous_end'   => '</span>',
		'previous_mark'  => '&laquo; ',
		'next_start'     => '<span class="next">',
		'next_end'       => '</span>',
		'next_mark'      => ' &raquo;',
		'active_start'   => '<span class="active">',
		'active_end'     => '</span>',
	);

	/**
	 * @var	integer	The total number of items
	 */
	protected $total_items = 0;

	/**
	 * @var	integer	The total number of links to show
	 */
	protected $num_links = 5;

	/**
	 * @var	mixed	The pagination URL
	 */
	protected $pagination_url;

	// --------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * Loads in the config and sets the variables
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct($config = array())
	{
		$this->set_config($config);
		$this->initialize();
	}

	// --------------------------------------------------------------------

	/**
	 * Alias to the constructor
	 *
	 * @access public
	 * @return Pagination
	 */
	static public function forge($config = array())
	{
		return new static($config);
	}

	// --------------------------------------------------------------------

	public function set_config($config = array())
	{
		if (isset($config['template']))
		{
			$this->template = array_merge($this->template, ((array)$config['template']));
			unset($config['template']);
		}

		foreach ($config as $k => $v)
		{
			$this->$k = $v;
		}

		return $this;
	}

	// --------------------------------------------------------------------

	/**
	 * Prepares vars for creating links
	 *
	 * @access public
	 * @return array    The pagination variables
	 */
	protected function initialize()
	{
		$this->total_pages = ceil($this->total_items / $this->per_page) ?: 1;

		if ($this->current_page > $this->total_pages)
		{
			$this->current_page = $this->total_pages;
		}
		elseif ($this->current_page < 1)
		{
			$this->current_page = 1;
		}

		// The current page must be zero based so that the offset for page 1 is 0.
		$this->offset = ($this->current_page - 1) * $this->per_page;
	}

	// --------------------------------------------------------------------

	/**
	 * Creates the pagination links
	 *
	 * @access public
	 * @return mixed    The pagination links
	 */
	public function create_links()
	{
		if ($this->total_pages == 1)
		{
			return '';
		}

		\Lang::load('pagination', true);

		$pagination  = $this->template['wrapper_start'];
		$pagination .= $this->prev_link(\Lang::get('pagination.previous'));
		$pagination .= $this->page_links();
		$pagination .= $this->next_link(\Lang::get('pagination.next'));
		$pagination .= $this->template['wrapper_end'];

		return $pagination;
	}

	// --------------------------------------------------------------------

	/**
	 * Pagination Page Number links
	 *
	 * @access public
	 * @return mixed    Markup for page number links
	 */
	public function page_links()
	{
		if ($this->total_pages == 1)
		{
			return '';
		}

		$pagination = '';

		// Let's get the starting page number, this is determined using num_links
		$start = (($this->current_page - $this->num_links) > 0) ? $this->current_page - ($this->num_links - 1) : 1;

		// Let's get the ending page number
		$end   = (($this->current_page + $this->num_links) < $this->total_pages) ? $this->current_page + $this->num_links : $this->total_pages;

		for($i = $start; $i <= $end; $i++)
		{
			if ($this->current_page == $i)
			{
				$pagination .= $this->template['active_start'].$i.$this->template['active_end'];
			}
			else
			{
				$url = ($i == 1) ? '' : $i;
				$pagination .= \Html::anchor(sprintf(rtrim($this->pagination_url, '/'), $url), $i);
			}
		}

		return $this->template['page_start'].$pagination.$this->template['page_end'];
	}

	// --------------------------------------------------------------------

	/**
	 * Pagination "Next" link
	 *
	 * @access public
	 * @param string $value The text displayed in link
	 * @return mixed    The next link
	 */
	public function next_link($value)
	{
		if ($this->total_pages == 1)
		{
			return '';
		}

		$tmpl = $this->template['next_start']
			.$value.$this->template['next_mark']
			.$this->template['next_end'];

		if ($this->current_page == $this->total_pages)
		{
			return $tmpl;
		}
		else
		{
			$next_page = $this->current_page + 1;
			return \Html::anchor(sprintf(rtrim($this->pagination_url, '/'), $next_page), $tmpl);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Pagination "Previous" link
	 *
	 * @access public
	 * @param string $value The text displayed in link
	 * @return mixed    The previous link
	 */
	public function prev_link($value)
	{
		if ($this->total_pages == 1)
		{
			return '';
		}

		$tmpl = $this->template['previous_start']
			.$this->template['previous_mark'].$value
			.$this->template['previous_end'];

		if ($this->current_page == 1)
		{
			return $tmpl;
		}
		else
		{
			$previous_page = $this->current_page - 1;
			$previous_page = ($previous_page == 1) ? '' : '/'.$previous_page;
			return \Html::anchor(sprintf(rtrim($this->pagination_url, '/'), $previous_page), $tmpl);
		}
	}
}