<?php
return array(
	'_root_'  => 'main/index',  // The default route
	'_404_'   => 'errors/404', 	 // The main 404 route

	'me'                => 'users',
	'me/events'         => 'users/events/edit',
	'me/profile'        => 'users/profile/edit',
	'me/photos'         => 'users/photos/show/edit',
	'me/photos/:num'    => 'users/photos/preview/me/$1',
	'me/photos/:action' => 'users/photos/$1',

	'profile/:username/photos/:num' => 'users/photos/preview/$2',
	'profile/:username/photos'      => 'users/photos/show',
	'profile/:username/upload'      => 'users/photos/upload',
	'profile/:username'             => 'users/profile/$1',

	'tests/raw' => 'tests/raw',
	'tests(/:suite)?(/:group)?' => 'tests/index',

	'events/([0-9]+)' => 'events/show/$1',
	'events/:id/photos/(:num)' => 'events/photos/preview/$2',
	'events/:id/photos(/:action)?' => 'events/photos/$3'
);