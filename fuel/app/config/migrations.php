<?php
return array(
	'version' => 
	array(
		'app' => 
		array(
			'default' => 14,
		),
		'module' => 
		array(
		),
		'package' => 
		array(
			'ninjauth' => 3,
		),
	),
	'folder' => 'migrations/',
	'table' => 'migration',
);
