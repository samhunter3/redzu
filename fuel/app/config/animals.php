<?php

return array
(
	'dog' => array
	(
		'descriptions' => array('Cheerful', 'Devoted', 'Faithful')
	),

	'dolphin' => array
	(
		'descriptions' => array('Charismatic', 'Gregarious', 'Loving')
	),

	'eagle' => array
	(
		'descriptions' => array('Powerful', 'Self-confident', 'Striking')
	),

	'elephant' => array
	(
		'descriptions' => array('Family-oriented', 'Intelligent', 'Protective')
	),

	'fox' => array
	(
		'descriptions' => array('Flirtatious', 'Generous', 'Creative')
	),

	'gorilla' => array
	(
		'descriptions' => array('Unpretentious', 'Fun loving', 'Adventurous')
	),

	'horse' => array
	(
		'descriptions' => array('Optimistic', 'Acute insight', 'Stubborn')
	),

	'lion' => array
	(
		'descriptions' => array('Powerful', 'Compelling', 'Courageous')
	),

	'monkey' => array
	(
		'descriptions' => array('Witty', 'Energetic', 'Cheerful')
	),

	'panda_bear' => array
	(
		'descriptions' => array('Confident', 'Ethical', 'Protective')
	),

	'tiger' => array
	(
		'descriptions' => array('Brave', 'Engaging', 'Lively')
	),

	'cat' => array
	(
		'descriptions' => array('Attractive', 'Spiritual', 'Sexy')
	),

	'wolf' => array
	(
		'descriptions' => array('Good looking', 'Athletic', 'Passionate')
	),

	'rat' => array
	(
		'descriptions' => array('Intelligent', 'Compassionate', 'Charming')
	),

	'ox' => array
	(
		'descriptions' => array('Tolerant', 'Stable', 'Persevering')
	),

	'rabbit' => array
	(
		'descriptions' => array('Introvert', 'Friendly', 'Calm')
	),

	'dragon' => array
	(
		'descriptions' => array('Intense power', 'Free spirit', 'Extrovert')
	),

	'snake' => array
	(
		'descriptions' => array('Great mediator', 'Secretive', 'Keen')
	),

	'horse' => array
	(
		'descriptions' => array('Optimistic', 'Acute insight', 'Animated')
	),

	'sheep' => array
	(
		'descriptions' => array('Peace', 'Easygoing', 'Calm')
	),

	'rooster' => array
	(
		'descriptions' => array('Flamboyant', 'Feisty', 'Confident')
	),

	'pig' => array
	(
		'descriptions' => array('Fun', 'Patient', 'Understanding')
	),
);

// 'image' => \Uri::create( \Asset::find_file('animals/dog.png', 'img') ),
/* end of file animals.php */