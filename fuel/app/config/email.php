<?php

return array(

	/**
	 * Default settings
	 */
	'defaults' => array(

		/**
		 * Mail useragent string
		 */
		'useragent'	=> 'RedzuEmail',
		/**
		 * Mail driver (mail, smtp, sendmail)
		 */
		'driver'		=> 'mail',

		/**
		 * Whether to send as html, set to null for autodetection.
		 */
		'is_html'		=> true,

		/**
		 * Email charset
		 */
		'charset'		=> 'utf-8',

		/**
		 * Ecoding (8bit, base64 or quoted-printable)
		 */
		'encoding'		=> '8bit',

		/**
		 * Email priority
		 */
		'priority'		=> \Email::P_NORMAL,

		/**
		 * Default sender details
		 */
		'from'		=> array(
			'email'		=> 'support@redzu.com',
			'name'		=> 'Redzu Support',
		),

		/**
		 * Whether to validate email addresses
		 */
		'validate'	=> true,

		/**
		 * Auto attach inline files
		 */
		'auto_attach' => true,

		/**
		 * Auto generate alt body from html body
		 */
		'generate_alt' => true,

		/**
		 * Wordwrap size, set to null, 0 or false to disable wordwrapping
		 */
		'wordwrap'	=> null,

		/**
		 * Path to sendmail
		 */
		'sendmail_path' => '/usr/sbin/sendmail',

		/**
		 * SMTP settings
		 */
		'smtp'	=> array(
			'host'		=> 'email-smtp.us-east-1.amazonaws.com',
			'port'		=> 25,
			'username'	=> 'AKIAJ73G2ZVV4AROEPXA',
			'password'	=> 'AnscN/p9hdz+dINFCMFDDkCwPVAB7SQaK3o07k7doxZ+',
			'timeout'	=> 5,
		),

		/**
		 * Newline
		 */
		'newline'	=> "\n",

		/**
		 * Attachment paths
		 */
		'attach_paths' => array(
			// absolute path
			'',
			// relative to docroot.
			DOCROOT,
		),
	),

	/**
	 * Default setup group
	 */
	'default_setup' => 'default',

	/**
	 * Setup groups
	 */
	'setups' => array(
		'default' => array(),
	),

);
