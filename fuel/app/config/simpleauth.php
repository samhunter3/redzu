<?php

return array(

	/**
	 * DB connection, leave null to use default
	 */
	'db_connection' => null,

	/**
	 * DB table name for the user table
	 */
	'table_name' => 'users',

	/**
	 * Choose which columns are selected, must include: username, password, email, last_login,
	 * login_hash, group & profile_fields
	 */
	'table_columns' => array('*'),

	/**
	 * This will allow you to use the group & acl driver for non-logged in users
	 */
	'guest_login' => true,

	/**
	 * Groups as id => array(name => <string>, roles => <array>)
	 */
	'groups' => array(
		-1 => array(
			'name' => 'Banned',
			'roles' => array('banned'),
		),
		0 => array(
			'name' => 'Guests',
			'roles' => array('guest'),
		),
		1 => array(
			'name' => 'Users',
			'roles' => array('user', 'guest'),
		),
		50 => array(
			'name' => 'Moderators',
			'roles' => array('user', 'guest', 'moderator'),
		),
		100 => array(
			'name' => 'Administrators',
			'roles' => array('user', 'guest', 'moderator', 'admin'),
		),
		999 => array(
			'name' => 'Hackers',
			'roles' => array('super'),
		),
	),

	/**
	 * Roles as name => array(location => rights)
	 */
	'roles' => array(
		'banned'    => false,
		'super'     => true,
		'guest'     => array(
			'errors' => array('unknown', '404'),
			'users'  => array('login', 'recover', 'invite', 'request', 'register', 'logout', 'session', 'callback'),
			'main'   => array('index'),
		),
		'moderator' => array(),
		'admin'     => array(
			'events' => array('create', 'update'),
			'admin' => array('index', 'users' => array('index', 'delete', 'edit', 'create'))
		),
		'user'      => array(
			'photos' => array('index', 'setdefault', 'edit', 'delete', 'preview', 'upload'),
			'events' => array('index', 'calendar', 'list', 'photos' => true, 'show'),
			'deals' => array('index', 'show'),
			'users' => array(
				'index', 'profile', 'cancel', 'ajax', 'list', 'upgrade', 'connected',
				'search',
				'photos' => array('show', 'upload', 'ajax', 'preview'),
				'settings' => true,
				'messages' => true,
			),
		),
	),

	/**
	 * Salt for the login hash
	 */
	'login_hash_salt' => '7xqNt=ps(wezrBS*XDAF*EM6xs)4+j-^yHm(YEAYtg,c2b1f2c54f8fce4',

	/**
	 * $_POST key for login username
	 */
		// 'username_post_key' => 'username',

	/**
	 * $_POST key for login password
	 */
		// 'password_post_key' => 'password',
);