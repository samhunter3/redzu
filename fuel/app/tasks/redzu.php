<?php
namespace Fuel\Tasks;

class Redzu
{

	/**
	 * This method gets ran when a valid method name is not used in the command.
	 *
	 * @return string
	 */
	public static function run()
	{
		$cli = "\nCommands:\n\n";
		$cli .= "oil r redzu:clear [type] - Clear data for something";

		return $cli;
	}

	public static function clear($type = null)
	{ 
		if( ! $type )
		{
			$cli = "\nCommands:\n\n";
			$cli .= "oil r redzu:clear all             - Clear all Redzu data [USE WISELY]\n";
			$cli .= "oil r redzu:clear thumbnails      - Deletes thumbnail cache\n";
			$cli .= "oil r redzu:clear logs            - Deletes app log data\n";

			return $cli;
		}

		$cli = "\nResult:\n\n";

		if ($type === 'thumbnails' or $type === 'all')
		{
			$thumbdir = DOCROOT.'public'.DS.'assets'.DS.'img'.DS.'gd';
			$files = glob($thumbdir.DS.'*.*');

			foreach( $files as $file )
			{
				unlink( $file );
				$cli .= "Deleted thumbnail - {$file}\n";
			}
		}

		if ($type === 'logs' or $type === 'all')
		{
			$files = glob(APPPATH.'logs'.DS.'*'.DS.'*'.DS.'*.php');
			foreach( $files as $file )
			{
				unlink( $file );
				rmdir( dirname($file) );
				$cli .= "Deleted log - {$file}\n";
			}
		}

		return $cli;
	}

	public static function __callStatic($method, $args)
	{
		return static::run($method, $args);
	}
}

/* End of file tasks/robots.php */
