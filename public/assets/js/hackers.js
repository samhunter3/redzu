(function () {

	var consoleURL = '/admin/hackers/ajax/console',
	    _consolePrefix, $prefix, $input, $output, $container, $ocontainer,
	    clientCommands, tipJar;

	function setConsolePrefix(val)
	{
		_consolePrefix = val.replace(/(^\s+|\s+$)/g, '');
		$prefix.html(_consolePrefix+'$&nbsp;');

		$input.width($container.width() - $prefix.width());
	}

	function writeToConsole(data, input) {
		if (input === 'user') {
			data = _consolePrefix+'$&nbsp;'+data;
		}
		$output.append(data+'<br />');
		$ocontainer.scrollTop($output.height());
	}

	function consoleEval(data, callback)
	{
		var command_split, method;

		data = data.replace(/(^\s+|\s+$)/g, '');

		if (! data.length) {
			return callback();
		}
		else {
			command_split = data.split(/\s/g);
			method = command_split[0];

			if (clientCommands[method]) {
				clientCommands[method].call(this, command_split.slice(1), callback);
				return;
			}
		}

		$.post(consoleURL, { "command": data }, function (resdata) {
			if (Object.prototype.toString.call(callback) === '[object Function]') {
				callback(resdata.response);
			}
		}, 'json');
	}

	tipJar = [
		'You can evaluate php on the fly with the "php" command',
		'This console is pretty garbage. You must need a really good reason to use it'
	];

	clientCommands = {
		
		// clears the console
		"clear": function (args, callback) {
			$output.html('');
			if (callback) {
				callback();
			}
		},

		"help": function (args, callback) {
			consoleEval('help_ext ' + args.join(' '), function (data) {
				var i;
				for (i in clientCommands) {
					data += ', ' + i;
				}

				if (callback) {
					callback(data);
				}
			});	
		},

		"tip": function (args, callback) {
			var $tipHold = $('<div style="color:#ccc;background:#444;padding:5px;font-size:12px;">thinking of a wise tip..</div>');
			$output.append($tipHold).append('<br />');
			

			setTimeout(function () {
				$tipHold.html('<strong>tip:</strong> ' + tipJar[Math.floor(tipJar.length * Math.random())]);
			}, 500);

			if (callback) {
				callback();
			}
		},

		"js": function (args, callback) {
			var output;

			if (args.length < 1) {
				callback();
				return writeToConsole('usage: js [source code...]');
			}

			output = {
				"clear": function () {
					clientCommands.clear();	
				},

				"write": function (data) {
					writeToConsole(data);
				}
			};

			try {
				eval('(function () { this.write(' + args.join(' ') + '); }).call(output);');
			}
			catch (e) {
				writeToConsole('-redzu: js: <span style="color:#f33">' + e.message + '</span>');
			}
			finally {
				if (callback) {
					callback();
				}
			}
		},
	};

	$prefix = $('#hacker-console-input-prefix');
	$container = $('#hacker-console-input-container');
	$ocontainer = $('#hacker-console-output-container');
	$input = $('#hacker-console-input');
	$output = $('#hacker-console-output');
	
	$output.wrap('<div style="display:table;height:200px;width:100%;" />');

	$(document).bind('ready click', function (e) {
		if (document.activeElement === document.body) {
			e.stopPropagation();
			$input.focus();
		}
	});

	$input.bind('keydown', function (e) {
		if(e.keyCode === 13) {
			writeToConsole(this.value, 'user');
			$input.addClass('ajax-load-dark-input').attr('disabled', 'disabled');
			consoleEval(this.value, function (data) {
				$input.removeClass('ajax-load-dark-input').removeAttr('disabled');
				if (data) {
					writeToConsole(data);
				}
			});

			this.value = '';
		}
	});

	setConsolePrefix("hackers:redzu " + redzu.user.username);
	$output.append('<span style="color:#888">First time user? Try &quot;</span>help<span style="color:#888">&quot;</span><br />');
	
	setTimeout(function () {
		clientCommands.tip([], null);
	}, 1500);
}).call(this);