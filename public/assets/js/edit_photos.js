(function ($) {

	var photoModURL = '/users/photos/ajax/:action/:uid/:pid';

	function html_entities(str)
	{
		return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	}

	function trim(str)
	{
		return str.replace(/(^\s+|\s+$)/g,'');
	}

	function nl2br(str)
	{
		return str.replace(/(\r)?\n/g, '<br/>\n');
	}

	function clean_whitespace(str)
	{
		return str.replace(/[^\S\n]+/g, ' ');
	}

	function photoMsgCallback( type, message )
	{
		if( type === 'error' ) {
			alert( message );
			return false;
		}

		return true;
	}

	/**
	 * Photo Ajax Module
	 *
	 * @param string mod
	 * @param object|string params
	 * @param function Callback function
	 * @return void
	 */
	function photoAjax( mod, uid, params, callback )
	{
		var url = photoModURL.replace(/\:action/g, mod),
		    pid = '';

		if( typeof params === 'object' && params['id'] )
		{
			pid = params['id'];
		}

		url = url.replace(/\:pid/g, pid);
		url = url.replace(/\:uid/g, uid);
		$.post( url, params, function (data) {

		if( data.error ) {
			if( data.error && photoMsgCallback('error', data.error) === false ) {
				return false;
			}
		}
		else if( data.message )	{
			photoMsgCallback('message', data.message);
		}

		callback.apply(this, Array.prototype.slice.call(arguments));
		}, 'json' );
	}

	$('.image-box').each(function (k, v) {
		var $self = $(this),
			object_id = $self.attr('data-photo-id'),
			$nav = $self.find('.image-nav'),
		    $description = $self.find('.image-desc .clickb'),
		    $editButton = $nav.find('.im-action-edit'),
		    $delButton = $nav.find('.im-action-delete'),
		    $defaultLink = $nav.find('.im-action-setdefault'),
		    editCallback;
		
		$delButton.click(function (e) {
			if( confirm('Are you sure you want to delete this photo?') ) {
				photoAjax('delete', 'me', { id: object_id }, function () {
					$self.animate({ opacity: 'hide', height: 'hide' }, 'fast');
				});
			}
		});

		$defaultLink.click(function (e) {
			photoAjax('setdefault', 'me', { id: object_id }, function (data) {
				window.location.reload();
			});
		});

		editCallback = function (e) {
			var $new, changeCallback, desc, $newval;

			desc = clean_whitespace(trim($description.hasClass('default-value') ? '' : $description.text()));

			$new = $('<textarea class="edit-pure">' + desc + '</textarea>');
			
			changeCallback = function (e) {
				if( e['shiftKey'] ) {
					if( e.keyCode === 13 && $new.val().indexOf('\n') !== -1) {
						e.preventDefault();
						return false;
					}
					return true;
				}
				if( ! e['keyCode'] || e.keyCode === 13 )
				{
					$newval = $new.val();
					$new.remove();

					if( ! $newval.replace(/\s+/g,'').length ) {
						$description.html('click here to edit description');
					}

					$description.show().html(nl2br(html_entities($new.val())));
					photoAjax('edit', 'me', { id: object_id, description: $new.val() }, function (data) {
						data.description = clean_whitespace(trim(data.description));

						if( data.description ) {
							$description.show().html(nl2br(data.description));
						}
					});
				}
			};

			$description.hide();
			$description.after($new);
			$new.focus().blur(changeCallback).keypress(changeCallback);
		};
		
		$editButton.click(editCallback);
		$description.click(editCallback);
	});


}).call(this, jQuery);  