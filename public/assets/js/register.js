(function ($)
{
	var zodiac, zodiac_years, zodiac_min_year, i;

	zodiac = [
		'rat', 'ox', 'tiger', 'rabbit', 'dragon', 'snake',
		'horse', 'sheep', 'monkey', 'rooster', 'dog', 'pig'
	];

	$('.animal-selection-form li>a').bind('click', function (e) {
		var $self = $(this),
		    name = $self.find('.image-label').text(),
		    key  = $self.attr('data-animal'),
		    $img  = $self.find('.image img');

		$('#animal').val( key );
		$('#animalimg').html($img.clone().attr({ width: '100', height: '100' }));
		$('#animalsubmit').val('Next step \u00BB');
		$('#skip').show();

	});

	$('.animal-selection-form li>a').each(function () {
		var $self = $(this),
		    key  = $self.attr('data-animal'),
		    $img  = $self.find('.image img'),
		    index;

		if( (index = $.inArray(key, zodiac)) !== -1 )
		{
			zodiac[index] = [ key, $img.clone() ];
		}

	});

	zodiac_years = $('#zodiac_years');
	zodiac_min_year = parseFloat( zodiac_years.attr('data-min-year') );

	for( i = zodiac_min_year; i > 1900; i-- )
	{
		zodiac_years.append('<option>' + i + '</option>');
	}

	zodiac_years.bind('change', function () {
		var animal;

		if( ! RegExp('^[0-9]+$').test(this.value) )
		{
			$('#zodiac_error').css({ visibility: 'visible' }).html('Please select your birth year.');
			return false;
		}

		$('#zodiac_error').css({ visibility: 'hidden' });

		animal = zodiac[((parseFloat(this.value) - 1901) % 12)];

		$('#animal').val( animal[0] );
		$('#animalimg').html(animal[1].attr({ width: '100', height: '100' }));
		$('#animalsubmit').val('Next step \u00BB');
		$('#skip').show();
	});

	$('#go_oldschool_btn').bind('click', function () {
		$('#animals_list_wrap, #zodiac_form').show();
		$('#animals_list, #go_oldschool_wrap').hide();
	});

	$('#go_back_btn').bind('click', function () {
		$('#animals_list_wrap, #zodiac_form').hide();
		$('#animals_list, #go_oldschool_wrap').show();
	});

}).call(this, jQuery);