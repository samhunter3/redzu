$('.calendar .event').tooltipsy({

	className: 'tipevent',

	offset: [0, -1],
    show: function (e, $el) {
    	var cur_top = parseInt($el[0].style.top.replace(/[a-z]/g, '')) + 'px'; 
        $el.css({
            'top': cur_top,
            'display': 'block'
        }).animate({
            'opacity': '1.0'
        }, 150).css('top', cur_top);
    },
    hide: function (e, $el) {
        $el.animate({
            'top': '+=200px',
            'opacity': '0.0',
        }, 300, function () { $el.css({ 'top': '-1000px' }) });
    },

	content: function ($el) {
		return $el.find('.tipdata').html();
	}

});