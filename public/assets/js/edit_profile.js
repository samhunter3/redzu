(function ($)
{
	var uid = ($('#profilefields').attr('data-uid') || this.redzu.user.uid || 'me'),
		uiCallback, uiDialog;

	this.profile_aboutme_fields = new ProfileFields(uid);
	this.profile_aboutme = new ProfileFields(uid);

	if( ! this.redzu ) {
		this.redzu = {};
	}

	uiCallback = function (e) {
		e.preventDefault();
		if( e.keyCode === 27 ) {
			uiDialog.hide();
		}
	};

	ui.Dialog.prototype.hide = (function (d)
	{
		return function () {
			$('body').unbind('keyup', uiCallback);
			return d.apply(this, arguments);
		}
	})(ui.Dialog.prototype.hide);

	ui.Dialog.prototype.show = (function (d)
	{
		return function () {
			$('body').bind('keyup', uiCallback);
			return d.apply(this, arguments);
		}
	})(ui.Dialog.prototype.show);

	redzu.update_profile_info = function (data)
	{
		var $form, bind_form;
		if(! ui) return false;

		form_submit = function (e) {
			e.preventDefault();

			$.post('/users/ajax/setprofiledata/'+uid, $form.serialize(), function (data) {
				if( 'form' in data ) {
					$form.html($(data.form).html());
				}

				if( data['success'] ) {
					uiDialog.dialog('close');
					window.location.reload();
				}
				handleFlagErrors($form);
			}, 'json');

			return false;
		};

		$.get('/users/ajax/setprofiledata/'+uid, function (data) {
			$form = $(data.form);
			$form.bind('submit', form_submit);
			$form.find('#gmtoffset').val(-(new Date().getTimezoneOffset())/60);
			uiDialog = $form.attr('title', 'Edit Profile Info').dialog({
				autoOpen: true,
				modal: true
			});
		}, 'json');
	};

	function html_entities(str)
	{
		return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	}

	function ProfileFields(uid, fields, el)
	{
		var i;
		this.uid = uid || -1;
		this.fields = fields || {};
	}
	ProfileFields.prototype.getFields = function ()
	{
		var i, fields = [];
		for(i in this.fields) {
			if(this.fields.hasOwnProperty(i)) {
				fields.push(i);
			}
		}

		return fields;
	};

	ProfileFields.prototype.getElement = function (key)
	{
		return this.fields[key][1];
	};

	ProfileFields.prototype.setElement = function (key, elem)
	{
		this.fields[key][1] = elem;
		return this;
	};

	ProfileFields.prototype.getLabel = function (key)
	{
		return this.fields[key][0];
	};

	ProfileFields.prototype.setLabel = function (key, label)
	{
		this.fields[key][0] = (label || '');
		return this;
	};

	ProfileFields.prototype.addField = function (name, label, el)
	{
		this.fields[name] = [(label || ''), el];
		return this;
	};

	ProfileFields.prototype.deleteField = function (name, el)
	{
		delete this.fields[name];
		return this;
	};

	ProfileFields.prototype.getURL = function (fields)
	{
		fields || (fields = this.getFields()); 
		return '/users/ajax/profilefields/' + uid + '/' + fields.join(',');
	};

	ProfileFields.prototype.onError = function (errors)
	{
		var errorText = '', self = this;
		$.each(errors, function (field, error) {
			errorText += self.getLabel(field) + ': ' + error + '\n';
		});

		alert(errorText);
		
		return false;
	};

	ProfileFields.prototype._getErrors = function (data)
	{
		var errors = {};

		$.each(data, function (k, v) {
			v.error && (errors[k] = v.error);
		});

		return $.isEmptyObject(errors) ? false : errors;
	};

	ProfileFields.prototype.update = function (params, callback, errorcallback)
	{
		var self = this;
		errorcallback = (errorcallback || this.onUpdateError || this.onError);
		$.post(this.getURL(), params, function (data)
		{
			var errors = self._getErrors(data);
			if( errors !== false && errorcallback.call(self, errors) === false )
			{
				return false;
			}

			$.each(data, function (k, v) {
				callback.call(self.getElement(k), k, v, errors);
			});
		}, 'json');
	};

	ProfileFields.prototype.getForm = function (callback, errorcallback)
	{
		var self = this;
		errorcallback = (errorcallback || this.onGetFormError || this.onError);
		$.get(this.getURL(), function (data)
		{
			var errors = self._getErrors(data);
			if( errors !== false && errorcallback.call(self,errors) === false )
			{
				return false;
			}

			$.each(data, function (k, v) {
				callback.call(self.getElement(k), k, v, errors);
			});
		}, 'json');
	};

	$('#profilefields .profile-field[data-key]').each(function ()
	{
		var $self = $(this),
		    name = $self.attr('data-key'),
		    value = $self.find('small')[0],
		    label = $self.find('.profile-field-label').text();

		profile_aboutme_fields.addField(name, label, $self);
	});

	$('#profile_aboutme_fields').prepend($('<span class="edit-profile-field">Edit</span>')
	.bind('click', function ()
	{
		var saveCallback,
		    editButton = this;

		saveCallback = function (e) {
			e.preventDefault();

			var $self = $(this), restoreForm;

			restoreForm = function () {
				$self.find('.alt-button').hide();
				$('#profilefields').unwrap();
				$(editButton).show();
				return true;
			};

			profile_aboutme_fields.update($self.serialize(), function (field, data, errors) {
				restoreForm && restoreForm() && (restoreForm = null);
				this.find('small').html(html_entities(data.value || '—')).show().siblings('.data-form').remove();
			});
		};

		$(editButton).hide();

		$('#profilefields').wrap($('<form />').bind('submit', saveCallback))
			.append($('<input type="submit" value="Save" class="alt-button">').button());

		// Get profile field elements for editing
		profile_aboutme_fields.getForm(function (field, data, errors)
		{
			var $dataform;

			if(errors[ field ])
			{
				return false;
			}

			$dataform = $(data.form);

			if( data.form ) {
				this.find('small').hide().before($('<span class="data-form"></span>').append($dataform));
				$dataform.focus();
			}
		});
	}));

	$('#user_summary').each(function () {
		var $self = $(this);
		$(this).bind('click', function (e) {
			e.preventDefault();
			e.stopPropagation();

			redzu.update_profile_info();
		});
		$(this).addClass('edit-user-summary');
		$('#main_user_status').after($('<button class="edit-no-text"></button>').button({ icons: { primary: 'ui-icon-pencil' } })).remove();
	});

	$('#profile_aboutme').each(function () {

		var $self = $(this),
		$content = $self.find('p'),
		$newContent = $('<form />'),
		$editButton = $('<button class="edit-button">Edit Description</button>').button({ icons: { primary: 'ui-icon-pencil' } }),
		$saveButton = $('<input type="submit" value="Save" class="alt-button">'),
		editEvent;

		editEvent = function (e) {
			profile_aboutme.getForm(function (field, data, errors) {

				$newContent.bind('submit', function (e) {
					e.preventDefault();

					profile_aboutme.update($newContent.serialize(), function (field, data, errors) {
						$newContent.remove();
						$content.show().html(html_entities(data.value).replace(/\n/g, '<br />'));
						$editButton.show();
					});
				});

				$editButton.hide();
				$content.hide();
				$self.prepend( $newContent.html(data.form).append($('<div>').html($saveButton.button())) );
			});
		};

		$self.append($editButton.bind('click', editEvent));

		profile_aboutme.addField('about_me', 'About Me', $self);

		if( ! $content.text().replace(/\s+/,'') )
		{
			$content.html('<small>You can write a little something about yourself here.</small>');
		}
	});
}).call(this, jQuery);