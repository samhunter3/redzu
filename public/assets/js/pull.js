(this.redzu) || (this.redzu = {});

(function (undefined) {

var requestTypes, i;

function redzuRequest(type, path, data, callback, err) {
	if (typeof data === 'function') {
		callback = data;
		data = undefined;
	}

	$.ajax({
		url: '/pull/' + path.replace(/(^[\/]+|[\/]+$)/g,'') + '.json',
		complete: function (ndata, type, ret) {
			if(type === 'error') {
				ret = null;
			}
			else {
				ret = $.parseJSON(ndata.responseText);
			}

			if (typeof callback === 'function') {
				return callback.call(redzu, ret, type);
			}
		},
		type: type.toUpperCase(),
		data: data,
		error: (err !== undefined ? err : callback),
	});
}

redzu.push = function () {
	redzu.dev.warn('redzu.push deprecated, use redzu.post instead');
	return redzu.post.apply(null, [].slice.call(arguments));
}
redzu.pull = function () {
	redzu.dev.warn('redzu.pull deprecated, use redzu.get instead');
	return redzu.get.apply(null, [].slice.call(arguments));
}

requestTypes = ['get', 'put', 'post', 'delete'];
for (i = 0; i < requestTypes.length; i++) {
	redzu[ requestTypes[i] ] = (function (requestType) {
		return function () {
			var args = [].slice.call(arguments, 0, 3);
			args.unshift(requestType);

			redzuRequest.apply(redzu, args);
			return redzu;
		};
	}(requestTypes[i]));
}

}).call(this);                 