(this.redzu) || (this.redzu = {});

$(document).ready(function() {

	/* Drop Down Menu */
	$("header li.dropdown").bind('click mouseout mouseover', function (e) {
      	if(e.type === 'mouseout') $(this).removeClass("expanded");
      	else $(this).toggleClass("expanded");
    });
	
	/* Featured Product Price Overlay */
	$("#featured .regular .product").mouseover(function () {
		$(this).find(".overlay").css("display","block");
	}).mouseout(function () {
		$(this).find(".overlay").css("display","none");
	});
	
	$("#featured .responsive .product").click(function () {
		$(this).find(".overlay").toggle('fast');
	});
	
	/* Sale Product Price Overlay */
	$(".sale .regular .product").mouseover(function () {
		$(this).find(".overlay").css("display","block");
	}).mouseout(function () {
		$(this).find(".overlay").css("display","none");
	});
	
	$(".sale .responsive .product").click(function () {
		$(this).find(".overlay").toggle('fast');
	});
	
	/* Expanding Search Input Field */
	$(".search-input").focus(function() {
		var $self = $(this);
		if($self.val() === $self.attr('placeholder')) {
			$self.val('');
		}
		$self.css("width","201px");
	}).focusout(function () {
		var $self = $(this);
		if($self.val() === '') {
			$self.val($self.attr('placeholder'));
		}
		$self.css("width","85px");
	});
	
	/* Remove Contents fron Newsletter Input Field */
	$(".newsletter-input").focus(function() {
		$(this).attr("value","");
	});
	
	/* Slideshow Control */
	$(".slider.regular").simplecarousel({
		width: 705,
		height: 390,
		visible: 1,
		auto: 8000,
		next: $('.slider-next'),
		prev: $('.slider-prev'),
		pagination: false,
		layout: "regular",
		fade: true
	});
	
	$(".slider.responsive").simplecarousel({
		width: 300,
		height: 214,
		visible: 1,
		auto: 8000,
		next: $('.slider-next'),
		prev: $('.slider-prev'),
		pagination: false,
		layout: "responsive",
		fade: true
	});
	
	/* Featured Products Control */
	$(".featured.regular").simplecarousel({
		width: 705,
		height: 240,
		visible: 1,
		auto: 8000,
		next: $('.featured-next'),
		prev: $('.featured-prev'),
		pagination: false,
		vertical: true,
		layout: "regular"
	});
	$(".featured.responsive").simplecarousel({
		width: 300,
		height: 231,
		visible: 1,
		auto: 8000,
		next: $('.featured-next'),
		prev: $('.featured-prev'),
		pagination: false,
		vertical: true,
		layout: "responsive"
	});
	
	/* Initializes Twitter Feed */
	$(function($){
		$(".tweet").tweet({
			join_text: "auto",
			username: "redzudating",
			count: 2,
			auto_join_text_default: "we said,",
			auto_join_text_ed: "we",
			auto_join_text_ing: "we were",
			auto_join_text_reply: "we replied",
			auto_join_text_url: "we shared",
			loading_text: "Loading Tweets.."
		});
	});
           
});

$('.im-action-like').live('click', function (e) {
	var $self = $(this),
	    object_type = $self.attr('data-object-type'),
	    object_id = $self.attr('data-object-id'),
	    count = (parseFloat($self.text().replace(/(\s+)/, '')) || 0),
	    t = count + ( $self.hasClass('im-icon-love') ? 1 : -1 );

	$self.toggleClass('im-icon-love').toggleClass('im-icon-unlove');

	if(t < 1) {
		$self.html('Like');
	} else {
		$self.html( '' + t + ' Like' + (t !== 1 ? 's' : '') );
	}

	$.post('/users/ajax/togglelike/'+ object_type +'/' + object_id, function (data) {
		if(data.text) {
			$self.html(data.text);
		}
	}, 'json');
});

$('.im-action-like.im-icon-unlove').live('load', function (e) {
	var $self = $(this);
	$self.hover(function (e) {
		$self.attr('data-old-value', $self.html());
		$self.html('Unlike');
	}, function (e) {
		var $self = $(this);
		$self.html( $self.attr('data-old-value') );
	});
});

/* tooltipsy by Brian Cray
 * Lincensed under GPL2 - http://www.gnu.org/licenses/gpl-2.0.html
 * Option quick reference:
 * - alignTo: "element" or "cursor" (Defaults to "element")
 * - offset: Tooltipsy distance from element or mouse cursor, dependent on alignTo setting. Set as array [x, y] (Defaults to [0, -1])
 * - content: HTML or text content of tooltip. Defaults to "" (empty string), which pulls content from target element's title attribute
 * - show: function(event, tooltip) to show the tooltip. Defaults to a show(100) effect
 * - hide: function(event, tooltip) to hide the tooltip. Defaults to a fadeOut(100) effect
 * - delay: A delay in milliseconds before showing a tooltip. Set to 0 for no delay. Defaults to 200
 * - css: object containing CSS properties and values. Defaults to {} to use stylesheet for styles
 * - className: DOM class for styling tooltips with CSS. Defaults to "tooltipsy"
 * - showEvent: Set a custom event to bind the show function. Defaults to mouseenter
 * - hideEvent: Set a custom event to bind the show function. Defaults to mouseleave
 * Method quick reference:
 * - $('element').data('tooltipsy').show(): Force the tooltip to show
 * - $('element').data('tooltipsy').hide(): Force the tooltip to hide
 * - $('element').data('tooltipsy').destroy(): Remove tooltip from DOM
 * More information visit http://tooltipsy.com/
 */
(function(a){a.tooltipsy=function(c,b){this.options=b;this.$el=a(c);this.title=this.$el.attr("title")||"";this.$el.attr("title","");this.random=parseInt(Math.random()*10000);this.ready=false;this.shown=false;this.width=0;this.height=0;this.delaytimer=null;this.$el.data("tooltipsy",this);this.init()};a.tooltipsy.prototype.init=function(){var b=this;b.settings=a.extend({},b.defaults,b.options);b.settings.delay=parseInt(b.settings.delay);if(typeof b.settings.content==="function"){b.readify()}if(b.settings.showEvent===b.settings.hideEvent&&b.settings.showEvent==="click"){b.$el.toggle(function(c){if(b.settings.showEvent==="click"&&b.$el[0].tagName=="A"){c.preventDefault()}if(b.settings.delay>0){b.delaytimer=window.setTimeout(function(){b.show(c)},b.settings.delay)}else{b.show(c)}},function(c){if(b.settings.showEvent==="click"&&b.$el[0].tagName=="A"){c.preventDefault()}window.clearTimeout(b.delaytimer);b.delaytimer=null;b.hide(c)})}else{b.$el.bind(b.settings.showEvent,function(c){if(b.settings.showEvent==="click"&&b.$el[0].tagName=="A"){c.preventDefault()}if(b.settings.delay>0){b.delaytimer=window.setTimeout(function(){b.show(c)},b.settings.delay)}else{b.show(c)}}).bind(b.settings.hideEvent,function(c){if(b.settings.showEvent==="click"&&b.$el[0].tagName=="A"){c.preventDefault()}window.clearTimeout(b.delaytimer);b.delaytimer=null;b.hide(c)})}};a.tooltipsy.prototype.show=function(f){var d=this;if(d.ready===false){d.readify()}if(d.shown===false){if((function(h){var g=0,e;for(e in h){if(h.hasOwnProperty(e)){g++}}return g})(d.settings.css)>0){d.$tip.css(d.settings.css)}d.width=d.$tipsy.outerWidth();d.height=d.$tipsy.outerHeight()}if(d.settings.alignTo==="cursor"&&f){var c=[f.pageX+d.settings.offset[0],f.pageY+d.settings.offset[1]];if(c[0]+d.width>a(window).width()){var b={top:c[1]+"px",right:c[0]+"px",left:"auto"}}else{var b={top:c[1]+"px",left:c[0]+"px",right:"auto"}}}else{var c=[(function(e){if(d.settings.offset[0]<0){return e.left-Math.abs(d.settings.offset[0])-d.width}else{if(d.settings.offset[0]===0){return e.left-((d.width-d.$el.outerWidth())/2)}else{return e.left+d.$el.outerWidth()+d.settings.offset[0]}}})(d.offset(d.$el[0])),(function(e){if(d.settings.offset[1]<0){return e.top-Math.abs(d.settings.offset[1])-d.height}else{if(d.settings.offset[1]===0){return e.top-((d.height-d.$el.outerHeight())/2)}else{return e.top+d.$el.outerHeight()+d.settings.offset[1]}}})(d.offset(d.$el[0]))]}d.$tipsy.css({top:c[1]+"px",left:c[0]+"px"});d.settings.show(f,d.$tipsy.stop(true,true))};a.tooltipsy.prototype.hide=function(c){var b=this;if(b.ready===false){return}if(c&&c.relatedTarget===b.$tip[0]){b.$tip.bind("mouseleave",function(d){if(d.relatedTarget===b.$el[0]){return}b.settings.hide(d,b.$tipsy.stop(true,true))});return}b.settings.hide(c,b.$tipsy.stop(true,true))};a.tooltipsy.prototype.readify=function(){this.ready=true;this.$tipsy=a('<div id="tooltipsy'+this.random+'" style="position:absolute;z-index:2147483647;display:none">').appendTo("body");this.$tip=a('<div class="'+this.settings.className+'">').appendTo(this.$tipsy);this.$tip.data("rootel",this.$el);var c=this.$el;var b=this.$tip;this.$tip.html(this.settings.content!=""?(typeof this.settings.content=="string"?this.settings.content:this.settings.content(c,b)):this.title)};a.tooltipsy.prototype.offset=function(c){var b=ot=0;if(c.offsetParent){do{if(c.tagName!="BODY"){b+=c.offsetLeft-c.scrollLeft;ot+=c.offsetTop-c.scrollTop}}while(c=c.offsetParent)}return{left:b,top:ot}};a.tooltipsy.prototype.destroy=function(){this.$tipsy.remove();a.removeData(this.$el,"tooltipsy")};a.tooltipsy.prototype.defaults={alignTo:"element",offset:[0,-1],content:"",show:function(c,b){b.fadeIn(100)},hide:function(c,b){b.fadeOut(100)},css:{},className:"tooltipsy",delay:200,showEvent:"mouseenter",hideEvent:"mouseleave"};a.fn.tooltipsy=function(b){return this.each(function(){new a.tooltipsy(this,b)})}})(jQuery);

/**
 * UIKit
 *
 * @link https://github.com/visionmedia/UIKit
 */
var ui={};
(function(e){function f(){this.callbacks={}}e.Emitter=f;f.prototype.on=function(b,a){(this.callbacks[b]=this.callbacks[b]||[]).push(a);return this};f.prototype.once=function(b,a){function d(){c.off(b,d);a.apply(this,arguments)}var c=this;this.on(b,d);return this};f.prototype.off=function(b,a){var d=this.callbacks[b];if(!d)return this;if(1==arguments.length)return delete this.callbacks[b],this;var c=d.indexOf(a);d.splice(c,1);return this};f.prototype.emit=function(b){var a=[].slice.call(arguments,1),
d=this.callbacks[b];if(d)for(var c=0,g=d.length;c<g;++c)d[c].apply(this,a);return this}})(ui);
(function(e,f){function b(d){ui.Emitter.call(this);d=d||{};this.template=f;this.el=$(this.template);this.render(d);a&&a.hide();b.effect&&this.effect(b.effect);a=this}var a;e.Dialog=b;e.dialog=function(a,c){switch(arguments.length){case 2:return new b({title:a,message:c});case 1:return new b({message:a})}};b.prototype=new ui.Emitter;b.prototype.render=function(a){var b=this.el,g=a.title,a=a.message,i=this;b.find(".close").click(function(){i.emit("close");i.hide();return!1});b.find("h1").text(g);g||
b.find("h1").remove();"string"==typeof a?b.find("p").text(a):a&&b.find("p").replaceWith(a.el||a);setTimeout(function(){b.removeClass("hide")},0)};b.prototype.closable=function(){this.el.addClass("closable");return this};b.prototype.effect=function(a){this._effect=a;this.el.addClass(a);return this};b.prototype.modal=function(){this._overlay=ui.overlay();return this};b.prototype.overlay=function(){var a=this;this._overlay=ui.overlay({closable:!0}).on("hide",function(){a.closedOverlay=!0;a.hide()});
return this};b.prototype.escapable=function(){var a=this;$(document).bind("keydown.dialog",function(b){27==b.which&&($(this).unbind("keydown.dialog"),a.hide())})};b.prototype.show=function(){var a=this._overlay;this.emit("show");a&&(a.show(),this.el.addClass("modal"));(!a||a.closable)&&this.escapable();this.el.appendTo("body");this.el.css({marginLeft:-(this.el.width()/2)+"px"});return this};b.prototype.hide=function(a){var b=this;this.emit("hide");if(a)return setTimeout(function(){b.hide()},a),this;
this.el.addClass("hide");this._effect?setTimeout(function(a){a.remove()},500,this):b.remove();this._overlay&&!b.closedOverlay&&this._overlay.hide();return this};b.prototype.remove=function(){this.el.remove();return this}})(ui,'<div id="dialog" class="hide">\n  <div class="content">\n    <h1>Title</h1>\n    <a href="#" class="close">\u00d7</a>\n    <p>Message</p>\n  </div>\n</div>');
(function(e,f){function b(a){ui.Emitter.call(this);var b=this,a=a||{};this.closable=a.closable;this.el=$(f);this.el.appendTo("body");this.closable&&this.el.click(function(){b.hide()})}e.Overlay=b;e.overlay=function(a){return new b(a)};b.prototype=new ui.Emitter;b.prototype.show=function(){this.emit("show");this.el.removeClass("hide");return this};b.prototype.hide=function(){var a=this;this.emit("hide");this.el.addClass("hide");setTimeout(function(){a.el.remove()},2E3);return this}})(ui,'<div id="overlay" class="hide"></div>');
(function(e,f){function b(a){ui.Dialog.call(this,a)}e.Confirmation=b;e.confirm=function(a,d){switch(arguments.length){case 2:return new b({title:a,message:d});case 1:return new b({message:a})}};b.prototype=new ui.Dialog;b.prototype.cancel=function(a){this.el.find(".cancel").text(a);return this};b.prototype.ok=function(a){this.el.find(".ok").text(a);return this};b.prototype.show=function(a){ui.Dialog.prototype.show.call(this);this.el.find(".ok").focus();this.callback=a||function(){};return this};b.prototype.render=
function(a){ui.Dialog.prototype.render.call(this,a);var b=this,a=$(f);this.el.addClass("confirmation");this.el.append(a);this.on("close",function(){b.emit("cancel");b.callback(!1)});a.find(".cancel").click(function(a){a.preventDefault();b.emit("cancel");b.callback(!1);b.hide()});a.find(".ok").click(function(a){a.preventDefault();b.emit("ok");b.callback(!0);b.hide()})}})(ui,'<div class="actions">\n  <button class="cancel">Cancel</button>\n  <button class="ok main">Ok</button>\n</div>');
(function(e,f){function b(a,b,c){return"rgb("+a+", "+b+", "+c+")"}function a(a,b,c,d){return"rgba("+a+", "+b+", "+c+", "+d+")"}function d(a){var b=$(a.target).offset();return{x:a.pageX-b.left,y:a.pageY-b.top}}function c(){ui.Emitter.call(this);this._colorPos={};this.el=$(f);this.main=this.el.find(".main").get(0);this.spectrum=this.el.find(".spectrum").get(0);$(this.main).bind("selectstart",function(a){a.preventDefault()});$(this.spectrum).bind("selectstart",function(a){a.preventDefault()});this.hue(b(255,
0,0));this.spectrumEvents();this.mainEvents();this.h=this.w=180;this.render()}e.ColorPicker=c;c.prototype=new ui.Emitter;c.prototype.size=function(a){return this.width(a).height(a)};c.prototype.width=function(a){this.w=a;this.render();return this};c.prototype.height=function(a){this.h=a;this.render();return this};c.prototype.spectrumEvents=function(){function a(c){var c=d(c).y,g=b.hueAt(c-4);b.hue(g.toString());b.emit("change",g);b._huePos=c;b.render()}var b=this,c=$(this.spectrum),e;c.mousedown(function(b){b.preventDefault();
e=!0;a(b)});c.mousemove(function(b){e&&a(b)});c.mouseup(function(){e=!1})};c.prototype.mainEvents=function(){function a(c){b._colorPos=d(c);c=b.colorAt(b._colorPos.x,b._colorPos.y);b.color(c.toString());b.emit("change",c);b.render()}var b=this,c=$(this.main),e;c.mousedown(function(b){e=!0;a(b)});c.mousemove(function(b){e&&a(b)});c.mouseup(function(){e=!1})};c.prototype.colorAt=function(a,c){var d=this.main.getContext("2d").getImageData(a,c,1,1).data;return{r:d[0],g:d[1],b:d[2],toString:function(){return b(this.r,
this.g,this.b)}}};c.prototype.hueAt=function(a){a=this.spectrum.getContext("2d").getImageData(0,a,1,1).data;return{r:a[0],g:a[1],b:a[2],toString:function(){return b(this.r,this.g,this.b)}}};c.prototype.color=function(a){if(0==arguments.length)return this._color;this._color=a;return this};c.prototype.hue=function(a){if(0==arguments.length)return this._hue;this._hue=a;return this};c.prototype.render=function(a){a=a||{};this.renderMain(a);this.renderSpectrum(a)};c.prototype.renderSpectrum=function(){var c=
this.spectrum,d=c.getContext("2d"),e=this._huePos,f=0.12*this.w,h=this.h;c.width=f;c.height=h;c=d.createLinearGradient(0,0,0,h);c.addColorStop(0,b(255,0,0));c.addColorStop(0.15,b(255,0,255));c.addColorStop(0.33,b(0,0,255));c.addColorStop(0.49,b(0,255,255));c.addColorStop(0.67,b(0,255,0));c.addColorStop(0.84,b(255,255,0));c.addColorStop(1,b(255,0,0));d.fillStyle=c;d.fillRect(0,0,f,h);e&&(d.fillStyle=a(0,0,0,0.3),d.fillRect(0,e,f,1),d.fillStyle=a(255,255,255,0.3),d.fillRect(0,e+1,f,1))};c.prototype.renderMain=
function(){var c=this.main,d=c.getContext("2d"),e=this.w,f=this.h,h=(this._colorPos.x||e)+0.5,j=(this._colorPos.y||0)+0.5;c.width=e;c.height=f;c=d.createLinearGradient(0,0,e,0);c.addColorStop(0,b(255,255,255));c.addColorStop(1,this._hue);d.fillStyle=c;d.fillRect(0,0,e,f);c=d.createLinearGradient(0,0,0,f);c.addColorStop(0,a(255,255,255,0));c.addColorStop(1,a(0,0,0,1));d.fillStyle=c;d.fillRect(0,0,e,f);d.save();d.beginPath();d.lineWidth=1;d.strokeStyle=a(0,0,0,0.5);d.arc(h,j,5,0,2*Math.PI,!1);d.stroke();
d.strokeStyle=a(255,255,255,0.5);d.arc(h,j,4,0,2*Math.PI,!1);d.stroke();d.beginPath();d.restore()}})(ui,'<div class="color-picker">\n  <canvas class="main"></canvas>\n  <canvas class="spectrum"></canvas>\n</div>');
(function(e,f){function b(a){return function(b,d){return e.notify.apply(this,arguments).type(a)}}function a(b){ui.Emitter.call(this);b=b||{};this.template=f;this.el=$(this.template);this.render(b);a.effect&&this.effect(a.effect)}var d;e.Notification=a;$(function(){d=$('<ul id="notifications">');d.appendTo("body")});e.notify=function(b,d){switch(arguments.length){case 2:return(new a({title:b,message:d})).show().hide(4E3);case 1:return(new a({message:b})).show().hide(4E3)}};e.info=e.notify;e.warn=b("warn");
e.error=b("error");a.prototype=new ui.Emitter;a.prototype.render=function(a){var b=this.el,d=a.title,a=a.message,e=this;b.find(".close").click(function(){e.hide();return!1});b.click(function(a){a.preventDefault();e.emit("click",a)});b.find("h1").text(d);d||b.find("h1").remove();"string"==typeof a?b.find("p").text(a):a&&b.find("p").replaceWith(a.el||a);setTimeout(function(){b.removeClass("hide")},0)};a.prototype.closable=function(){this.el.addClass("closable");return this};a.prototype.effect=function(a){this._effect=
a;this.el.addClass(a);return this};a.prototype.show=function(){this.el.appendTo(d);return this};a.prototype.type=function(a){this._type=a;this.el.addClass(a);return this};a.prototype.sticky=function(){return this.hide(0).closable()};a.prototype.hide=function(a){var b=this;if("number"==typeof a){clearTimeout(this.timer);if(!a)return this;this.timer=setTimeout(function(){b.hide()},a);return this}this.el.addClass("hide");this._effect?setTimeout(function(a){a.remove()},500,this):b.remove();return this};
a.prototype.remove=function(){this.el.remove();return this}})(ui,'<li class="notification hide">\n  <div class="content">\n    <h1>Title</h1>\n    <a href="#" class="close">\u00d7</a>\n    <p>Message</p>\n  </div>\n</li>');
(function(e,f){function b(a){ui.Emitter.call(this);this.el=$(f);this.events();this.render({label:a});this.state="hidden"}e.SplitButton=b;b.prototype=new ui.Emitter;b.prototype.events=function(){var a=this,b=this.el;b.find(".button").click(function(b){b.preventDefault();a.emit("click",b)});b.find(".toggle").click(function(b){b.preventDefault();a.toggle()})};b.prototype.toggle=function(){return"hidden"==this.state?this.show():this.hide()};b.prototype.show=function(){this.state="visible";this.emit("show");
this.el.addClass("show");return this};b.prototype.hide=function(){this.state="hidden";this.emit("hide");this.el.removeClass("show");return this};b.prototype.render=function(a){var a=a||{},b=this.el.find(".button"),a=a.label;"string"==a?b.text(a):b.text("").append(a);return this}})(ui,'<div class="split-button">\n  <a class="button" href="#">Action</a>\n  <a class="toggle" href="#"><span></span></a>\n</div>');
(function(e,f){function b(){var a=this;ui.Emitter.call(this);this.items={};this.el=$(f).hide().appendTo("body");$("html").click(function(){a.hide()})}e.Menu=b;e.menu=function(){return new b};b.prototype=new ui.Emitter;b.prototype.add=function(a,b){var c=this,e=$('<li><a href="#">'+a+"</a></li>").addClass(a.toLowerCase().replace(/ +/g,"-").replace(/[^a-z0-9-]/g,"")).appendTo(this.el).click(function(e){e.preventDefault();e.stopPropagation();c.hide();c.emit(a);b&&b()});this.items[a]=e;return this};b.prototype.remove=
function(a){var b=this.items[a];if(!b)throw Error('no menu item named "'+a+'"');this.emit("remove",a);b.remove();delete this.items[a];return this};b.prototype.has=function(a){return!!this.items[a]};b.prototype.moveTo=function(a,b){this.el.css({top:b,left:a});return this};b.prototype.show=function(){this.emit("show");this.el.show();return this};b.prototype.hide=function(){this.emit("hide");this.el.hide();return this}})(ui,'<div class="menu">\n</div>');
(function(e,f){function b(a,b){ui.Emitter.call(this);this._front=a||$("<p>front</p>");this._back=b||$("<p>back</p>");this.template=f;this.render()}e.Card=b;e.card=function(a,d){return new b(a,d)};b.prototype=new ui.Emitter;b.prototype.front=function(a){this._front=a;this.render();return this};b.prototype.back=function(a){this._back=a;this.render();return this};b.prototype.flip=function(){this.emit("flip");this.el.toggleClass("flipped");return this};b.prototype.effect=function(a){this.el.addClass(a);
return this};b.prototype.render=function(){var a=this,b=this.el=$(this.template);b.find(".front").empty().append(this._front.el||$(this._front));b.find(".back").empty().append(this._back.el||$(this._back));b.click(function(){a.flip()})}})(ui,'<div class="card">\n  <div class="wrapper">\n    <div class="face front">1</div>\n    <div class="face back">2</div>\n  </div>\n</div>');

$('#redzu-user-top-img').bind('click', function (e) {
	var $self = $(this),
	    $notifications = $self.find('.redzu-user-top-notifications');

	if( $notifications[0] && $notifications.text().match(/^(\s+)?$/) === null )
	{
		e.preventDefault();
		e.stopPropagation();

		if( redzu.notifications )
		{
			redzu.notifications.show();
		}
	}

});

/**
 * Handle error fields
 */
function handleFlagErrors(selector) {
	$(selector).find('.input-with-error').each(function () {
		var $self=$(this), $error = $self.find('.error');

		if($error.text().replace(/(^\s+|\s+$)/g, '').length) {
			window.$error = $error;
			window.$self = $self;
			$error.fadeIn(250).css({ display: 'inline-block' });
			$self.find('select,input').bind('change', function () {
				$error.hide();
				$self.unbind('change', arguments.callee);
			});
		}
	});
};





/**
 * History.js
 */
// window.JSON||(window.JSON={}),function(){function f(a){return a<10?"0"+a:a}function quote(a){return escapable.lastIndex=0,escapable.test(a)?'"'+a.replace(escapable,function(a){var b=meta[a];return typeof b=="string"?b:"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+a+'"'}function str(a,b){var c,d,e,f,g=gap,h,i=b[a];i&&typeof i=="object"&&typeof i.toJSON=="function"&&(i=i.toJSON(a)),typeof rep=="function"&&(i=rep.call(b,a,i));switch(typeof i){case"string":return quote(i);case"number":return isFinite(i)?String(i):"null";case"boolean":case"null":return String(i);case"object":if(!i)return"null";gap+=indent,h=[];if(Object.prototype.toString.apply(i)==="[object Array]"){f=i.length;for(c=0;c<f;c+=1)h[c]=str(c,i)||"null";return e=h.length===0?"[]":gap?"[\n"+gap+h.join(",\n"+gap)+"\n"+g+"]":"["+h.join(",")+"]",gap=g,e}if(rep&&typeof rep=="object"){f=rep.length;for(c=0;c<f;c+=1)d=rep[c],typeof d=="string"&&(e=str(d,i),e&&h.push(quote(d)+(gap?": ":":")+e))}else for(d in i)Object.hasOwnProperty.call(i,d)&&(e=str(d,i),e&&h.push(quote(d)+(gap?": ":":")+e));return e=h.length===0?"{}":gap?"{\n"+gap+h.join(",\n"+gap)+"\n"+g+"}":"{"+h.join(",")+"}",gap=g,e}}"use strict",typeof Date.prototype.toJSON!="function"&&(Date.prototype.toJSON=function(a){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+f(this.getUTCMonth()+1)+"-"+f(this.getUTCDate())+"T"+f(this.getUTCHours())+":"+f(this.getUTCMinutes())+":"+f(this.getUTCSeconds())+"Z":null},String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(a){return this.valueOf()});var JSON=window.JSON,cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,escapable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta={"\b":"\\b","\t":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},rep;typeof JSON.stringify!="function"&&(JSON.stringify=function(a,b,c){var d;gap="",indent="";if(typeof c=="number")for(d=0;d<c;d+=1)indent+=" ";else typeof c=="string"&&(indent=c);rep=b;if(!b||typeof b=="function"||typeof b=="object"&&typeof b.length=="number")return str("",{"":a});throw new Error("JSON.stringify")}),typeof JSON.parse!="function"&&(JSON.parse=function(text,reviver){function walk(a,b){var c,d,e=a[b];if(e&&typeof e=="object")for(c in e)Object.hasOwnProperty.call(e,c)&&(d=walk(e,c),d!==undefined?e[c]=d:delete e[c]);return reviver.call(a,b,e)}var j;text=String(text),cx.lastIndex=0,cx.test(text)&&(text=text.replace(cx,function(a){return"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)}));if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return j=eval("("+text+")"),typeof reviver=="function"?walk({"":j},""):j;throw new SyntaxError("JSON.parse")})}(),function(a,b){"use strict";var c=a.History=a.History||{},d=a.jQuery;if(typeof c.Adapter!="undefined")throw new Error("History.js Adapter has already been loaded...");c.Adapter={bind:function(a,b,c){d(a).bind(b,c)},trigger:function(a,b,c){d(a).trigger(b,c)},extractEventData:function(a,c,d){var e=c&&c.originalEvent&&c.originalEvent[a]||d&&d[a]||b;return e},onDomLoad:function(a){d(a)}},typeof c.init!="undefined"&&c.init()}(window),function(a,b){"use strict";var c=a.document,d=a.setTimeout||d,e=a.clearTimeout||e,f=a.setInterval||f,g=a.History=a.History||{};if(typeof g.initHtml4!="undefined")throw new Error("History.js HTML4 Support has already been loaded...");g.initHtml4=function(){if(typeof g.initHtml4.initialized!="undefined")return!1;g.initHtml4.initialized=!0,g.enabled=!0,g.savedHashes=[],g.isLastHash=function(a){var b=g.getHashByIndex(),c;return c=a===b,c},g.saveHash=function(a){return g.isLastHash(a)?!1:(g.savedHashes.push(a),!0)},g.getHashByIndex=function(a){var b=null;return typeof a=="undefined"?b=g.savedHashes[g.savedHashes.length-1]:a<0?b=g.savedHashes[g.savedHashes.length+a]:b=g.savedHashes[a],b},g.discardedHashes={},g.discardedStates={},g.discardState=function(a,b,c){var d=g.getHashByState(a),e;return e={discardedState:a,backState:c,forwardState:b},g.discardedStates[d]=e,!0},g.discardHash=function(a,b,c){var d={discardedHash:a,backState:c,forwardState:b};return g.discardedHashes[a]=d,!0},g.discardedState=function(a){var b=g.getHashByState(a),c;return c=g.discardedStates[b]||!1,c},g.discardedHash=function(a){var b=g.discardedHashes[a]||!1;return b},g.recycleState=function(a){var b=g.getHashByState(a);return g.discardedState(a)&&delete g.discardedStates[b],!0},g.emulated.hashChange&&(g.hashChangeInit=function(){g.checkerFunction=null;var b="",d,e,h,i;return g.isInternetExplorer()?(d="historyjs-iframe",e=c.createElement("iframe"),e.setAttribute("id",d),e.style.display="none",c.body.appendChild(e),e.contentWindow.document.open(),e.contentWindow.document.close(),h="",i=!1,g.checkerFunction=function(){if(i)return!1;i=!0;var c=g.getHash()||"",d=g.unescapeHash(e.contentWindow.document.location.hash)||"";return c!==b?(b=c,d!==c&&(h=d=c,e.contentWindow.document.open(),e.contentWindow.document.close(),e.contentWindow.document.location.hash=g.escapeHash(c)),g.Adapter.trigger(a,"hashchange")):d!==h&&(h=d,g.setHash(d,!1)),i=!1,!0}):g.checkerFunction=function(){var c=g.getHash();return c!==b&&(b=c,g.Adapter.trigger(a,"hashchange")),!0},g.intervalList.push(f(g.checkerFunction,g.options.hashChangeInterval)),!0},g.Adapter.onDomLoad(g.hashChangeInit)),g.emulated.pushState&&(g.onHashChange=function(b){var d=b&&b.newURL||c.location.href,e=g.getHashByUrl(d),f=null,h=null,i=null,j;return g.isLastHash(e)?(g.busy(!1),!1):(g.doubleCheckComplete(),g.saveHash(e),e&&g.isTraditionalAnchor(e)?(g.Adapter.trigger(a,"anchorchange"),g.busy(!1),!1):(f=g.extractState(g.getFullUrl(e||c.location.href,!1),!0),g.isLastSavedState(f)?(g.busy(!1),!1):(h=g.getHashByState(f),j=g.discardedState(f),j?(g.getHashByIndex(-2)===g.getHashByState(j.forwardState)?g.back(!1):g.forward(!1),!1):(g.pushState(f.data,f.title,f.url,!1),!0))))},g.Adapter.bind(a,"hashchange",g.onHashChange),g.pushState=function(b,d,e,f){if(g.getHashByUrl(e))throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");if(f!==!1&&g.busy())return g.pushQueue({scope:g,callback:g.pushState,args:arguments,queue:f}),!1;g.busy(!0);var h=g.createStateObject(b,d,e),i=g.getHashByState(h),j=g.getState(!1),k=g.getHashByState(j),l=g.getHash();return g.storeState(h),g.expectedStateId=h.id,g.recycleState(h),g.setTitle(h),i===k?(g.busy(!1),!1):i!==l&&i!==g.getShortUrl(c.location.href)?(g.setHash(i,!1),!1):(g.saveState(h),g.Adapter.trigger(a,"statechange"),g.busy(!1),!0)},g.replaceState=function(a,b,c,d){if(g.getHashByUrl(c))throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");if(d!==!1&&g.busy())return g.pushQueue({scope:g,callback:g.replaceState,args:arguments,queue:d}),!1;g.busy(!0);var e=g.createStateObject(a,b,c),f=g.getState(!1),h=g.getStateByIndex(-2);return g.discardState(f,e,h),g.pushState(e.data,e.title,e.url,!1),!0}),g.emulated.pushState&&g.getHash()&&!g.emulated.hashChange&&g.Adapter.onDomLoad(function(){g.Adapter.trigger(a,"hashchange")})},typeof g.init!="undefined"&&g.init()}(window),function(a,b){"use strict";var c=a.console||b,d=a.document,e=a.navigator,f=a.sessionStorage||!1,g=a.setTimeout,h=a.clearTimeout,i=a.setInterval,j=a.clearInterval,k=a.JSON,l=a.alert,m=a.History=a.History||{},n=a.history;k.stringify=k.stringify||k.encode,k.parse=k.parse||k.decode;if(typeof m.init!="undefined")throw new Error("History.js Core has already been loaded...");m.init=function(){return typeof m.Adapter=="undefined"?!1:(typeof m.initCore!="undefined"&&m.initCore(),typeof m.initHtml4!="undefined"&&m.initHtml4(),!0)},m.initCore=function(){if(typeof m.initCore.initialized!="undefined")return!1;m.initCore.initialized=!0,m.options=m.options||{},m.options.hashChangeInterval=m.options.hashChangeInterval||100,m.options.safariPollInterval=m.options.safariPollInterval||500,m.options.doubleCheckInterval=m.options.doubleCheckInterval||500,m.options.storeInterval=m.options.storeInterval||1e3,m.options.busyDelay=m.options.busyDelay||250,m.options.debug=m.options.debug||!1,m.options.initialTitle=m.options.initialTitle||d.title,m.intervalList=[],m.clearAllIntervals=function(){var a,b=m.intervalList;if(typeof b!="undefined"&&b!==null){for(a=0;a<b.length;a++)j(b[a]);m.intervalList=null}},m.debug=function(){(m.options.debug||!1)&&m.log.apply(m,arguments)},m.log=function(){var a=typeof c!="undefined"&&typeof c.log!="undefined"&&typeof c.log.apply!="undefined",b=d.getElementById("log"),e,f,g,h,i;a?(h=Array.prototype.slice.call(arguments),e=h.shift(),typeof c.debug!="undefined"?c.debug.apply(c,[e,h]):c.log.apply(c,[e,h])):e="\n"+arguments[0]+"\n";for(f=1,g=arguments.length;f<g;++f){i=arguments[f];if(typeof i=="object"&&typeof k!="undefined")try{i=k.stringify(i)}catch(j){}e+="\n"+i+"\n"}return b?(b.value+=e+"\n-----\n",b.scrollTop=b.scrollHeight-b.clientHeight):a||l(e),!0},m.getInternetExplorerMajorVersion=function(){var a=m.getInternetExplorerMajorVersion.cached=typeof m.getInternetExplorerMajorVersion.cached!="undefined"?m.getInternetExplorerMajorVersion.cached:function(){var a=3,b=d.createElement("div"),c=b.getElementsByTagName("i");while((b.innerHTML="<!--[if gt IE "+ ++a+"]><i></i><![endif]-->")&&c[0]);return a>4?a:!1}();return a},m.isInternetExplorer=function(){var a=m.isInternetExplorer.cached=typeof m.isInternetExplorer.cached!="undefined"?m.isInternetExplorer.cached:Boolean(m.getInternetExplorerMajorVersion());return a},m.emulated={pushState:!Boolean(a.history&&a.history.pushState&&a.history.replaceState&&!/ Mobile\/([1-7][a-z]|(8([abcde]|f(1[0-8]))))/i.test(e.userAgent)&&!/AppleWebKit\/5([0-2]|3[0-2])/i.test(e.userAgent)),hashChange:Boolean(!("onhashchange"in a||"onhashchange"in d)||m.isInternetExplorer()&&m.getInternetExplorerMajorVersion()<8)},m.enabled=!m.emulated.pushState,m.bugs={setHash:Boolean(!m.emulated.pushState&&e.vendor==="Apple Computer, Inc."&&/AppleWebKit\/5([0-2]|3[0-3])/.test(e.userAgent)),safariPoll:Boolean(!m.emulated.pushState&&e.vendor==="Apple Computer, Inc."&&/AppleWebKit\/5([0-2]|3[0-3])/.test(e.userAgent)),ieDoubleCheck:Boolean(m.isInternetExplorer()&&m.getInternetExplorerMajorVersion()<8),hashEscape:Boolean(m.isInternetExplorer()&&m.getInternetExplorerMajorVersion()<7)},m.isEmptyObject=function(a){for(var b in a)return!1;return!0},m.cloneObject=function(a){var b,c;return a?(b=k.stringify(a),c=k.parse(b)):c={},c},m.getRootUrl=function(){var a=d.location.protocol+"//"+(d.location.hostname||d.location.host);if(d.location.port||!1)a+=":"+d.location.port;return a+="/",a},m.getBaseHref=function(){var a=d.getElementsByTagName("base"),b=null,c="";return a.length===1&&(b=a[0],c=b.href.replace(/[^\/]+$/,"")),c=c.replace(/\/+$/,""),c&&(c+="/"),c},m.getBaseUrl=function(){var a=m.getBaseHref()||m.getBasePageUrl()||m.getRootUrl();return a},m.getPageUrl=function(){var a=m.getState(!1,!1),b=(a||{}).url||d.location.href,c;return c=b.replace(/\/+$/,"").replace(/[^\/]+$/,function(a,b,c){return/\./.test(a)?a:a+"/"}),c},m.getBasePageUrl=function(){var a=d.location.href.replace(/[#\?].*/,"").replace(/[^\/]+$/,function(a,b,c){return/[^\/]$/.test(a)?"":a}).replace(/\/+$/,"")+"/";return a},m.getFullUrl=function(a,b){var c=a,d=a.substring(0,1);return b=typeof b=="undefined"?!0:b,/[a-z]+\:\/\//.test(a)||(d==="/"?c=m.getRootUrl()+a.replace(/^\/+/,""):d==="#"?c=m.getPageUrl().replace(/#.*/,"")+a:d==="?"?c=m.getPageUrl().replace(/[\?#].*/,"")+a:b?c=m.getBaseUrl()+a.replace(/^(\.\/)+/,""):c=m.getBasePageUrl()+a.replace(/^(\.\/)+/,"")),c.replace(/\#$/,"")},m.getShortUrl=function(a){var b=a,c=m.getBaseUrl(),d=m.getRootUrl();return m.emulated.pushState&&(b=b.replace(c,"")),b=b.replace(d,"/"),m.isTraditionalAnchor(b)&&(b="./"+b),b=b.replace(/^(\.\/)+/g,"./").replace(/\#$/,""),b},m.store={},m.idToState=m.idToState||{},m.stateToId=m.stateToId||{},m.urlToId=m.urlToId||{},m.storedStates=m.storedStates||[],m.savedStates=m.savedStates||[],m.normalizeStore=function(){m.store.idToState=m.store.idToState||{},m.store.urlToId=m.store.urlToId||{},m.store.stateToId=m.store.stateToId||{}},m.getState=function(a,b){typeof a=="undefined"&&(a=!0),typeof b=="undefined"&&(b=!0);var c=m.getLastSavedState();return!c&&b&&(c=m.createStateObject()),a&&(c=m.cloneObject(c),c.url=c.cleanUrl||c.url),c},m.getIdByState=function(a){var b=m.extractId(a.url),c;if(!b){c=m.getStateString(a);if(typeof m.stateToId[c]!="undefined")b=m.stateToId[c];else if(typeof m.store.stateToId[c]!="undefined")b=m.store.stateToId[c];else{for(;;){b=(new Date).getTime()+String(Math.random()).replace(/\D/g,"");if(typeof m.idToState[b]=="undefined"&&typeof m.store.idToState[b]=="undefined")break}m.stateToId[c]=b,m.idToState[b]=a}}return b},m.normalizeState=function(a){var b,c;if(!a||typeof a!="object")a={};if(typeof a.normalized!="undefined")return a;if(!a.data||typeof a.data!="object")a.data={};b={},b.normalized=!0,b.title=a.title||"",b.url=m.getFullUrl(m.unescapeString(a.url||d.location.href)),b.hash=m.getShortUrl(b.url),b.data=m.cloneObject(a.data),b.id=m.getIdByState(b),b.cleanUrl=b.url.replace(/\??\&_suid.*/,""),b.url=b.cleanUrl,c=!m.isEmptyObject(b.data);if(b.title||c)b.hash=m.getShortUrl(b.url).replace(/\??\&_suid.*/,""),/\?/.test(b.hash)||(b.hash+="?"),b.hash+="&_suid="+b.id;return b.hashedUrl=m.getFullUrl(b.hash),(m.emulated.pushState||m.bugs.safariPoll)&&m.hasUrlDuplicate(b)&&(b.url=b.hashedUrl),b},m.createStateObject=function(a,b,c){var d={data:a,title:b,url:c};return d=m.normalizeState(d),d},m.getStateById=function(a){a=String(a);var c=m.idToState[a]||m.store.idToState[a]||b;return c},m.getStateString=function(a){var b,c,d;return b=m.normalizeState(a),c={data:b.data,title:a.title,url:a.url},d=k.stringify(c),d},m.getStateId=function(a){var b,c;return b=m.normalizeState(a),c=b.id,c},m.getHashByState=function(a){var b,c;return b=m.normalizeState(a),c=b.hash,c},m.extractId=function(a){var b,c,d;return c=/(.*)\&_suid=([0-9]+)$/.exec(a),d=c?c[1]||a:a,b=c?String(c[2]||""):"",b||!1},m.isTraditionalAnchor=function(a){var b=!/[\/\?\.]/.test(a);return b},m.extractState=function(a,b){var c=null,d,e;return b=b||!1,d=m.extractId(a),d&&(c=m.getStateById(d)),c||(e=m.getFullUrl(a),d=m.getIdByUrl(e)||!1,d&&(c=m.getStateById(d)),!c&&b&&!m.isTraditionalAnchor(a)&&(c=m.createStateObject(null,null,e))),c},m.getIdByUrl=function(a){var c=m.urlToId[a]||m.store.urlToId[a]||b;return c},m.getLastSavedState=function(){return m.savedStates[m.savedStates.length-1]||b},m.getLastStoredState=function(){return m.storedStates[m.storedStates.length-1]||b},m.hasUrlDuplicate=function(a){var b=!1,c;return c=m.extractState(a.url),b=c&&c.id!==a.id,b},m.storeState=function(a){return m.urlToId[a.url]=a.id,m.storedStates.push(m.cloneObject(a)),a},m.isLastSavedState=function(a){var b=!1,c,d,e;return m.savedStates.length&&(c=a.id,d=m.getLastSavedState(),e=d.id,b=c===e),b},m.saveState=function(a){return m.isLastSavedState(a)?!1:(m.savedStates.push(m.cloneObject(a)),!0)},m.getStateByIndex=function(a){var b=null;return typeof a=="undefined"?b=m.savedStates[m.savedStates.length-1]:a<0?b=m.savedStates[m.savedStates.length+a]:b=m.savedStates[a],b},m.getHash=function(){var a=m.unescapeHash(d.location.hash);return a},m.unescapeString=function(b){var c=b,d;for(;;){d=a.unescape(c);if(d===c)break;c=d}return c},m.unescapeHash=function(a){var b=m.normalizeHash(a);return b=m.unescapeString(b),b},m.normalizeHash=function(a){var b=a.replace(/[^#]*#/,"").replace(/#.*/,"");return b},m.setHash=function(a,b){var c,e,f;return b!==!1&&m.busy()?(m.pushQueue({scope:m,callback:m.setHash,args:arguments,queue:b}),!1):(c=m.escapeHash(a),m.busy(!0),e=m.extractState(a,!0),e&&!m.emulated.pushState?m.pushState(e.data,e.title,e.url,!1):d.location.hash!==c&&(m.bugs.setHash?(f=m.getPageUrl(),m.pushState(null,null,f+"#"+c,!1)):d.location.hash=c),m)},m.escapeHash=function(b){var c=m.normalizeHash(b);return c=a.escape(c),m.bugs.hashEscape||(c=c.replace(/\%21/g,"!").replace(/\%26/g,"&").replace(/\%3D/g,"=").replace(/\%3F/g,"?")),c},m.getHashByUrl=function(a){var b=String(a).replace(/([^#]*)#?([^#]*)#?(.*)/,"$2");return b=m.unescapeHash(b),b},m.setTitle=function(a){var b=a.title,c;b||(c=m.getStateByIndex(0),c&&c.url===a.url&&(b=c.title||m.options.initialTitle));try{d.getElementsByTagName("title")[0].innerHTML=b.replace("<","&lt;").replace(">","&gt;").replace(" & "," &amp; ")}catch(e){}return d.title=b,m},m.queues=[],m.busy=function(a){typeof a!="undefined"?m.busy.flag=a:typeof m.busy.flag=="undefined"&&(m.busy.flag=!1);if(!m.busy.flag){h(m.busy.timeout);var b=function(){var a,c,d;if(m.busy.flag)return;for(a=m.queues.length-1;a>=0;--a){c=m.queues[a];if(c.length===0)continue;d=c.shift(),m.fireQueueItem(d),m.busy.timeout=g(b,m.options.busyDelay)}};m.busy.timeout=g(b,m.options.busyDelay)}return m.busy.flag},m.busy.flag=!1,m.fireQueueItem=function(a){return a.callback.apply(a.scope||m,a.args||[])},m.pushQueue=function(a){return m.queues[a.queue||0]=m.queues[a.queue||0]||[],m.queues[a.queue||0].push(a),m},m.queue=function(a,b){return typeof a=="function"&&(a={callback:a}),typeof b!="undefined"&&(a.queue=b),m.busy()?m.pushQueue(a):m.fireQueueItem(a),m},m.clearQueue=function(){return m.busy.flag=!1,m.queues=[],m},m.stateChanged=!1,m.doubleChecker=!1,m.doubleCheckComplete=function(){return m.stateChanged=!0,m.doubleCheckClear(),m},m.doubleCheckClear=function(){return m.doubleChecker&&(h(m.doubleChecker),m.doubleChecker=!1),m},m.doubleCheck=function(a){return m.stateChanged=!1,m.doubleCheckClear(),m.bugs.ieDoubleCheck&&(m.doubleChecker=g(function(){return m.doubleCheckClear(),m.stateChanged||a(),!0},m.options.doubleCheckInterval)),m},m.safariStatePoll=function(){var b=m.extractState(d.location.href),c;if(!m.isLastSavedState(b))c=b;else return;return c||(c=m.createStateObject()),m.Adapter.trigger(a,"popstate"),m},m.back=function(a){return a!==!1&&m.busy()?(m.pushQueue({scope:m,callback:m.back,args:arguments,queue:a}),!1):(m.busy(!0),m.doubleCheck(function(){m.back(!1)}),n.go(-1),!0)},m.forward=function(a){return a!==!1&&m.busy()?(m.pushQueue({scope:m,callback:m.forward,args:arguments,queue:a}),!1):(m.busy(!0),m.doubleCheck(function(){m.forward(!1)}),n.go(1),!0)},m.go=function(a,b){var c;if(a>0)for(c=1;c<=a;++c)m.forward(b);else{if(!(a<0))throw new Error("History.go: History.go requires a positive or negative integer passed.");for(c=-1;c>=a;--c)m.back(b)}return m};if(m.emulated.pushState){var o=function(){};m.pushState=m.pushState||o,m.replaceState=m.replaceState||o}else m.onPopState=function(b,c){var e=!1,f=!1,g,h;return m.doubleCheckComplete(),g=m.getHash(),g?(h=m.extractState(g||d.location.href,!0),h?m.replaceState(h.data,h.title,h.url,!1):(m.Adapter.trigger(a,"anchorchange"),m.busy(!1)),m.expectedStateId=!1,!1):(e=m.Adapter.extractEventData("state",b,c)||!1,e?f=m.getStateById(e):m.expectedStateId?f=m.getStateById(m.expectedStateId):f=m.extractState(d.location.href),f||(f=m.createStateObject(null,null,d.location.href)),m.expectedStateId=!1,m.isLastSavedState(f)?(m.busy(!1),!1):(m.storeState(f),m.saveState(f),m.setTitle(f),m.Adapter.trigger(a,"statechange"),m.busy(!1),!0))},m.Adapter.bind(a,"popstate",m.onPopState),m.pushState=function(b,c,d,e){if(m.getHashByUrl(d)&&m.emulated.pushState)throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");if(e!==!1&&m.busy())return m.pushQueue({scope:m,callback:m.pushState,args:arguments,queue:e}),!1;m.busy(!0);var f=m.createStateObject(b,c,d);return m.isLastSavedState(f)?m.busy(!1):(m.storeState(f),m.expectedStateId=f.id,n.pushState(f.id,f.title,f.url),m.Adapter.trigger(a,"popstate")),!0},m.replaceState=function(b,c,d,e){if(m.getHashByUrl(d)&&m.emulated.pushState)throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");if(e!==!1&&m.busy())return m.pushQueue({scope:m,callback:m.replaceState,args:arguments,queue:e}),!1;m.busy(!0);var f=m.createStateObject(b,c,d);return m.isLastSavedState(f)?m.busy(!1):(m.storeState(f),m.expectedStateId=f.id,n.replaceState(f.id,f.title,f.url),m.Adapter.trigger(a,"popstate")),!0};if(f){try{m.store=k.parse(f.getItem("History.store"))||{}}catch(p){m.store={}}m.normalizeStore()}else m.store={},m.normalizeStore();m.Adapter.bind(a,"beforeunload",m.clearAllIntervals),m.Adapter.bind(a,"unload",m.clearAllIntervals),m.saveState(m.storeState(m.extractState(d.location.href,!0))),f&&(m.onUnload=function(){var a,b;try{a=k.parse(f.getItem("History.store"))||{}}catch(c){a={}}a.idToState=a.idToState||{},a.urlToId=a.urlToId||{},a.stateToId=a.stateToId||{};for(b in m.idToState){if(!m.idToState.hasOwnProperty(b))continue;a.idToState[b]=m.idToState[b]}for(b in m.urlToId){if(!m.urlToId.hasOwnProperty(b))continue;a.urlToId[b]=m.urlToId[b]}for(b in m.stateToId){if(!m.stateToId.hasOwnProperty(b))continue;a.stateToId[b]=m.stateToId[b]}m.store=a,m.normalizeStore(),f.setItem("History.store",k.stringify(a))},m.intervalList.push(i(m.onUnload,m.options.storeInterval)),m.Adapter.bind(a,"beforeunload",m.onUnload),m.Adapter.bind(a,"unload",m.onUnload));if(!m.emulated.pushState){m.bugs.safariPoll&&m.intervalList.push(i(m.safariStatePoll,m.options.safariPollInterval));if(e.vendor==="Apple Computer, Inc."||(e.appCodeName||"")==="Mozilla")m.Adapter.bind(a,"hashchange",function(){m.Adapter.trigger(a,"popstate")}),m.getHash()&&m.Adapter.onDomLoad(function(){m.Adapter.trigger(a,"hashchange")})}},m.init()}(window);

// $(function () {

// if( window.history['pushState'] && document['documentElement'] )
// {
// 	var qdomain = window.location.hostname.replace(/(\.|\-|\_)/g, '\\$1'),
// 	    regex_href = RegExp('^(http?\:\/\/'+qdomain+')(.+)$','i'),
// 	    test_href, load_asset, loaded_assets = [];

// 	test_href = function (href, m)
// 	{
// 		m=href.match(regex_href);
// 		if( m && ! RegExp(/^(\#|javascript\:)/).test(href) )
// 		{
// 			return m[m.length-1];
// 		}
// 		return false;
// 	};

// 	handle_push = function (url) {
// 		History.pushState({}, 'Redzu', url);
// 		$.ajax({
// 			'url': url,
// 			complete: function (data) {
// 				document.documentElement.innerHTML='';
// 				$.each(load_assets($(data.responseText)), function (k,v) {
// 					document.documentElement.appendChild(v);
// 				});
// 			}
// 		});
// 	};

// 	var i;
// 	setInterval(function () {
// 		console.log(i++);
// 	}, 1000);

// 	load_assets = function ($dom) {
// 		var iterator, new_dom = [];

// 		iterator = function (k) {
// 			if( this.nodeName === 'SCRIPT' )
// 			{
// 				h = test_href(this.src || this.innerHTML);
// 				if(h && (h=h.replace(/^[\/]+|[\/]+$/g,'').split('?')[0]))
// 				{
// 					if(loaded_assets.indexOf(h) === -1)
// 					{
// 						loaded_assets.push(h);
// 						($dom) && (new_dom.push(this));
// 					}
// 				}	
// 				return;
// 			}
// 			($dom) && (new_dom.push(this));
// 		};

// 		if( $dom ) {
// 			$dom.siblings().each(iterator);
// 			$dom.siblings('meta').remove();
// 		} else {
// 			$(document).find('script, link[rel=stylesheet]').each(iterator);
// 		}

// 		return new_dom;
// 	};

// 	$('a[href]').live('click', function (e) {
// 		var url = test_href(this.href);
// 		if (url)
// 		{
// 			e.preventDefault();
// 			handle_push(url);
// 		}
// 	});

// 	load_assets();
// }

// });

handleFlagErrors('#main');

$('.button').removeClass('button').addClass('alt-button');
$('button,.alt-button,input[type=button],.button-beginact,input[type=submit]').button();

(function ()
{
	(this.redzu) || (this.redzu = {});
	(this.redzu.pages) || (this.redzu.pages = {});

	/**
	 * RedzuException
	 */
	function RedzuException (msg) {
		this.name = 'RedzuException';
		this.message = msg;
	}

	RedzuException.prototype = this.Error.prototype;

	redzu.dev = {};
	redzu.dev.log = function () {
		(redzu.env === 'development') && console.log('\u2665', [].slice.call(arguments));
	};
	redzu.dev.error = function (message) {
		if (redzu.env === 'production') {
			throw new RedzuException(message);
		}
		else if (redzu.env === 'development') {
			console.error('\u2665', [].slice.call(arguments));
		}
	};
	redzu.dev.warn = function () {
		(redzu.env === 'development') && console.warn('\u2665', [].slice.call(arguments));
	};

	redzu.pages.eventCalendar = function () {
		handleFlagErrors("#event_form");
		$("#date").datepicker();

		$('#ticket_price').keypress(function (e) {
			console.log(String.fromCharCode(e.keyCode));
			if(RegExp('[^0-9\.]','g').test(String.fromCharCode(e.keyCode))) {
				e.preventDefault();
			}
		});

		$("#ticket_price").bind('keyup', function (e) {
			var price;
			price = parseFloat(this.value);

			if(! price)	{
				$('#after-ticket-price').show();
			}
			else {
				$('#after-ticket-price').hide();
			}
		});

		$(".calendarbox").click(function (e) {
			$(this).find('input').focus();
		});

	};

	$('#cancel_member_form').each(function () {
		var $self = $(this);

		$self.find('input[name=cancel]').bind('change', function () {
			if (this.value === '1') {
				$self.find('.reason').show().find('textarea').focus();
			}
			else {
				$self.find('.reason').hide();
			}

			$self.find('.submitbtn').show();
		});

		$self.bind('submit', function (e) {
			if(! confirm('This is permanant. One last chance to change your mind.')) {
				e.preventDefault();
			}
		});

	});

	$('#delete_event_btn').bind('click', function (e) {
		if(confirm('Are you sure you want to delete this event?') &&
			confirm('Positive?'))
		{
			window.location=$(this).attr('data-link');
		}
	});

}).call(this);