(this.redzu) || (this.redzu = {});
(function () {
	var noMessages = true;

	if (! redzu.msgTo)
	{
		redzu.msgTo = $('#messages').attr('data-to');
	}

	redzu.putMessage = function (message) {
		var el, img = $('#user_img_'+message.from_user_id).html(),
			classes = 'odd';

		if($('#messages .message').last().hasClass('odd'))
		{
			classes = 'even';
		}

		message.message = message.message.replace(/\</g, '&lt;')
		.replace(/\>/g, '&gt;').replace(/\"/g, '&quot;').replace(/\n/g, "<br />");

		el = $('<div class="message clear ' + classes + '">'
			+ '<div class="message-image">' + img + '</div>'
			+ '   <div class="message-right">'
			+ '<div class="message-text">' +message.message
			+ '</div></div><div class="message-time">' + message.created_at + '</div>'
			+ ' </div>');

		$('#messages').append(el);

			el.find('.message-right').append(function (el2) {
				return el2 = $('<span class="message-delete">Delete Message</span>')
					.bind('click', function () {
						el.fadeOut(300);
						redzu.delete('messages', { messages: [message.id] });
					});
			});

		$(window).scrollTop($('#messages').position().top+$('#messages').height()-210);
	};

	redzu.putMessages = function (messages) {
		$.each(messages, function (k, v) {
			redzu.putMessage(v);
		});
	};

	redzu.pollMessages = function (res, lastID) 
	{
		lastID = lastID || 0;
		redzu.pull('messages', { user: redzu.msgTo, last: lastID, poll: 'true' }, function (res) {
			lastID = res.last;

			if (! $.isEmptyObject(res.messages))
			{
				if (noMessages)
				{
					$('#messages').html('');
					noMessages = false;
				}

				redzu.putMessages(res.messages);
			}

			setTimeout(function () { redzu.pollMessages(res, lastID) }, 10);
		});
	}

	$('#chat_form').bind('submit', function (e) {
		var messagetext = $(this).find('textarea').val();
		e.preventDefault();

		if (messagetext.length) {
			$(this).find('textarea').val('');
			redzu.push('messages', { user: redzu.msgTo, message: messagetext }, function () {
				redzu.putMessage({
					'from_user_id': redzu.user.uid,
					'created_at': 'just now',
					'message': messagetext
				});
			});
		}

	});

	$('#chat_form').find('textarea').bind('keypress', function (e) {
		if(e.keyCode == 13 && ! e.shiftKey)
		{
			e.preventDefault();
			$('#chat_form').submit();
		}
	})

	$('#messages').html('<div class="tcenter pvl">No messages.</div>');
	redzu.pollMessages();

}).call(this);